DEFAULT_HTML=index_en.html
try-bsml-prod=../production
GIT_VERSION=$(shell git rev-parse --short HEAD || echo "No version number")
DATE=$(shell date +%Y-%m-%d)
GIT_OR_DATE=$(shell git rev-parse --short HEAD || date +%Y-%m-%d)

all: MLall HTML COPY_SCRIPTS POP_FILESYS

MLall:
	make -C ml-sources all

COPY_SCRIPTS:
	mkdir -p js
	cd ml-sources; cp `cat scripts` ../js/

HTML:
	(echo "let version=\"${GIT_OR_DATE}\";;";  cat build_HTML.ml) | ocaml
	sed -i 's/$$version$$/'${GIT_OR_DATE}'/g' *.html
	[ -e index.html ] || ln -s ${DEFAULT_HTML} index.html

POP_FILESYS: FS FS_START FS_EXOS

FS :
	[ -d filesys/start ] || mkdir -p filesys/start/

FS_START:
	echo "let _ = print_endline \"Version: "${GIT_VERSION}", "${DATE}"\"" > filesys/start/version.ml
	cp ml-sources/start/*.ml  filesys/start/

FS_EXOS:
	mkdir -p filesys/exercise/BSML #Only exos from directory BSML for now
	cp -a ml-sources/exercise/exercises_dir filesys/exercise
	cp -a ml-sources/exercise/exercises_utils.cmo filesys/exercise
	cp -a ml-sources/exercise/BSML/*.cmo ml-sources/exercise/BSML/*.html  ml-sources/exercise/BSML/exercises_list filesys/exercise/BSML

# Build directory for production

$(try-bsml-prod) : 
	mkdir -p $(try-bsml-prod)

prod : $(try-bsml-prod)  all
	cp -a *.html css/ js/ extjs/ images/ $(try-bsml-prod)
	cp -a filesys $(try-bsml-prod)/
	rm $(try-bsml-prod)/index.html 
	cp -a $(try-bsml-prod)/index_en.html $(try-bsml-prod)/index.html
	mkdir -p $(try-bsml-prod)/ace-builds/
	cp -a ace-builds/src-min-noconflict/ $(try-bsml-prod)/ace-builds/

clean:
	-for i in  `cat ml-sources/scripts`; do rm js/$$i ; done;
	-make -C ml-sources clean
	-rm index*.html
	-rm -rf filesys

clean-prod:
	rm -rf $(try-bsml-prod)

# %:
# 	make -C ml-sources $*

