#
#INSTALL

Successfully compiled with
 - ocaml 4.02.3 (latest compiler version supporting js_of_ocaml.2.6)
 - camlp4 4.02+7
 - ocamlfind 
 - bsml /home/julien/recherche/Software/BSML/branches/0.5.4-findlib
 - ppx_tools_versioned.5.1
 - js_of_ocaml.2.6 : https://github.com/ocsigen/js_of_ocaml/tree/2.6
     can't build it with opam, problem while compiling
     ppx_tools, install following dependencies and then build
     js_of_ocaml from source. To get other dependencies, do 
     opam install js_of_ocaml.2.6
 - deriving.0.7.1 
 - tyxml.3.5.0
 - reactiveData.0.1 

Using opam 2 RC2

 opam switch create bsml_web 4.02.3
 opam pin add bsml /home/julien/recherche/Software/BSML/branches/0.5.4-findlib
 opam install ppx_tools_versioned.5.1
 opam install js_of_ocaml.2.6 #will fail but install required deps
 opam install deriving.0.7.1 tyxml.3.5.0 reactiveData.0.1 
