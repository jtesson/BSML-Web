(* Build database and translate a file 
   Made to be cat and redirected to ocaml
   the identifier version should be linked to a string before in the toplevel
*)
#require "str" ;;
#require "unix";;
    
type tag = string
type text = string
type lang = string
(* the translation base  *)
let base : (lang * ((tag*text) list)) list ref = ref [] ;;
                                            
let db_add (lg:lang) tg txt =
  if List.mem_assoc lg !base then
    base :=
      List.map
        (
          fun (lng,tg_txt_lst) ->
          if lg =lng then
            (lng, ((tg,txt):: (List.remove_assoc tg tg_txt_lst)))
          else
            lng,tg_txt_lst
        )
        !base
  else
    base:=(lg,[tg,txt])::!base

let soi = string_of_int
let db_get_txt  lg tg =
  if tg = "date" then
    let date = Unix.localtime (Unix.time ()) in
    soi (date.tm_year+1900) ^"-"^ (soi (date.tm_mon+1))^"-"^ (soi date.tm_mday)
  else
    if tg = "version" then
      version
    else
      List.assoc tg (List.assoc lg !base)
                                            
let start_tag = Str.regexp "@\\([a-zA-Z_]+\\)@"                             
let end_tag tag = Str.regexp ("@"^tag^"@")

let string_count_char_between str c i j =
  let count = ref 0 in
  String.iteri (fun pos char -> if pos >= i && pos < j && char = c then incr count) str;
  !count 

(**  n premiers éléments d'une liste  *)
let take n lst =
  let rec aux lst i res =
  if n = i then List.rev res else
    match lst with
      [] ->  List.rev res
    | h :: t -> aux t (i+1) (h::res)
  in
  aux lst 0 []
(** chaine de caractère construite à partir d'une liste
 si limit est >=0, seulement limit éléments seront affichés
limit start sep et stop sont optionnels.
string_of_data doit être une fonction convertissant une donnée en chaine de caractère.

exemple : 
   string_of_list ~start:"[. " ~sep:"," string_of_int [0;1;3]
donne la chaîne [. 0,1,3]
  *)
let string_of_list ?( limit= -1 ) ?(start="[") ?(sep=";") ?(stop="]") string_of_data lst =
  let rec str_of_list_aux str lst =
    match lst with
    | [] -> failwith "unreachable state"
    | [d] -> str ^(string_of_data d)
    | h::t -> str_of_list_aux (str ^ (string_of_data h)^sep) t
       
  in
  let stop = if limit >= 0 then "..."^stop else stop in
  let lst = take limit lst in 
  match  lst with
    [] -> start^stop
  | h::t -> (str_of_list_aux start lst)^stop

let string_of_pair  ?(start="(") ?(sep=",") ?(stop=")") string_of_fst_data string_of_snd_data (a,b) =
  start^(string_of_fst_data a)^sep^(string_of_snd_data b)^stop

let markers ?(num=false) ?(ref_line=None) ml =
  let marks = List.sort compare ml in
  let marks_and_tabulations =
    match ref_line with
      None -> List.map (fun i -> i,0) marks
    | Some refline ->
       let _, res = List.fold_left (fun (pre_i, res) i -> i, ((i,string_count_char_between refline '\t' pre_i i ):: res )) (0,[])  ml
       in List.rev res
  in
  (* let _ = print_endline (string_of_list ~start:"->" ~stop:"<-"(string_of_pair string_of_int string_of_int) marks_and_tabulations) in *)
  let res =ref "" and length = ref 0 in
  let rec aux marks =
    match marks with
      [] -> ()
    | (pos,tabs)::t ->
       let sub_len = pos - !length -tabs  in
       length:= !length + sub_len + tabs;
       res := !res ^ (String.make sub_len ' ' ^(String.make tabs '\t' )^(if num then string_of_int pos else "^")); aux t
  in
  aux marks_and_tabulations; !res
              
let rec get_text file line line_counter tag pos_start_text acc_text =
  try
    let pos_end_text = Str.search_forward (end_tag tag) line pos_start_text
    in
    acc_text^(String.sub line pos_start_text (pos_end_text-pos_start_text))
  with
    Not_found ->
    let _ = incr line_counter in 
    get_text file (input_line file)line_counter tag 0
             (acc_text^(String.sub line pos_start_text (String.length line - pos_start_text))^"\n")
  
let scan_translation_base ?(print=false) filename =
  let file = open_in filename in
  let header = input_line file in
  let  _ =                    (* checking header *)
    (Str.search_forward (Str.regexp "LANG=") header 0 = 0) || failwith "Invalid header" in
  let lang =
    String.sub header 5 (String.length header -5) 
  in
  let line_counter = ref 0 in 
  try
    while true do
      let line =  input_line file in
      let _ = incr line_counter in 
      let pos = Str.search_forward start_tag line 0 in
      let tag = Str.matched_group 1 line in
      let pos_start_text = (pos+String.length tag+2) in
      let text = get_text file line line_counter tag pos_start_text ""
      in
      (if print then
         begin
           print_endline line;
           print_endline (markers [pos_start_text]);
           print_endline ("tag:"^tag); print_endline text
         end
      );
      db_add lang tag text
    done
  with _ -> if print then print_endline ("finished at line "^(string_of_int !line_counter))

let rec subst_tags_line_output ?(print=false) lang_output_lst line start =
  try
    (* matching next tag *)
    let pos_tag = Str.search_forward start_tag line start in
    (* getting tag name *)
    let tag = Str.matched_group 1 line in
    (* where to start again next *)
    let pos_start_next_text = (pos_tag+String.length tag+2) in
    begin
      (* Copying text before tag, and then translation of tag in all files *)
      (if print then
         begin
           print_endline line;
           print_endline (markers ~ref_line:(Some line) [start; pos_tag]);
           print_endline (markers ~num:true ~ref_line:(Some line) [start; pos_tag]);
           output_substring stdout line start (pos_tag-start);
           flush stdout
         end
      );
      List.iter (fun (lang, out_file) ->
          output_substring out_file line start (pos_tag-start);
          let translation =  (db_get_txt lang tag) in 
          ( output_string out_file translation;
            (if print then output_string stdout translation)
          )
        ) lang_output_lst 
      ;
        subst_tags_line_output ~print:print lang_output_lst line pos_start_next_text 
    end
  with
    Not_found ->
    List.iter
      (
        fun (_, out_file) ->
        output_substring out_file line start (String.length line -start);
        output_string out_file "\n"
      )
      lang_output_lst
                 
          
let translate_template ?(prefix="") ?(print=false) tpl_file lang_lst =
  let file = open_in tpl_file in
  let lang_output_files = List.map (fun lang -> lang, open_out (prefix^(Filename.chop_suffix tpl_file ".tpl") ^ "_"^lang^".html")) lang_lst in
  try
    while true do
      let line =  input_line file
      in
        subst_tags_line_output ~print:print  lang_output_files line 0 
    done
  with
    End_of_file -> List.iter (fun (_,output_file) -> close_out output_file) lang_output_files; close_in file

(* From my ml_utils file *)
let ls s = Sys.readdir s
let lls s =  Array.to_list (ls s)
           
let grep ?(opt=[]) str lst =
  let str,lst =
    if List.mem "-i" opt then
      String.lowercase str ,List.map String.lowercase lst
    else
      str,lst
  in    
  let filter = 	 fun line ->
    let found  =
      let reg = if List.mem "-e" opt
                then Str.regexp str
                else Str.regexp_string str in
      try
	Str.search_forward reg line 0 >= 0
      with Not_found ->
	false
    in
    if List.mem "-v" opt then
      not found
    else
      found
  in List.filter filter lst

let egrep ?(opt=[]) str lst  = grep ~opt:("-e"::opt) str lst
   
let sed ?(opt=[]) motif templ chaine =
  let motif,chaine =
    Str.regexp motif , chaine
  in    
  Str.global_replace motif templ chaine
  
let is_dir d = Sys.is_directory d                           
let (|>) x f = f x
let (||>) x f = List.map f x ;;

              
let list_lang_bases () = lls "."  |> (egrep ".*\\.tpl.lang$") ;;
                       
let build_base () =
  let bases = list_lang_bases () in
  List.iter scan_translation_base bases ;;
                  
let _ = build_base (); !base ;;

let _ = translate_template (* ~prefix:"test" *) "index.tpl" (List.map fst !base) ;;
