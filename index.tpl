<!DOCTYPE html >
<html  lang="@LANG@">
  <head>
    <title>@TITLE@</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!--   <\!-- Bootstap css -\-> -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
    <!-- <\!-- jQuery library -\-> -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
    <!--   <\!-- Bootstrap JavaScript :  -\-> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

    <!-- local styles and scripts copies  -->
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <script src="extjs/jquery.min.js"></script>
    <script src="extjs/bootstrap.min.js"></script>
      
    <link rel="stylesheet" href="css/bsml_bell.css?@version@" />
    
    <meta name="Keywords"  content="@KEYWORDS@">
    <link rel="shortcut icon" type="image/png" href="images/favicon.png" />
  </head>
  <body class="">
    <div  id="loading-panel" class="loading">
      <div id="loading-descr">@LOADING@ <span id="loading-text"></span><br/>
	<progress id="loading-progress"  max="100">
	  <span id="loading-counter">0</span>%
	</progress>
      </div>
    </div>
    <header class="navbar-wrapper">
      <nav class="navbar navbar-inverse navbar-static-top">
	<div id="intro-navbar" class=""></div>
	<div class="container-fluid">
	  <div  class="navbar-header">
            <div id="intro-navbar"></div>
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar"> 
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
	    </button>
	    <img src="images/bsml_logo_transparent.png"
		 alt="logo BSML" class="navbar-left"
		 style="height: 50px;">
	    <h1 class="navbar-brand" style="font-size:
					    49px;margin-top: 0px;margin-bottom: 0px;"> BSML</h1>
	  </div>
	  <div id="navbar" class="navbar-collapse collapse">
	    <ul class="nav navbar-nav">
	      <li class="dropdown"> 
		<a  class="dropdown-toggle"
                    id="machine_menu"
		    data-toggle="dropdown"
                    in-intro
                >@BSP_MACHINE_MENU@<span class="caret"></span></a>
		<ul id="machine_definition" class="dropdown-menu navbar-inverse">
		    <li>@NUM_PROC@
		      <input id="bsp_p" class="text-input" min="0" max="128" value="4" style="width:2em" type="text">
		    </li>
		    <li>
			@PROC_SPEED@
		      <input id="bsp_r" class="text-input" min="1" value="500.G" style="width:4em" type="text">
		    </li>
		    <li>
		      @BANDWIDTH@
		      <input id="bsp_g" class="text-input" min="1" value="18." style="width:4em" type="text">
		    </li>
		    <li>
		      @LATENCY@
		      <input id="bsp_L" class="text-input" min="1" value="7200."
			     style="width:4em" type="text">
		    </li>
		    <li>
		      <button id="btn-reset" class="button">@RESET_MACHINE@</button>
		    </li>
		</ul>
	      </li>
	      <li class="dropdown">
		<a id="load-source-menu" class="dropdown-toggle" data-toggle="dropdown" in-intro>
		  @LOAD_SOURCE@<span class="caret"></span>
		</a>
		<div class="dropdown-menu  navbar-inverse"><input id="input-file" multiple="" class="sub-menu" type="file"></div>
	      </li>
	      <li class="dropdown">
		<a id="add-exercise-menu" class="dropdown-toggle" data-toggle="dropdown" in-intro>
		  @ADD_EXERCISE@<span class="caret"></span>
		</a>
		
		<div class="dropdown-menu  navbar-inverse"><input multiple="" id="exercise_loader" name="exercise" class="sub-menu" type="file"></div></li>
	      <li   id="reinit-instrumentation" class="btn btn-danger navbar-btn"in-intro>
		@CLEAR_SS_DISPLAY@
	      </li>
	      <li > 
		<a  id="help-menu" data-toggle="collapse" data-target="#help" in-intro>@HELP@<span class="caret"></span></a>
	      </li>
	      <li > 
		<a id="intro-start">@TUTO@</a>
	      </li>
	      <li id="about" data-toggle="modal" data-target="#about-text"><a>@ABOUT@</a></li>
	      <li id="lang" in-intro>
		<a href="index_@OTH_LANG_ABBREV@.html">
		  <img src="images/@OTH_LANG_ABBREV@_flag.png" width="30 px"> @OTH_LANG@</a></li>
	    </ul>
	  </div>
	</div>
      </nav>
    </header>
    <div id="main" class="main">
      <div id="intro-main" class="">
        <div id="intro-container">
          <div id="intro-descriptions"></div>
          <div id="all-descriptions">
            <div id="help-menu-description">Getting help on the interface</div>
            <div id="reinit-instrumentation-description">Removing the drawings of supersteps </div>
            <div id="add-exercise-menu-description" intro-next="exercises">Adding new exercises specially crafted for this interface</div>
            <div id="load-source-menu-description" intro-next="source-file" >Loading multiple source files</div>
            <div id="machine_menu-description">Specify here the caracteristics of the simulated machine</div>
            <div id="lang-description">Select your language</div>
            <div id="editor-description">Edit your code here</div>
          </div>
          <span id="intro-prev">@PREV@<img width="40em" src="images/prev.png"/></span>
          <span id="intro-next"><img width="40em" src="images/next.png"/>@NEXT@</span>
          <span id="intro-stop">Stop<img width="40em" src="images/stop.png"/></span>
        </div>
      </div>
      <div id="editor_toplevel" class="collapse">

        <div id="editor"  class="remove-offline collapse in" in-intro>let compute n = for i = 0 to n do let x =ref 1 in while !x <10000000  do incr x done done
let sync () = Bsml.proj &lt;&lt; 0 &gt;&gt;
let _ = sync ()
let v =  compute 20
let _ = sync ()
let v =   compute 20
let comms = Bsml.put &lt;&lt; fun i -> if i = $this$ +1 then i else 0 &gt;&gt;

let v = let _ = compute 20 in &lt;&lt; compute (20* $this$ ) &gt;&gt;
let comms = Bsml.put &lt;&lt; fun i -> Array.make (10*i) i &gt;&gt;      
      </div>
      
      <nav class="navbar navbar-inverse navbar-thin" >
	<ul id="editor_menu" class="navbar-inverse navbar-nav nav navbar-collapse collapse remove-offline" >
	  <li class="dropdown"> 
	    <a  class="dropdown-toggle"
		id="Editor-Code-interaction-title"
		       data-toggle="dropdown" >
		
		@CODE@

		<span class="caret"></span></a>
	    <ul id="Editor-Code-interaction" class="dropdown-menu navbar-inverse">
		<li><button type="button" class="button" id="editor_load_button" >

		    @COPY_CODE@

		</button> </li>
		<li><button type="button" class="button" id="editor_load_selection_button" >

		    @COPY_SELECTION@

		</button> </li>
	      <li><button type="button" class="button"
			  id="editor_eval_button">

		  @EVALUATE_ALL@

	      </button>
	      </li> 
	      <li>
		<button type="button" class="button" id="editor_eval_selection_button" >
		    @EVALUATE_SELECTION@
		</button>
	      </li>
	    </ul>
	    
	  <li class="dropdown"> 
	    <a  class="dropdown-toggle"
		id="Editor-save-share-title"
		data-toggle="dropdown" > @SAVE_AND_SHARE@ <span class="caret"></span></a>
	    <ul id="Editor-save-share" class="dropdown-menu navbar-inverse">
	      <li><button type="button" class="button"
			  id="btn-share-editor">@SHARE_EDITOR_CONTENT@</button></li>
	      <li>
		<button type="button" class="button"
			id="editor_save_button">@SAVE_EDITOR_CONTENT@</button>
	      </li>
	    </ul>
	    <li class="navbar-btn"><a class="button"  data-toggle="collapse"
				      data-target="#toplevel-container"><span class="text">@TOPLEVEL@</span><span class="caret"></span></a></li>
	    <li class="navbar-btn"><a class="button" data-toggle="collapse"
				      data-target="#editor"><span class="text">@EDITOR@</span><span class="caret upcaret"></span></a></li>
	</ul>
      </nav>
      <script src="ace-builds/src-min-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
      <script src="ace-builds/src-min-noconflict/ext-language_tools.js" type="text/javascript" charset="utf-8"></script>
      <script type="text/javascript" >
	// trigger extension
	ace.require("ace/ext/language_tools");
	var editor = ace.edit("editor");
	editor.setTheme("ace/theme/monokai");
	editor.getSession().setMode("ace/mode/ocaml");
	editor.setOptions({
        enableBasicAutocompletion: true,
        enableSnippets: true,
        enableLiveAutocompletion: true
	});

      </script>
      <div id="toplevel-container" class="collapse in">
	<a href="http://traclifo.univ-orleans.fr/BSML/" >
      	  <img class="bsml-logo"  src="images/bsml_logo_transparent.png" alt="BSML" />
	</a>
	<pre id="output"></pre>
	<div>
          <div id="sharp" class="sharp"></div>
          <textarea id="userinput">@LOADING@ ...</textarea>
          <button type="button" class="btn btn-default btn-share"
                  id="btn-share">@SHARE@</button>
	</div>
      </div>
    </div>
      <div id="toggle-right-side-panel" class="btn-default"
      onclick="$('#toggle-right-side-panel').toggleClass('collapsed');
	       $('#toplevel-side').collapse('toggle');
	       $('#main').toggleClass('collapse-side-panel')">
	<span class="caret"></span></div>
      <div id="toplevel-side" class="collapse in width">
      <div id="help" class="collapse">
	<h4>@TOPLEVEL_COMMANDS@</h4>
	<table class="table table-striped table-condensed">
	  <tbody>
	    <tr>
	      <td>@ENTER@</td>
	      <td>@SUBMIT_CODE@</td>
	    </tr>
	    <tr>
	      <td>@CTRL_ENTER@</td>
	      <td>@NEWLINE@</td>
	    </tr>
	    <tr>
	      <td>@UP_DOWN@</td>
	      <td>@BROWSE_HISTORY@</td>
	    </tr>
	    <tr>
	      <td>@CTRL_L@</td>
	      <td>@CLEAR_DISPLAY@</td>
	    </tr>
	    <tr>
	      <td>@CTRL_K@</td>
	      <td>@RESET_TOPLEVEL@</td>
	    </tr>
	    <tr>
	      <td>@TAB@</td>
	      <td>@INDENT_CODE@</td>
	    </tr>
	  </tbody>
	</table>
      </div>
      <div class="full-line">
      <div id="sources" class="closed toggle-panels">
	<h3>@SOURCE_CODES@</h3>
	<!-- <input type="button" id="btn-load-file" value="charger" -->
		    <!-- 	 class="small-button"/> -->
		    <div id="source-file-container" class="source-file-container list-group"></div>
		    <div id="toplevel-examples" class="list-group"></div>
      </div>
      <div id="exercises-files" class="toggle-panels  remove-offline">
	<h3>@EXERCISES@</h3>
	<!-- <\!-- <input type="text" id="exercise-url" value="test.cmo" /> -\-> -->
	<div id="courses_list" class=" menu courses-list-container list-group"></div>
	<!-- <input type="button" id="btn-exercise" value="charger" class="small-button"/> -- -->
	<div id="exercise_list" class=" menu exercice-list-container list-group"></div>
      </div>
	</div>

	<div class="full-line   toggle-panels">
	
	<div id="exercises" class="not-ready">
	  <div id="exercise_title_and_descritpion">
	    <div id="exercise_title_and_check">
	      <h4 id="exercise_title" class="exercise_title_class"></h4>
	      <button id="exercise_check_button"
		      class="small-button">@CHECK@</button>
	    </div>
	    <div id="exercise_description" class="exercise_descr_class"></div>
	    <div id="exercise_result_text"></div>
	    <div id="exercise_result_jauge"></div>
	    <div id="exercise_functions"></div> 
	  </div>
	</div>

	<div id="debug" class="debug"></div>
      </div>
	<div id="bsp-canvas-container-container">
	  <h3 class="modal-header"> @SUPERSTEPS_COST@</h3>
	  <div id="bsp-canvas-container">
	    <!-- <div id="single-bsp-canvas-container-0" class="container-bsp-step"> -->
	    <!--   <canvas width="400" height="400" id="bsp-canvas-0" class="canvas-bsp-step"></canvas> -->
	    <!-- </div> -->
	  </div>
	</div>
    </div>
    </div>
    <aside id="about-text" class="modal fade">
      <div class="modal-dialog">
	<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">@ABOUT@</h4>
	</div>
	<div class="modal-body">
	    <p> @AUTHOR@ : Julien Tesson </p>
	    @ABOUT_TEXT@
	    <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">@CLOSE@</button>
          </div>
	</div>
      </div>
    </aside>
    <script type="text/javascript">

      window.onhashchange = function() { window.location.reload() }
      var hash = window.location.hash.replace(/^#/,"");
      var fields = hash.split(/&/);
      var prefix = "js/";
      var version = "";
      for(var f in fields){
        var data = fields[f].split(/=/);
        if(data[0] == "version"){
          version = data[1].replace(/%20|%2B/g,"+");
          break;
        }
      }
      function load_script(url){
          var load_bar =  document.getElementById("loading-text");
	  var fileref=document.createElement('script');
	  load_bar.innerHTML = url;
	  fileref.setAttribute("type","text/javascript");
	  fileref.setAttribute("src", prefix+(version==""?"":(version+"/"))+url+"?@version@");
	  document.getElementsByTagName("head")[0].appendChild(fileref);
      }
     load_script("stdlib.cmis.js");
     load_script("js_of_ocaml.cmis.js");
     load_script("js_of_ocaml.toplevel.cmis.js");
     load_script("js_of_ocaml.deriving.cmis.js");
     load_script("js_of_ocaml.tyxml.cmis.js");
     load_script("js_of_ocaml.graphics.cmis.js"); 
     load_script("lwt.cmis.js");
     load_script("dynlink.cmis.js");
     load_script("bsml.cmis.js");
     load_script("bsml.js_lib.cmis.js");
     load_script("bsml.instrumentation.cmis.js");
     load_script("bsml.top_js.cmis.js");
     load_script("toplevel.js");
    </script>
  </body>
</html>
