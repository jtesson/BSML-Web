(**********************************************************)
(*  Online BSML  : Binary file downloading                *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(**********************************************************)

open XmlHttpRequest
open Lwt

let blob_ressource url =
    let open XmlHttpRequest in
    (
      (XmlHttpRequest.perform_raw Blob url) 
      >>= 
        (fun response -> 
         if response.code = 200 then
           File.readAsBinaryString (Js.Opt.get response.content (fun () -> failwith "no blob"))
         else    
                failwith "blurp"
        )
    )

let register_remote_file url dest=
    blob_ressource url 
    >|=
    (fun js_str -> 
       let txt =(Js.to_string js_str) 
       in
        Sys_js.register_file dest txt
    )
    
let  handle_exercise url name=
     register_remote_file url name
     >|=
     (fun _ -> Loader.load_exercise (fun s -> log s) (fun s -> print_endline s) name)
     
  
(* let exo_modules = *)
(*   handle_exercise "filesys/exercise/BSML/application.cmo"  "exos/BSML/application.cmo"  *)
