open Bsmlsig
open BsmlInstrumentation

type level = Global | Local

module type INSTRUMENTED_WITH_LEVEL = sig
  include INSTRUMENTED_BSMLSEQ
  val current_level : unit -> level
end                            
