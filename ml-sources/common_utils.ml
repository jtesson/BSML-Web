(**********************************************************)
(*  Online BSML  : helper functions                       *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(**********************************************************)

open Bsmlsig
       

       
let my_assert c msg : 'a =
  assert (if not c then print_endline msg; c)
	 

let take n l =
  let rec aux n l res =
    if n <=0
    then List.rev res
    else
      match l with
	[] -> List.rev res
      | h::t -> aux (n-1) t (h::res)
  in
  aux n l []

let transpose mat =
  let n = Array.length mat in
  let m = if n > 0 then Array.length mat.(0) else 0
  in
  Array.init m (fun i  -> Array.init n (fun j -> mat.(j).(i) )) 


let forall_before n predicat =
  let rec aux i  = if i = n then true else predicat i && aux (i+1)
  in aux 0

	 
let failing_before n predicat =
  let rec aux i  =
    if i = n then [] else
      if predicat i then  aux (i+1)
      else i:: (aux(i+1))
  in aux 0
	 
let exists_before n predicat =
  let rec aux i  = if i = n  then false else predicat i || aux (i+1)
  in aux 0

	 
let from_0_to_n n =
  let rec aux i list =
    if i = 0  then 0::list
    else aux (i-1) (i::list)
  in aux n []

let is_nil l = match l with [] -> true | _ -> false 
						
let get_heads_tails list_of_list =
  (* my_assert (List.for_all (fun l -> List.length l = List.length h)); *)
  my_assert (match list_of_list with []-> true | h::t -> not (is_nil h)) "get_heads_tails";
  List.fold_left
    (fun  (hd,tails) -> function (h::t) ->  (h::hd),(t::tails)|  [] -> raise (Invalid_argument "get_head_tails"))
    ([],[])
    list_of_list 
		 
let zip_all list_of_list =
  match  list_of_list with
    [] -> []
  | h::t ->
     my_assert (List.for_all (fun l -> List.length l = List.length h) t) "zip_all";
     let rec aux h lol res =
       match h with
	 [] -> res
       | h::t -> let (res',lol) = get_heads_tails lol in
		 aux t lol ((List.rev res')::res)
     in
     List.rev (aux h list_of_list [])
(*  *)

let rec zip l1 l2 =
  my_assert (List.length l1 = List.length l2) "zip";
  match l1,l2 with
    [],[] -> []
  | h1::t1 , h2::t2 -> (h1,h2)::zip t1 t2
  | _  -> failwith "unsatisfied assert in zip"
(*  *)

				    
let reduce op l=
  match l with
    [] -> failwith "cannot reduce empty list"
  | [h] -> h
  | h::t -> List.fold_left op h t


let red_heads_tails op list_of_list
  =
  (* my_assert (List.for_all (fun l -> List.length l = List.length h)); *)
  my_assert (match list_of_list with []-> false | h::t -> not (is_nil h)) "red_head_tails";
  match list_of_list with
    [] -> assert false 
  | []::t -> assert false
  |(h_h::t_h) :: lol ->
    List.fold_left
      (fun (hd,tails) l -> match l with (h::t) -> ( op h hd),(t::tails) | _ -> failwith "unsatisfied assert in red_head_tails")
      (h_h,[t_h])
      lol
      
let zip_red ?(until=(-1)) op list_of_list =
  match  list_of_list with
    [] -> []
  | h::t ->
     my_assert (List.for_all (fun l -> List.length l = List.length h) t) "zip_red";
     let rec aux iter  h lol res =
       if iter = 0
       then res
       else
	 match h with
	   [] -> res
	 | h::t -> let (res',lol) = red_heads_tails op lol in
		   aux (iter -1) t lol (res'::res)
     in
     List.rev (aux until h list_of_list [])

	      
  (* let lol = [[0;1;2];[10;11;12];[20;21;22]] *)
  (* let res = zip_red ( + ) lol *)
  (* let pas_lol = [[0;1];[10;11;12];[20;21;22]] *)
  (* let res = zip_all lol	     *)
  (* let res = zip_all pas_lol	     *)
  (* let res = zip_all [] *)
  (* module My_Utils (Bsml : INSTRUMENTED_BSML with *)
  (* 		   type logs  = *)
  (* 			  (((float list * float option * float)*int array)* *)
  (* 			     (float list * float option * float) *)
  (* 			  ) Bsml.par *)
  (* 			  * *)
  (* 			    ((unit*int array array list)*unit) ) = *)
  (*   struct  *)

  (*     let fold_procs f start = *)
  (*       let rec aux f start p = *)
  (* 	if p = 0 then *)
  (* 	  f start p *)
  (* 	else *)
  (* 	  f (aux f start (p-1)) p *)
  (*       in *)
  (*       aux f start (bsp_p -1) *)

  (*     let vect_logs () =  *)
  (*       let (logs,((_,sizes),_) ) = Bsml.get_logs () *)
  (*       in *)

  (*       ( *)
  (*       (\* 	<<  *\) *)
  (*       (\* 	 let (((seq_times , _ , last_seq) , _ ), (replicated_times , _ , last_rep) ) *\) *)
  (*       (\* 	      =  *\) *)
  (*       (\* 	    $logs$  *\) *)
  (*       (\* 	 in (last_seq::seq_times , last_rep::replicated_times) >> *\) *)
  (*       (\* , *\) *)
  (* 	sizes *)
  (*       ) *)

  
  (*     let log_suspend f = *)
  (*       if Bsml.logging_state () then *)
  (* 	(  Bsml.stop_logging (); *)
  (* 	   try *)
  (* 	     let res = f () in *)
  (* 	     Bsml.start_logging (); *)
  (* 	     res *)
  (* 	   with *)
  (* 	     exn -> *)
  (* 	     Bsml.start_logging (); *)
  (* 	     raise exn *)
  (* 	) *)
  (*       else  *)
  (* 	f () *)


  (*     let take n l = *)
  (*       let rec aux n l res = *)
  (* 	if n <=0 *)
  (* 	then List.rev res *)
  (* 	else *)
  (* 	  match l with *)
  (* 	    [] -> List.rev res *)
  (* 	  | h::t -> aux (n-1) t (h::res) *)
  (*       in *)
  (*       aux n l [] *)

  (*     let transpose mat = *)
  (*       let n = Array.length mat in *)
  (*       let m = if n > 0 then Array.length mat.(0) else 0 *)
  (*       in *)
  (*       Array.init m (fun i  -> Array.init n (fun j -> mat.(j).(i) ))  *)

  (*     (\* let test = Array.init 5 (fun i-> Array.init 3 (fun j -> (i,j))) *\) *)
  (*     (\* transpose test		       *\) *)

  (*     let compute_hp_hm_h max plus (matrix : int array array) = *)
  (*       let hp = (Array.fold_left max 0) (Array.map (Array.fold_left plus 0) matrix) *)
  (*       and hm = (Array.fold_left max 0) (Array.map (Array.fold_left plus 0) (transpose matrix)) *)
  (*       in *)
  (*       (hp,hm, max hp hm) *)
  
  (*     let get_w_h ?(until=(-1)) () = *)
  (*       log_suspend *)
  (* 	(fun () -> *)
  (* 	 let logs_loc, log_sizes = vect_logs() in *)
  (* 	 let get_seq_rep = proj logs_loc in *)
  (* 	 let all_timings = *)
  (* 	   fold_procs *)
  (* 	     (fun cumul p -> *)
  (* 	      let  seqs,reps = get_seq_rep p in *)
  (* 	      (zip_red ( +. ) [seqs; reps])::cumul *)
  (* 	     ) *)
  (* 	     ([]) *)
  (* 	 in *)
  (* 	 let reduced_timings = zip_red ~until max all_timings *)
  (* 	 in *)
  (* 	 let h = *)
  (* 	   (List.map (compute_hp_hm_h max (+)) log_sizes) *)
  (* 	 in *)
  (* 	 let h = if until < 0 then h else take (until-1) h *)
  (* 	 in *)
  (* 	 reduced_timings,h *)
  (* 	) *)
  
  (*     let nb_step () = *)
  (*       let _,((_,sizes),_) = Bsml.get_logs() in *)
  (*       List.length sizes *)

  (*     let perf f = *)
  (*       let n_step_init = nb_step() in *)
  (*       begin *)
  (* 	sync () ; *)
  (* 	let res = f () in *)
  (* 	( log_suspend ( *)
  (* 	      fun () -> *)
  (* 	      let new_steps =(nb_step() - n_step_init) in *)
  (* 	      let (time, coms) = *)
  (* 		get_w_h ~until:new_steps () *)
  (* 	      in *)
  (* 	      print_endline ((string_of_list string_of_float time)^(string_of_list (fun (_,_,h) -> string_of_int h) coms)); *)
  (* 		match  time with *)
  (* 		[] -> () *)
  (* 	      | h::t -> *)
  (* 		 let wh = zip t coms in *)
  (* 		 List.iteri (fun i  (w,(_,_,h))  -> *)
  (* 			     let i = string_of_int i *)
  (* 			     and h = string_of_int h *)
  (* 			     and w = string_of_float w *)
  (* 			     in *)
  (* 			     print_endline (" step "^i^" w :"^w^" h :"^h)) *)
  (* 			    wh; *)
  (* 		 let  h = string_of_float h in  *)
  (* 		 print_endline ("not finished step :"^h) *)
  (* 	    ); *)
  (* 	  res *)
  (* 	) *)
  (*       end *)
  

  (*     let bsp_cost = perf *)

  (*   end *)
