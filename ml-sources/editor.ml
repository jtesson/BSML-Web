(**********************************************************)
(*  Online BSML  : JS Ace editor interface                *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(**********************************************************)

(** This files provide facilities when an ace editor is loaded on the web page

TODO : the current interface is very far from complete, it just has the very basics  *)

let rawEditor () =     
  Js.Unsafe.eval_string "editor"
 	
let editor :>  
      < getValue : Js.js_string Js.t  Js.meth ;
        setValue : Js.js_string Js.t -> int -> Js.js_string Js.t  Js.meth ;
        getSession : 'a Js.meth ;
	getSelection : 'a Js.meth ;
        > Js.t
  =     
    rawEditor ()

(* type session = 'a *)
(* type selection = 'a *)
    
let editor_session  =
  editor##getSession()
	

let append s =
  let text = Js.to_string (editor##getValue()) ^ s in
  editor##setValue(Js.string text,0)

let replace text =
  editor##setValue(Js.string text,0)
	
let get_selected_js_text () =
  let selection  = editor_session##getSelection() in
  editor_session##getTextRange(selection##getRange())

let get_selected_text () =
  Js.to_string(get_selected_js_text())

let get_text () =
  Js.to_string(editor##getValue())

let bind_Command name bind_win bind_mac handler =
  (rawEditor())##command##addCommand(Js.Unsafe.eval_string("{name:"^name^",bindkey : {win:"^bind_win^",mac:"^bind_mac^"},exec:"^handler^"}"))
  
module Storage = File_storage.FileStorage (struct let name  = "Editor_store" end)

let default = "default"
					  
let save () = Storage.save_file
		~overwrite:true
		default
		(get_text ())

let init () =
  if Storage.file_exists default  then
    (
      ignore(editor##setValue(Js.string (Storage.get_file default),0));
      ignore(editor##getSelection()##clearSelection())
    )
  else
    ()


  
		
