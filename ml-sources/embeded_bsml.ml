(**********************************************************)
(*  Online BSML  : BSML env dynamically built and shared  *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(**********************************************************)
open Bsmlsig

open BsmlInstrumentation

open BsmlWithLocality

module Mktop  (P : Machine.MachineParam)
       (Instrum : functor (B:BSMLSEQ) -> (INSTRUMENTED_WITH_LEVEL with type 'a par = 'a B.par))
       =
struct
  module BspMachine = Machine.Make_Machine(P)
  module BsmlFromBsmlJs = BsmlJs.Make (BspMachine) (Instrum)
  module BsmlRaw = BsmlFromBsmlJs.BsmlRaw
  module Bsml =Instrum (BsmlRaw)
  module Stdlib = BsmlFromBsmlJs.Stdlib
  let init_output () =
    Toploop.print_out_value := BsmlRaw.print_out_value 
  let init_msg () =
    begin
      print_string "        Bulk Synchronous Parallel ML version 0.5.4";
      print_newline();
      print_string "          The BSP Machine has ";
      print_int Bsml.bsp_p;
      print_string " processor";
      if (Bsml.bsp_p>1) then print_string "s";
      print_newline();
      print_string "          o BSP parameter g = ";
      print_float  Bsml.bsp_g;
      print_string " flops/word";
      print_newline();
      print_string "          o BSP parameter L = ";
      print_float  Bsml.bsp_l;
      print_string " flops";
      print_newline();
      print_string "          o BSP parameter r = ";
      print_float  Bsml.bsp_r;
      print_string " flops/s";
      print_newline();
    end
end

(* Embedded BSML 
  stores the currently used BSML environment to serve as a link between the REPL and the exercise execution environment.
  
*)
open Shared
       
(*  Fake module just for the initialisation *)
module Mach = struct
  let p =  1
  let r =  1.
  let g =  1.
  let l =  1.
end

(*  Fake module just for the initialisation *)
module No_instrumentation = functor (B:Bsmlsig.BSML) ->
	  struct
	    let names =["empty_instrumentation"]
	    type logs_local= unit
	    let local_unit : logs_local =  ()
	    type logs_global = unit
	    let global_unit : logs_global = ()
	    let lgu = B.mkpar (fun _->local_unit) ,global_unit
				 
	    let init _ = lgu
	    let reinit _ = lgu
	    let pause _ = lgu 
	    let resume _ = lgu
	    let selective_pause _ _ = lgu
	    let selective_resume _ _ = lgu
	    let logging_global_start _  = lgu
	    let logging_global_stop _  = lgu
	    let logging_local_start _ _ _ = local_unit
	    let logging_local_stop _ _ _ _ = local_unit
	    let logging_before_sync_local _ _ _ = local_unit
	    let logging_comm_out _ _ _ _ _ = local_unit
	    let logging_comm_in  _ _ _ _ _ = local_unit
	    let logging_after_sync_local _ _ _ = local_unit
	    let logging_sync_global _ = lgu
  end

(*  Fake module just for the initialisation *)
module Make_No_Instr (Bsml : Bsmlsig.BSMLSEQ) : (INSTRUMENTED_WITH_LEVEL with type 'a par = 'a Bsml.par)  =
  struct
    include  BsmlInstrumentation.Make (Bsml) (No_instrumentation)
    let current_level () = failwith "not implemented"
  end
								      
(*  Fake module just for the initialisation *)
module Tmp = Mktop (Mach) (Make_No_Instr)

(* TODO : move in src/interface/stdlibsig.ml as a functor of BSML *)
module type STDLIB =
  sig
    module Bsml : Bsmlsig.BSMLSEQ
    module Base :
      sig
        val replicate : 'a -> 'a Bsml.par
        val parfun :
          ('a -> 'b) -> 'a Bsml.par -> 'b Bsml.par
        val parfun2 :
          ('a -> 'b -> 'c) -> 'a Bsml.par -> 'b Bsml.par -> 'c Bsml.par
        val parfun3 :
          ('a -> 'b -> 'c -> 'd) -> 'a Bsml.par -> 'b Bsml.par -> 'c Bsml.par -> 'd Bsml.par
        val parfun4 :
          ('a -> 'b -> 'c -> 'd -> 'e) -> 'a Bsml.par -> 'b Bsml.par -> 'c Bsml.par -> 'd Bsml.par -> 'e Bsml.par
        val apply2 :
          ('a -> 'b -> 'c) Bsml.par -> 'a Bsml.par -> 'b Bsml.par -> 'c Bsml.par
        val apply3 :
          ('a -> 'b -> 'c -> 'd) Bsml.par -> 'a Bsml.par -> 'b Bsml.par -> 'c Bsml.par -> 'd Bsml.par
        val apply4 :
          ('a -> 'b -> 'c -> 'd -> 'e) Bsml.par -> 'a Bsml.par -> 'b Bsml.par -> 'c Bsml.par -> 'd Bsml.par -> 'e Bsml.par
        val mask :
          (int -> bool) -> 'a Bsml.par -> 'a Bsml.par -> 'a Bsml.par
        val applyat :
          int -> ('a -> 'b) -> ('a -> 'b) -> 'a Bsml.par -> 'b Bsml.par
        val applyif :
          (int -> bool) -> ('a -> 'b) -> ('a -> 'b) -> 'a Bsml.par -> 'b Bsml.par
        val procs : int list
        val this : int Bsml.par
        val bsml_print :
          ('a -> unit) -> int -> 'a Bsml.par -> unit Bsml.par
        val parprint :
          ('a -> unit) -> 'a Bsml.par -> unit Bsml.par
        val get_one :
          'a Bsml.par -> int Bsml.par -> 'a Bsml.par
        val get_list :
          'a Bsml.par -> int list Bsml.par -> 'a list Bsml.par
        val put_one :
          (int * 'a) Bsml.par -> 'a list Bsml.par
        val put_list :
          (int * 'a) list Bsml.par -> 'a list Bsml.par
        val proj_list_pids : 'a Bsml.par -> (int * 'a) list
      end
    module Comm :
      sig
        val shift : int -> 'a Bsml.par -> 'a Bsml.par
        val shift_right : 'a Bsml.par -> 'a Bsml.par
        val shift_left : 'a Bsml.par -> 'a Bsml.par
        val totex : 'a Bsml.par -> (int -> 'a) Bsml.par
        val total_exchange :
          'a Bsml.par -> 'a list Bsml.par
        exception Scatter
        val scatter :
          ('a -> int -> 'b option) -> int -> 'a Bsml.par -> 'b Bsml.par
        val scatter_list :
          int -> 'a list Bsml.par -> 'a list Bsml.par
        val scatter_array :
          int -> 'a array Bsml.par -> 'a array Bsml.par
        val scatter_string :
          int -> string Bsml.par -> string Bsml.par
        exception Gather
        val gather :
          int -> 'a Bsml.par -> (int -> 'a option) Bsml.par
        val gather_list :
          int -> 'a Bsml.par -> 'a list Bsml.par
        exception Bcast
        val bcast_direct :
          int -> 'a Bsml.par -> 'a Bsml.par
        val bcast_totex_gen :
          ('a -> int -> 'b option) -> ((int -> 'b) -> 'c) -> int -> 'a Bsml.par -> 'c Bsml.par
        val bcast_totex_list :
          int -> 'a list Bsml.par -> 'a list Bsml.par
        val bcast_totex_array :
          int -> 'a array Bsml.par -> 'a array Bsml.par
        val bcast_totex_string :
          int -> string Bsml.par -> string Bsml.par
        val bcast_totex :
          int -> 'a Bsml.par -> 'a Bsml.par
        val scan_direct :
          ('a -> 'a -> 'a) -> 'a Bsml.par -> 'a Bsml.par
        val scan_logp :
          ('a -> 'a -> 'a) -> 'a Bsml.par -> 'a Bsml.par
        val scan_wide :
          (('a -> 'a -> 'a) ->  'a Bsml.par -> 'a Bsml.par) -> (('a -> 'a -> 'a) -> 'b -> 'b) -> ('b -> 'a) -> (('a -> 'a) -> 'b -> 'b) -> ('a -> 'a -> 'a) -> 'b Bsml.par -> 'b Bsml.par
        val scan_wide_direct :
          (('a -> 'a -> 'a) -> 'b -> 'b) -> ('b -> 'a) -> (('a -> 'a) -> 'b -> 'b) -> ('a -> 'a -> 'a) -> 'b Bsml.par -> 'b Bsml.par
        val scan_wide_logp :
          (('a -> 'a -> 'a) -> 'b -> 'b) -> ('b -> 'a) -> (('a -> 'a) -> 'b -> 'b) -> ('a -> 'a -> 'a) -> 'b Bsml.par -> 'b Bsml.par
        val scan_list_direct :
          ('a -> 'a -> 'a) -> 'a list Bsml.par -> 'a list Bsml.par
        val scan_list_logp :
          ('a -> 'a -> 'a) -> 'a list Bsml.par -> 'a list Bsml.par
        val scan_array_direct :
          ('a -> 'a -> 'a) -> 'a array Bsml.par -> 'a array Bsml.par
        val scan_array_logp :
          ('a -> 'a -> 'a) -> 'a array Bsml.par -> 'a array Bsml.par
        val fold_direct :
          ('a -> 'a -> 'a) -> 'a Bsml.par -> 'a Bsml.par
        val fold_wide :
          (('a -> 'a -> 'a) ->  'a Bsml.par -> 'a Bsml.par) -> (('a -> 'a -> 'a) -> 'b -> 'a) -> ('a -> 'a -> 'a) -> 'b Bsml.par -> 'a Bsml.par
        val fold_logp :
          ('a -> 'a -> 'a) -> 'a Bsml.par -> 'b Bsml.par
        val fold_list_direct :
          ('a -> 'a -> 'a) -> 'a list Bsml.par -> 'a Bsml.par
        val fold_list_logp :
          ('a -> 'a -> 'a) -> 'a list Bsml.par -> 'b Bsml.par
        val fold_array_direct :
          ('a -> 'a -> 'a) -> 'a array Bsml.par -> 'b Bsml.par
        val fold_array_logp :
          ('a -> 'a -> 'a) -> 'a array Bsml.par -> 'b Bsml.par
      end
    module Sort :
      sig
        exception Regular_sampling_sort
        val regular_sampling_sort_list :
          ('a -> 'a -> bool) -> 'a list Bsml.par -> 'a list Bsml.par
        val regular_sampling_sort_array :
          ('a -> 'a -> int) -> 'a array Bsml.par -> 'a array Bsml.par
      end

  end

(* Signature of a module that holds BSML and It's Stdlib *)
module type BSML_ENV = sig
  val  init_output : unit -> unit
  val init_msg     : unit -> unit
  module Bsml : INSTRUMENTED_WITH_LEVEL
  module Stdlib : STDLIB with module Bsml := Bsml
end

(* reference to the Bsml Environment *)
let bsml_env_ref : (module BSML_ENV)  ref  = ref (module Tmp : BSML_ENV)

(* **************************  *)
(* Building a BSML environment *)
(* **************************  *)
                                                 
(* Getting Dom elements with BSP parameters *)
let bsp_p_box : 'a Js.t =find_input "bsp_p"
let bsp_g_box : 'a Js.t =find_input "bsp_g"
let bsp_r_box : 'a Js.t =find_input "bsp_r"
let bsp_L_box : 'a Js.t =find_input "bsp_L"

				    
(* Functions to read BSP value (type conversion, ... *)
let get_bsp_p () = try
    int_of_string (Js.to_string bsp_p_box##value ) with _ -> 4

let float_of_string (s:string) =
  try
    float_of_string s
  with _ ->
    float_of_string (s ^ ".")
                    
let get_bsp_g () = try
    float_of_string (Js.to_string bsp_g_box##value )
  with _ -> 18.

let get_bsp_r () =
  try
    float_of_string (Js.to_string bsp_r_box##value )
  with _ -> 5000.
      
      
let get_bsp_L () =
  try
    float_of_string (Js.to_string bsp_L_box##value )
  with _ -> 72000.

 

(* replacing the Bsml environment  *)
let set_current_bsml_env (env : ( module BSML_ENV )) =
  bsml_env_ref := env

(* getting the current the Bsml environment  *)
let get_current_bsml_env () =
  !bsml_env_ref
		    
(* getting the current the Bsml module, without stdlib  *)
let get_current_bsml () =
  let module Mod_Env = (val !bsml_env_ref) in
  (module Mod_Env.Bsml : INSTRUMENTED_WITH_LEVEL)

(*  Observation is a module that aims at benching functions the same way,
    from both user and exercises provider sides  
*)
module type OBSERVATION = sig
    (** Perf returns  (resultat, (travail async, comms)) where length (travail async) = length comms +1 *)
    val perf : (unit -> 'a) -> ('a * ( float list * int list))
    val bytes_in  : 'a -> int
    val bsp_test : (unit -> 'a) -> 'a
  end

module Tmp_obs :OBSERVATION = struct 
    let perf f = failwith "perf not implemented - temporary observer should not be used (Embeded_bsml)"
    let bytes_in d = failwith  "bytes_in not implemented - temporary observer should not be used (Embeded_bsml)"
    let bsp_test f = failwith  "bsp_test not implemented - temporary observer should not be used (Embeded_bsml)"
end

let bsml_obs_ref : (module OBSERVATION)  ref  = ref (module Tmp_obs : OBSERVATION) 

let set_current_observer (obs : ( module OBSERVATION )) =
  bsml_obs_ref := obs

let get_current_observer () =
  let module Mod_Obs = (val !bsml_obs_ref) in
  (module Mod_Obs : OBSERVATION)


(** Superstep drawings is scaled *)
(* module Drawing_Container : JsInstrumentation.DRAWING_CONTAINER = JsInstrumentation.Legacy_Container (Unit) *)
module Drawing_Container : JsInstrumentation.DRAWING_CONTAINER = JsInstrumentation.Scaled_Container (Unit)
                                                               
(* Building Machine description and BSML toplevel and registering it  *)
let init_bsml () =
  let module Mac = struct
      let p =  if get_bsp_p() < 1 then 1 else get_bsp_p()
      let r =  get_bsp_r()
      let g =  get_bsp_g()
      let l =  get_bsp_L()
    end
  in
  let module BsmltopEnv = ( Mktop (Mac)  (JsInstrumentation.Make (Drawing_Container)) )
  in
  (* Building Observer interface (uses the JsInstrumentation)  *)
  let module Observation =
    struct
      open Common_utils
      open BsmltopEnv
      open Bsml
      open Stdlib.Base
      let sync () = ignore (proj (mkpar (fun _ ->  0)) 0)

      let fold_procs f start =
        let rec aux f start p =
          if p = 0 then
	    f start p
          else
	    f (aux f start (p-1)) p
        in
        aux f start (bsp_p -1)
            
      let vect_logs () =
        let ((logs),(((_,sizes),_),_) ) = Bsml.get_logs ()
        in
        (parfun (fun logs ->
                    let (((seq_times , _ , last_seq) , _ ), (replicated_times , _ , last_rep) ),_
                      =
                      logs
                    in (last_seq::seq_times , last_rep::replicated_times)) logs
        ,
          sizes
        )

          
      let log_suspend f =
        if Bsml.logging_state () then
          (  Bsml.stop_logging ();
	     try
	       let res = f () in
	       Bsml.start_logging ();
	       res
	     with
	       exn ->
	       Bsml.start_logging ();
	       raise exn
          )
        else
          f ()


      (* let test = Array.init 5 (fun i-> Array.init 3 (fun j -> (i,j))) *)
      (* transpose test		       *)

      let compute_hp_hm_h max plus (matrix : int array array) =
        let hp = (Array.fold_left max 0) (Array.map (Array.fold_left plus 0) matrix)
        and hm = (Array.fold_left max 0) (Array.map (Array.fold_left plus 0) (transpose matrix))
        in
        (hp,hm, max hp hm)
          
      let get_w_h ?(until=(-1)) () =
        log_suspend
          (fun () ->
            let logs_loc, log_sizes = vect_logs() in
            let get_seq_rep = proj logs_loc in
            let all_timings =
	      fold_procs
	        (fun cumul p ->
	          let  seqs,reps = get_seq_rep p in
	          (zip_red ( +. ) [seqs; reps])::cumul
	        )
	        ([])
            in
            let reduced_timings = zip_red ~until max all_timings
            in
            let h =
	      (List.map (compute_hp_hm_h max (+)) log_sizes)
            in
            let h = if until < 0 then h else take (until-1) h
            in
            reduced_timings,h
          )
          
      let nb_step () =
        let _,(((_,sizes),_),_) = Bsml.get_logs() in
        List.length sizes


      let perf f =
        let n_step_init = nb_step() in
        begin
          (* print_endline ("nb step "^string_of_int n_step_init); *)
          if not ( logging_state () ) then Bsml.start_logging() else ();
          sync () ;
          let res =
	    try f ()
	    with exn -> failwith ("exception during perf test"^Printexc.to_string exn)
          in
          log_suspend (
	      fun () ->
	      try
	        (* print_endline ("nb step "^string_of_int (nb_step())); *)
	        let new_steps =(nb_step() - n_step_init) in
	        let (time, comms) =
	          get_w_h ~until:new_steps ()
	        in
	        (res, (time, List.map (fun (_,_,h) -> h) comms))
	      with exn ->
	        failwith ("exception during perf gathering "^Printexc.to_string exn)
	    )
        end

      let bsp_cost f =
        let (res,(time, comms)) = perf f in
        log_suspend
          ( fun () ->
	    match  time with
	      [] -> ()
	    | h::t ->
	       let wh = zip t comms in
	       List.iteri (fun i  (w,h)  ->
		   let i = string_of_int i
		   and h = string_of_int h
		   and w = string_of_float w
		   in
		   print_endline (" step "^i^" w :"^w^" h :"^h))
		          wh;
	       let  h = string_of_float h in
	       print_endline ("not finished step :"^h)
          );
        res
          

      let bsp_test = bsp_cost
		       
      let bytes_in = Tools.size_of_data
    end
  in
  let bsmltopenv  = (module
		       BsmltopEnv
		       : BSML_ENV
		    )
    
  in
  set_current_bsml_env bsmltopenv
  ;
  set_current_observer (module Observation : OBSERVATION)
                       


    
