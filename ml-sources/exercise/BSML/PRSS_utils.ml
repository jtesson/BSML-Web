(** {1  Outils pour la définition et le test du tri par échantillonnage régulier}  *)

module BsmlEnv = (val Embeded_bsml.get_current_bsml_env())
open BsmlEnv
      
open Bsml

(** {3  Outils pour la définition par échantillonnage régulier}  *)

    (** Trie les éléments d'une liste à l'aide des fonctions de prédéfinies dans la bilbiothèque standard Ocaml.
Équivalent à [List.sort Pervasives.compare lst]
     *)
    let sort lst = List.sort Pervasives.compare lst

    (**/**)	       
    (**  Prend les n premiers éléments d'une liste  *)
    let take n lst =
      let rec aux lst i res =
	if n = i then List.rev res else
	  match lst with
	    [] ->  List.rev res
	  | h :: t -> aux t (i+1) (h::res)
      in
      aux lst 0 []

    (**  Élimine les n premiers éléments d'une liste  *)
    let rec drop n lst =
      if n = 0 then lst
      else 
	match lst with
	  [] ->  []
	| h :: t -> drop (n-1) t

    (** Suppression de l'encapsulation de type option.
 Renvoie [a] si appliqué à [Some a]
 Lève l'exception [Failure "no_some on None"] si appliqué à [None]
     *)
    let no_some : 'a option -> 'a = function None -> failwith "no_some on None" | Some v -> v
    (**/**)	       
											      
											      
    (** Prélèvement d'échantillons dans une liste à intervalles réguliers.

  [echantillons  nb_echantillon lst] prélève [nb_echantillon] dans la liste [lst]
     *)
    let echantillons nb_echantillon lst =
      let len = List.length lst in
      assert (len > (nb_echantillon)) ;
      assert (nb_echantillon>0) ;
      let drop_mod m n lst= if m > 0 then
			      drop (n+1) lst
			    else
			      drop n lst
      in
      let rec aux modulo nb_e ~nb_drop lst res =
	match lst with
	  [] -> assert false
	| h :: t ->
	   if nb_e = 0 then
	     h::res
	   else
	     aux modulo (nb_e-1) ~nb_drop (drop_mod (modulo-nb_e) nb_drop lst) (h::res)
      in
      let nb_drop =  (len/(nb_echantillon+1))
      and modulo= (len mod (nb_echantillon+1))
      in
      List.rev (aux modulo (nb_echantillon-1) nb_drop (drop_mod modulo nb_drop lst) [])      
	       
    (** Prends les éléments d'une liste du début jusqu'au premier élément supérieur ou égal à e (exclu).

Exemple : elements_until 3 [ 0; 2;1 ;3 ;2 ;4] = [0;2;1]

     *)
    let elements_until ~e ~lst =
      let rec aux lst e res =
	match lst with
	  []  -> List.rev res
	| h :: t when h >= e -> List.rev res
	| h :: t ->  aux t e (h::res)
      in
      aux lst e []

	  
    (** Prends les éléments d'une liste à partir du premier élément supérieur ou égal à e (inclu) jusqu'à la fin de la liste.

Exemple : elements_starting_from 3 [ 0; 2;1 ;3 ;2 ;4] = [3 ;2 ;4]

     *)
    let rec  elements_starting_from e lst =
      match lst with
	[]  -> []
      | h :: t when h >= e -> lst
      | h :: t ->  elements_starting_from e t
					  
    (** Construction d'une liste séquentielle répliquée à partir d'un vecteur parallèle de liste.
Coût BSP : 

   w = O(1) 

   +H = (bsp_p-1)*max(sz_of_lst(i)) où sz_of_lst(i) est la taille de la liste au processeur i.

   +1*L
     *)
    let list_of_par_list plst =
      List.flatten (List.map (proj plst) Stdlib.Base.procs)

    (** {3  Fonctions utiles pour faire des tests}   *)


    (** Construit la chaine de caractère construite à partir d'une liste.

 Si [limit] est >=0, seulement [limit] éléments seront affichés
 
 [limit], [start], [sep] et [stop] sont optionnels.

 [string_of_data] doit être une fonction convertissant une donnée en chaine de caractère.

exemples : 
   - [ string_of_list string_of_int [0;1;3] ] donne la chaîne [ "[0;1;3]" ]

   - [ string_of_list ~start:"( " ~sep:"," string_of_int [0;1;3] ]
donne la chaîne [ "( 0,1,3\]" ]

     *)
    let string_of_list ?( limit= -1 ) ?(start="[") ?(sep=";") ?(stop="]") string_of_data lst =
      let rec str_of_list_aux str lst =
	match lst with
	| [] -> failwith "unreachable state"
	| [d] -> str ^(string_of_data d)
	| h::t -> str_of_list_aux (str ^ (string_of_data h)^sep) t
				  
      in
      let stop = if limit >= 0 then "..."^stop else stop in
      let lst = take limit lst in 
      match  lst with
	[] -> start^stop
      | h::t -> (str_of_list_aux start lst)^stop

    (**/**)	       

    let soi = string_of_int
    let sol = string_of_list
    (**/**)	       
		


		
    (** Liste des entiers de 0 à n inclus  *)
    let from_0_to_n n =
      let rec aux i list =
	if i = 0  then 0::list
	else aux (i-1) (i::list)
      in aux n []

    (** Liste des entiers de n à 0 inclus  *)
    let from_n_to_0 n =
      List.rev (from_0_to_n n)
	       

    (** Liste de taille aléatoire (bornée par le premier paramètre) contenant des entiers aléatoires entre 0 et la borne donnée en deuxième paramètre  *)
    let random_int_list_fixed_max ~max_size ~max_value =
      let size = Random.int  max_size in
      let rec aux size res =
	if size <= 0 then res
	else aux (size - 1) (Random.int max_value :: res)
      in
      aux size []

    (** Liste alétoire de taille inférieure à [10 000], valeurs  inférieure à [10 000]*)
    let random_int_list ()= random_int_list_fixed_max 10000 10000
						      
    (** Liste alétoire de taille inférieure à [100], valeurs  inférieure à [1 000]*)
    let random_int_list_small ()= random_int_list_fixed_max 100 1000

							    
    (**/**)	       
    let part_at p l=
      let len = List.length l in
      let m = len mod bsp_p in
      let size = (len / bsp_p) + if p >= m then 0 else 1
      in take size (drop (size * p + (if p >= m then m else p)) l)
    (**/**)	       
	      
    (** Distribution d'une liste de manière équilibrée sur les processeurs.   *)
    let distribute lst = << part_at $this$ lst  >> 

    (** Vecteur parallèle contenant la distribution de la listes des entiers de 21 à 0.

Sur 4 processeurs, le vecteur obtenu sera
[< [21; 20; 19; 18; 17; 16], [14; 13; 12; 11; 10; 9], [9; 8; 7; 6; 5],
    [4; 3; 2; 1; 0] > ]
     *)
    let test_list = distribute (from_n_to_0  21)

			       
(** Verification de PRSS:
[check_par_sort par_sort plist = list_of_par_list (par_sort plist) = sort (list_of_par_list plist)]
 *)
    let check_par_sort ~par_sort ~plist = list_of_par_list (par_sort plist) = sort (list_of_par_list plist)

(** 
(*********************************)

 *)
