module BsmlEnv = (val Embeded_bsml.get_current_bsml_env())
open BsmlEnv
      
open Bsml

let name = "applying_parallel_vector"
let title = function
    "fr" -> "Application dans les vecteurs parallèles"
  | "en" |  _ -> "Functional application in  parallel vectors"
              
let description  =
  function "fr" ->
           " Dans cet exercice, nous allons construire des fonctions produisant du calcul parallèles.<br/>
            <p> Un calcul parallèle à lieu lors de l'évaluation d'une expression dans un vecteur parallèle. Ainsi, <code> let plus_un v = << $v$ + 1 >></code> ou 
            <code> let plus_un v = apply << fun v -> v + 1 >> v </code> sont deux façons d'écrire une fonction ajoutant en parallèle 1 à tous les élément d'un vecteur parallèle
            </p>
            
	    "
         | "en" ->
            "In this exercice you will build functions that actually perform parallel computation.<br/>
            <p> A parallel computation occurs while an expression within a parallel vector is evaluated. Thus, <code> let plus_un v = << $v$ + 1 >></code> or 
            <code> let plus_un v = apply << fun v -> v + 1 >> v </code> are two different style to write a function that adds 1 to all elements of a parallel vector of integers. All incrementations are done in parallel.
            </p>

             "             
             
let forall_proc predicat =  
  let rec aux i b  = if i = bsp_p then b else  aux (i+1) (b&&predicat i )
  in aux 0 true

let failing_proc predicat =
  let rec aux i lst =
    if i = bsp_p then lst else
      if predicat i then  aux (i+1) lst
      else aux(i+1) (i::lst)
  in List.rev (aux 0 [])
  
let exists_proc predicat =
  let rec aux i  = if i = bsp_p  then false else predicat i || aux (i+1)
  in aux 0 

let vect_eq ?(string_of=fun _ -> "") v1 v2 =
	      (* fun () -> *)
	      let pv1 = proj v1 and pv2 = proj v2 in
	      let predicat =  (fun p -> pv1 p = pv2 p) in
	      if
		forall_proc predicat
	      then
		(* ("égalité de vecteur", *)
		 Loader.Success
		(* ) *)
	      else
		let fail = List.map
			     (fun i ->
			      "\n at processor "^(string_of_int i)^" : "^
				(string_of (pv1 i))^"!="^(* "&ne;"^ *)
				  (string_of (pv2 i))^"\n"
			     )
			     (failing_proc predicat)
		in
		let expl = My_misc.string_of_list ~start:"" ~stop:""(fun i ->i ) fail
		in
		(* ("égalité de vecteur", *)
		 Loader.Fail ("different values at "^expl)
		(* ) *)
		  

let vect_eq_int =vect_eq ~string_of:string_of_int

let vect_eq_int_list =vect_eq ~string_of:(My_misc.string_of_list ~limit:10 string_of_int)

let vect_eq_str =vect_eq ~string_of:(fun i -> "\""^i^"\"")

let ( => ) b1 b2 = not b1 || b2

let apply_vect_int f1 f2 =
  let data = << Random.int 100000 >> in
  let v1 = f1 data and v2 = f2 data in
  vect_eq_int v1 v2

	      
let apply_vect_str f1 f2 =
  let data = << string_of_int ( Random.int 100000) >> in
  let v1 = f1 data and v2 = f2 data in
  vect_eq_str v1 v2

let apply_vect_int_str f1 f2 =
  let data = << Random.int 100000 >> in
  let v1 = f1 data and v2 = f2 data in
  vect_eq_str v1 v2

let random_int_list ()=
  let max_size = 10000 in
  let size = Random.int  max_size in
  let rec aux size res =
    if size <= 0 then res
    else aux (size - 1) (Random.int max_size :: res)
  in
  aux size []
      
let apply_vect_list f1 f2 =
  let data = << random_int_list () >> in
  let v1 = f1 data and v2 = f2 data in
  vect_eq v1 v2
	  
let apply_vect_list_int f1 f2 =
  let data = << random_int_list () >> in
  let v1 = f1 data and v2 = f2 data in
  vect_eq_int v1 v2

let apply_vect_int_list f1 f2 =
  let data = << random_int_list () >> in
  let v1 = f1 data and v2 = f2 data in
  vect_eq_int_list v1 v2

let apply_list_vect_int_list f1 f2 =
  let data =  My_misc.from_0_to_n (3*bsp_p + (bsp_p/2)) in
  let v1 = f1 data and v2 = f2 data in
  vect_eq_int_list v1 v2
		   
let fun_vect f1 f2 =
  let funct = (fun i -> i*2) in 
  let data = << Random.int 100000 >> in
  let v1 = f1 funct data and v2 = f2 funct data in
  vect_eq_int v1 v2

let fun_vect2 f1 f2 =
  let funct = (fun i j-> j,i) in 
  let data1 = << Random.int 100000 >> in
  let data2 = << Random.int 100000 >> in
  let v1 = f1 funct data1 data2 and v2 = f2 funct data1 data2 in
  vect_eq v1 v2

let is_random_list_gen _ f =
  let data1 = proj (f ()) in
  let data2 = proj (f ()) in
  let predicat =  (fun p ->
		   let d1 =(data1 p) and d2 =data2 p in
		   let l1 =List.length d1 and l2 = List.length d2 in
		   l1 != l2 || d1 != d2
		  )
  in
  if
    forall_proc predicat
  then
    Loader.Success
  else
    Loader.Fail ("deux générations produisent le même résultat")
  
(**
    :: Description :: fr => <p>une fonction qui prend en paramètre une fonction <code>f</code> et un vecteur <code>v</code> et qui applique f en tout points du vecteur v :
</p>
<p>
<code>parfun f &lt;v<sub>0</sub> ; v<sub>1</sub> ; ...; v<sub>p-1</sub> &gt;=&lt; f v<sub>0</sub> ;f v<sub>1</sub> ; ...; f v<sub>p-1</sub></p>
    
    :: Description :: en => 
    <p>A function that take a function <code>f</code> and a vector <code>v</code> as parameter  
      and which applies f to all data in vector v :
    </p>
    <p>
    <code>parfun f &lt;v<sub>0</sub> ; v<sub>1</sub> ; ...; v<sub>p-1</sub> &gt;=&lt; f v<sub>0</sub> ;f v<sub>1</sub> ; ...; f v<sub>p-1</sub></p>
    :: Hint        :: 
    :: testeur     :: fun_vect
 *)
let parfun f v = << f $v$ >>


(**
    :: Description :: fr => <p>une fonction qui prend en paramètre une fonction à deux paramètres <code>f</code> et deux vecteurs <code>v</code>  et <code>x</code> et qui applique <code>f</code> en tout points aux deux vecteurs :</p>
<p>
<code>parfun2 f &lt;v<sub>0</sub> ; v<sub>1</sub> ; ...; v<sub>p-1</sub> &gt; &lt;x<sub>0</sub> ; x<sub>1</sub> ; ...; x<sub>p-1</sub> &gt;=&lt; f v<sub>0</sub> x<sub>0</sub> ;f v<sub>1</sub> x<sub>1</sub> ; ...; f v<sub>p-1</sub> x<sub>p-1</sub>
</p>
    :: Description :: en => <p>A that take a function <code>f</code> of arity 2 and two vectors <code>v</code> and  <code>x</code> as parameter and which applies <code>f</code> to all points of the two vectors:</p>
<p>
<code>parfun2 f &lt;v<sub>0</sub> ; v<sub>1</sub> ; ...; v<sub>p-1</sub> &gt; &lt;x<sub>0</sub> ; x<sub>1</sub> ; ...; x<sub>p-1</sub> &gt;=&lt; f v<sub>0</sub> x<sub>0</sub> ;f v<sub>1</sub> x<sub>1</sub> ; ...; f v<sub>p-1</sub> x<sub>p-1</sub>
</p>
    :: Hint        :: 
    :: testeur     :: fun_vect2
 *)
let parfun2 f v x = << f $v$ $x$ >>

		      
(**
    :: Description :: fr => <p>qui ajoute pid à la valeur entière se trouvant au processeur pid</p>
    :: Description :: en => <p>which adds <code>pid</code> to the integer stored at <code>pid</code></p>
    :: Hint        :: 
    :: testeur     :: apply_vect_int
 *)
let add_pid  v = << $v$ +  $this$ >>

		      
(**
    :: Description :: fr => <p>qui soustrait, sur chaque processeur la valeur dans un vecteur <code>v</code> au nombre total de processeurs <code>bsp_p</code></p>
    :: Description :: en => <p>which substract the value stored in a vector <code>v</code> to the number of processors  <code>bsp_p</code> </p>
    :: Hint        :: 
    :: testeur     :: apply_vect_int
 *)
let bsp_p_minus  v = << bsp_p - $v$ >>

		      
(**
    :: Description :: fr => <p>qui concatène la chaîne " procs" à la valeur contenue en chaque processeur </p>
    :: Description :: en => <p>which concatanates  the string  " procs" to the string in a vector <code>v</code> </p>
    :: Hint        :: 
    :: testeur     :: apply_vect_str
 *)
let cat_processeurs  v = <<  $v$ ^" procs" >>
		      
(**
    :: Description :: fr => <p>qui convertit en chaîne de caractère l'entier trouvé en pid et lui concatène la chaîne de caractères " chars in "^(string_of_int pid) </p>
    :: Description :: en => <p> which converts the integers of a vector into strings concatenated to " chars in "^(string_of_int pid) </p>
    :: Hint        :: 
    :: testeur     :: apply_vect_int_str
 *)
let int_at_processeurs  v = <<  (string_of_int $v$) ^" chars in "^(string_of_int $this$) >>

(**
    :: Description :: fr => <p>qui applique partout la fonction List.length</p>
    :: Description :: en => <p>which aplies List.length on all elements of the parallel vector</p>
    :: Hint        :: 
    :: testeur     :: apply_vect_list_int
 *)
let par_length  v = <<  List.length $v$ >>

(**
    :: Description :: fr => <p>qui applique partout la fonction List.rev</p>
    :: Description :: en => <p>which applies  List.rev on all elements of the parallel vector</p>
    :: Hint        :: 
    :: testeur     :: apply_vect_list
 *)
let par_rev  v = <<  List.rev $v$ >>

(**
    :: Description :: fr => <p>Implémentez une fonction qui génère un vecteur de listes de longueur aléatoire contenant des entiers aléatoires  </p>
    :: Description :: en => <p>Write a function which generate a vector of lists of random length containing random integers </p>
    :: Hint        :: Random.int 10000
    :: testeur     :: is_random_list_gen
 *)
let random_list_vector  () = <<   random_int_list () >>

			       
(**
    :: Description :: fr => <p>Implémentez une fonction qui trie localement un vecteur de listes </p>
    :: Description :: en => <p>Write a function which sorts locally the lists in a vector</p>
    :: Hint        :: fr => utilisez compare et List.sort
    :: Hint        :: en => use compare and List.sort
    :: testeur     :: apply_vect_int_list
 *)
let local_sort v = <<   List.sort compare $v$ >>


let part_at p l=
  let len = List.length l in
  let m = len mod bsp_p in
  let size = (len / bsp_p) + if p >= m then 0 else 1
  in My_misc.take size (My_misc.drop (size * bsp_p + (if p >= m then m else p)) l)


(**
    :: Description :: fr => <p>Implémentez une fonction qui distribue une liste dans un vecteur parallèle
 de façon équilibrée </p> 
<p> [1;2;3;4;5;6] sera distribué sur 4 processeurs ainsi : &lt;&lt; [1;2] ; [3;4] ; [5]; [6] &gt;&gt; </p> 
    :: Description :: en => <p>Write a function which distributes evenly a list in a parallel vector</p> 
<p> [1;2;3;4;5;6] will be distributed on 4 processors as : &lt;&lt; [1;2] ; [3;4] ; [5]; [6] &gt;&gt; </p> 
    :: Hint        :: fr => Faites une fonction auxiliaire pour découper la liste.
    :: Hint        :: en => write a sequential auxiliary function to cut the list.
    :: testeur     :: apply_list_vect_int_list
 *)
		     
let distribute lst = << part_at $this$ lst  >>

