module BsmlEnv = (val Embeded_bsml.get_current_bsml_env())
open BsmlEnv
open Bsml

module Observation =  (val Embeded_bsml.get_current_observer())
open Observation

open My_misc
let name = "simple_comm"
let title = function "fr" -> "Fonctions de communication simples" | "en" -> "Simple communication functions"

let description  = function "fr" -> " <p>Dans cet exercice, nous allons utiliser la fonction proj pour implanter des schémas de communication simples.</p>
<p> La fonction <code>proj: 'a par -> int -> 'a </code>  est une communication all-to-all :
		<code>proj v</code> effectue un échange total des contenus dans le vecteur. 
		    Le résultat est une fonction répliquée qui, à un numéro de processeur, associe la valeur présente à ce processeur pour le vecteur v.
<p> Pour <code>v=<\"a\";\"b\";\"c\";\"d\"></code>, <code>proj v</code> construit la fonction <code> function 0 -> \"a\"|1 -> \"b\"| 2->\"c\"|3 ->\"d\"</code></p>
</p>
"
                          | "en" -> " <p>In this exercise, we will use the proj function to  implement simple communication schemes.</p>
<p> The function <code>proj: 'a par -> int -> 'a </code>  is an  all-to-all communication:
		<code>proj v</code> perform a total exchange of the values in the vector. 
		    The final result is a replicated function which, for a given proc number, associate the value in the vector at that processor.
</p>
<p> For <code>v=<\"a\";\"b\";\"c\";\"d\"></code>, <code>proj v</code> build the function <code> function 0 -> \"a\"|1 -> \"b\"| 2->\"c\"|3 ->\"d\"</code></p>
"
module Ex_utils = Exercises_utils.Exo_utils(Bsml)
open  Ex_utils
(**
    :: Description :: fr => Écrire une  fonction qui retourne la valeur détenue par le processeur 0 pour le vecteur passé en paramètre.<br/>
 Appliquée au vecteur  < 0 ; 1 ; 2 ; 3 > le résultat devrait être 0.<br/>
Une attention particulière devra être portée sur la quantité d'information transférée. H devrait être égale à p*s où p est le nombre de processeur et s est la taille de la donnée présente au processeur 0.
    :: Description :: en => Write a function which returns the value at processor 0 in the input vector.<br/>
 Applied to  < 0 ; 1 ; 2 ; 3 > the result should be 0.<br/>
One should be particularly careful on the amount of data transferred. H should be equal to  p*s where p is the number of processor and s the size of the data at processor 0.
    :: Hint        :: fr => Pour plus d'efficacité, il faut construire un vecteur contenant une petite valeur à tous les processeurs différents de 0. (Le transfert de la valeur None du type <code>option</code> est gratuit)
    :: Hint        :: en => For efficiency, It is necessary to build a vector having a small data in all processors but the first. Transferring the value None of type <code>option</code> is free)
    :: testeur     :: full_test_f_int_list_par_to_int_list
    ::   
 *)
let first_element v =  My_misc.no_some (Bsml.proj << if $this$ = 0 then Some $v$ else None  >> 0)

(**
    :: Description :: fr => Idem mais pour la valeur contenue au dernier processeur.
    :: Description :: en => Same as befor but for the value stored at last processor.

    :: Hint        ::
    :: testeur     :: full_test_f_int_rev_list_par_to_int_list
    ::   
 *)
let last_element v =  My_misc.no_some (Bsml.proj << if $this$ = bsp_p-1 then Some $v$ else None  >> (bsp_p-1))
				      
(**
    :: Description ::  fr => Utilisez <code>proj</code> et <code>Sort.list</code> pour écrire une fonction de tri telle  que 
<code>sort_seq  &lt;v<sub>0</sub> ; v<sub>1</sub> ; ...; v<sub>p-1</sub> &gt;</code> donne un résultat identique au tri de la liste  <code>[ v<sub>0</sub> ; v<sub>1</sub> ; ...; v<sub>p-1</sub> ] </code>
    :: Description ::  en => Use <code>proj</code> and <code>Sort.list</code> to write a sorting function such that
<code>sort_seq  &lt;v<sub>0</sub> ; v<sub>1</sub> ; ...; v<sub>p-1</sub> &gt;</code> gives the same result as sorting the list  <code>[ v<sub>0</sub> ; v<sub>1</sub> ; ...; v<sub>p-1</sub> ] </code>
    :: Hint        :: <code>List.map</code>, <code>list of procs</code>,  <code>proj</code>
    :: testeur     ::  full_test_f_int_rd_par_to_int_list
 *)
let sort_seq v = let projed = proj v in
		 List.sort compare (List.map projed all_procs)

			   

let rec rev_app l1 l2 =
  match l1 with
    [] -> l2
  | h :: t -> rev_app t (h::l2)
	    
(**
    :: Description ::  fr => Écrire une fonction de tri telle  que  <code>sort_seq_list  &lt;l<sub>0</sub> ; v<sub>1</sub> ; ...; l<sub>p-1</sub> &gt;</code> donne un résultat identique au tri de la liste  <code>l<sub>0</sub> @ l<sub>1</sub> @ ...@ l<sub>p-1</sub> </code>
    :: Description ::  en => Write a sorting function such that <code>sort_seq_list  &lt;l<sub>0</sub> ; v<sub>1</sub> ; ...; l<sub>p-1</sub> &gt;</code> gives the same result as sorting the liste <code>l<sub>0</sub> @ l<sub>1</sub> @ ...@ l<sub>p-1</sub> </code>
    :: Hint        :: <code>List.fold_left</code>,  <code>list of procs</code>,  <code>proj</code>
    :: testeur     :: full_test_f_int_rd_list_par_to_int_list
 *)
let sort_seq_list v =
  let projed = proj v in
  let all = List.fold_left ( rev_app ) [] (List.rev_map projed all_procs)
  in
  List.sort compare all


(**
    :: Description :: fr => Écrire une fonction de tri telle  que  <code>sort_seq_list_merge  &lt;l<sub>0</sub> ; v<sub>1</sub> ; ...; l<sub>p-1</sub> &gt; </code> donne un résultat identique au tri de la liste  <code>l<sub>0</sub> @ l<sub>1</sub> @ ...@ l<sub>p-1</sub> </code> en utilisant List.merge et la fonction sort_par de l'exercise "application dans les vecteurs parallèles"
    :: Description :: en =>  Write a sorting function such that <code>sort_seq_list  &lt;l<sub>0</sub> ; v<sub>1</sub> ; ...; l<sub>p-1</sub> &gt;</code> gives the same result as sorting the liste <code>l<sub>0</sub> @ l<sub>1</sub> @ ...@ l<sub>p-1</sub> </code> using <code>List.merge</code> and the <code>sort_par</code> function of the exercise "Functional application in parallel vectors"
    :: Hint        :: fr => Fonctions utiles : List.fold_left,  list of procs,  proj. Faites une application partielle de proj au vecteur parallèle afin de ne faire qu'une fois la syncronisation. 
    :: Hint        :: en => Useful functions : <code>List.fold_left</code>,  <code>list of procs</code>,  <code>proj</code>. 
Use a partial application of proj on then paralle vector in order to synchronise only once. 
    :: testeur     :: full_test_f_int_rd_list_par_to_int_list
 *)
let sort_seq_list_merge v =
  let projed = proj << List.sort compare $v$ >> in
  List.fold_left ( fun a b ->  List.merge compare b a ) [] (List.rev_map projed all_procs)

let div2 n = if n <= 0. then 0. else n /. 2.
let rfloats ()= << Random.float 1000. >>
let div2par v = << div2 $v$ >>

let max_val proj_v = 
    List.fold_left max 0. (List.map proj_v all_procs)

let vect_eq_f_f_par_to_f_par f1 f_user =
  let v1 = Random.float 1000. and v2 = << Random.float 1000. >> in
  vect_eq_float (f1 v1 v2)  (f_user v1 v2) 

		
			
let trace name f v v' =
  print_endline ("start " ^name);
  let res = try f v v' with exn -> failwith (name^" failed : "^Printexc.to_string exn) in
  ignore(<< print_endline ("value"^(sof $res$)) >> );
  print_endline ("stop " ^name);
  res
  
let full_test_f__f_par_to_f_par (f1 : float -> float par -> float par) (f_user : float -> float par -> float par) =
  (* let test = perf (fun ()  -> << 1 >> ) in *)
  let v = Random.float 2. and v' = << Random.float 1000. >> in
  (* let _ = try ignore (( fun () ->  f_user v v') () ) with exn -> failwith "fuser failed" in *)
  let  f1' () =  f1 v v'
  and  f_user' () = f_user v v' 
  (* in let _ = try ignore (f_user' () ) with exn -> failwith "fuser failed 2"  *)
  in
  (* try *)
    compare_all ~string_of:string_of_float vect_eq_float f1' f_user'
  (* with exn -> failwith ("perfvect_eq_f "^(Printexc.to_string exn)) *)
		
(**
    :: Description ::  fr => Écrire une fonction qui applique une division par deux jusqu'à ce que toutes les valeurs du vecteur soient toutes inférieures à epsilon.<br/>
Vous utiliserez :<ul><li> une fonction <code>div2 : float &gt; float</code> qui divise tout nombre >= 0 par 2 et qui rend 0 pour tout nombre négatif.</li>
<li> une fonction <code>div2par : float par &gt; float par</code> qui applique partout div2.</li> <ul>
    :: Description ::  en => Write a function that divide by 2 the values of a vector until they all are less than epsilon.<br/>
You will use :<ul><li> a function <code>div2 : float &gt; float</code> which divides any number >= 0 by 2 and returns 0 for negative numbers.</li>
<li> a function <code>div2par : float par &gt; float par</code> which applies div2 at all processor.</li>
 <ul>
    :: Hint        :: 
    :: testeur     :: vect_eq_f_f_par_to_f_par
 *)
let rec div_while epsilon floats =
  if max_val (proj floats) > epsilon then
    div_while epsilon (div2par floats)
  else
    floats

      
(**
    :: Description ::  fr => Écrire une fonction qui implémente la même fonctionalité que précédement en ne faisant qu'une super-étape.
    :: Description ::  en => Write a function that implements the same thing as the previous one but in only one superstep.
    :: Hint        :: 
    :: testeur     :: full_test_f__f_par_to_f_par
 *)
let rec optimal_div_while epsilon floats =
  let rec local_div floa =
    (* print_string "."; *)
    if  floa > epsilon then
      local_div (div2 floa)
    else
      floa
  in << local_div $floats$ >>
      
