module BsmlEnv = (val Embeded_bsml.get_current_bsml_env())
open BsmlEnv
open Bsml
       
open My_misc
module Observation =  (val Embeded_bsml.get_current_observer())
open Observation
let name = "comm_put"
let title = function "fr" -> "Schémas de communication " |"en"|_ -> "Communication Schemes "

let description  = function "fr" -> " <p>Dans cet exercice, nous allons utiliser la fonction put pour implanter des schémas de communication variés.</p>
<p> La fonction <code>put : ( int -> 'a) par -> ( int -> 'a) par </code>  est une fonction de  communication totale (all-to-all)  où chaque processeur envoie une valeur à chaque processeur. La valeur envoyée est spécifique au processeur destination.
Pour utiliser put, il faut construire un vecteur de fonction, chaque fonction encodant les valeurs à envoyer. <br/>
Le résultat sera un vecteur de fonctions, construite par put, qui encodent les valeurs reçues.	
Appliquer à 0 la fonction disponible au processeur 1 permet de savoir quelle valeur le processeur 1 à reçu du processeur 0.
</p>
"
                           | "en" -> " <p>In this exercise, we will use the <code>put</code> function to implement various communication schemes.</p>
<p> The function <code>put : ( int -> 'a) par -> ( int -> 'a) par </code>  is a total communcation function (all-to-all) where each processor send a value to each processor. 
The value is specific to the destination.
To use  <code>put</code>, one has to build a vector of functions, each function encoding the values to be sent. <br/>
The result will be a vector of functions built by <code>put</code>, which encodes the received values.	
Applied to 0, the function at processor 1 gives access to the value received by processor 1 from processor.
</p>
                                               "
                           | lg -> failwith ("lang \""^lg^"\" not supported")
module Ex_utils = Exercises_utils.Exo_utils(Bsml)
open  Ex_utils

let full_test_bcast f1 f_user =
  full_test  vect_eq_int_list
	     (Random.int (bsp_p-1) , << random_int_list () >>)
	     (fun (i,v) -> f1 i v)
	     (fun (i,v) -> f_user  i v)

let eq_int v1 v_user  =
  eq ~string_of:string_of_int (v1) (v_user)

let full_test_fold f1 f_user =
  full_test  eq_int
	     (<< Random.int 1000 >>)
	     (fun v -> f1 ( + ) 0 v)
	     (fun v -> f_user  ( + )   0 v)
let full_test_shift f1 f_user =
  full_test  vect_eq_int_list
	     (<< random_int_list () >>)
	     f1 
	     f_user
	     
let no_some = function None -> failwith "no_some on None" | Some v -> v
	     
(**
    :: Description :: fr => Définir la fonction <code>bcast_proj</code> telle que
 <code>bcast i &lt; x<sub>0</sub> ; x<sub>1</sub> ,..., x<sub>p−1</sub> &gt; = 
&lt; x<sub>i</sub> ; x<sub>i</sub> ,..., x<sub>i</sub> &gt;. 
Cette première version sera définie à l’aide de la primitive proj.
<BR/>
Estimez le temps nécessaire à l'évaluation de  <code>bcast_proj i x </code> (par rapport à la taille des éléments de x).
    :: Description :: en => Define a function <code>bcast_proj</code> such that  <code>bcast i &lt; x<sub>0</sub> ; x<sub>1</sub> ,..., x<sub>p−1</sub> &gt; = 
&lt; x<sub>i</sub> ; x<sub>i</sub> ,..., x<sub>i</sub> &gt;. 
This first version will e defined using  <code>proj</code>.
<br/>
Estimate the evaluation time of <code>bcast_proj i x </code> (compared to the size of the element x). 

    :: Hint        :: fr => Pour plus d'efficacité, il faut construire un vecteur contenant une petite valeur à tous les processeurs différents de 0. (Le transfert de la valeur None du type <code>option</code> est gratuit)
    :: Hint        :: en => For efficiency, It is necessary to build a vector having a small data in all processors but the first. Transferring the value None of type <code>option</code> is free)
    :: testeur     :: full_test_bcast
    ::   
 *)
let bcast_proj i v =  let res= no_some (Bsml.proj << if $this$ = i then Some $v$ else None  >> i) in
		 << res >>

(**
    :: Description :: Faites une seconde version à l’aide de la primitive de communication put. Estimez le temps nécessaire à l’évaluation de  <code>bcast_put i x</code>.
    :: Description :: Write a second version using <code>put</code>. Estimate the evaluation time of <code>bcast_put i x</code>.
    :: Hint        ::
    :: testeur     :: full_test_bcast
    ::   
 *)
let bcast_put i v =
  let comms = Bsml.put << fun _ -> if $this$ = i then Some $v$ else None  >>
  in
  <<  My_misc.no_some ($comms$ i) >>

let part_at p l=
  let len = List.length l in
  let m = len mod bsp_p in
  let std_size = (len / bsp_p) in
  let size = std_size + if p <= m then 1 else 0 in
  (* print_endline ( " My_misc.take "^(soi size )^"(My_misc.drop ("^(soi std_size)^" * "^(soi p)^" + (if "^(soi p)^" >= "^(soi m)^" then "^(soi m)^" else "^(soi p)^")) l)"); *)
  My_misc.take size (My_misc.drop (std_size * p + (if p >= m then m else p)) l)


(**
    :: Description ::  fr => <p>Définir <code>bcast2phases</code> tel que 
<code>bcast2phases from  &lt; bloc<sub>0</sub> ; bloc<sub>1</sub> ,..., bloc<sub>p−1</sub> &gt; = &lt; bloc<sub>from</sub> ; bloc<sub>from</sub> ,..., bloc<sub>from</sub> &gt; en utilisant l’algorithme BSP
à deux phases (le plus efficace quand le nombre de processeur est grand).
Estimez et mesurez le temps nécessaire pour  évaluer  <code>bcast2phases from blocs</code>.
</p>
    :: Description :: en =>  <p>Define <code>bcast2phases</code> such that  
<code>bcast2phases from  &lt; bloc<sub>0</sub> ; bloc<sub>1</sub> ,..., bloc<sub>p−1</sub> &gt; = &lt; bloc<sub>from</sub> ; bloc<sub>from</sub> ,..., bloc<sub>from</sub> &gt; 
using the two phases broadcast BSP algorithm (most efficient when the number of processor is large).
Estimate and  measure the evaluation time of <code>bcast2phases from blocs</code>.
</p>
    :: Hint        :: fr => Réutiliser les fonctions auxiliares définie pour la fonction distribute de l'exercice "Application dans les vecteurs parallèles"
    :: Hint        :: en => Reuse the auxiliary functions of the distribute function from the exercise "Functional application in  parallel vectors"
    :: testeur     ::  full_test_bcast
 *)
	 
let bcast2phases  from v = 
  let comm_fun  lst src dest = 
      if src = from 
      then Some (part_at dest lst)
      else None
  in
  let received = put << comm_fun $v$ $this$ >> in
  let splitted = << no_some ($received$ from) >> in
  let all_data= List.rev_map (proj splitted) all_procs in
  << List.fold_left (fun a b -> b @ a ) [] all_data >>
  
				      
(**
    :: Description ::  fr => <p>Définir <code>fold_proj</code> tel que <code>fold_proj op e  &lt; x<sub>0</sub> ; x<sub>1</sub> ,..., x<sub>p−1</sub> &gt;</code> soit la (op)-réduction des x<sub>i</sub> avec l’ élément neutre e.
</p>
<p>Rappel : L' élément neutre e d'un opérateur op est tel que (op e a) = (op a e) = a.</p>

<p> Faites une première version à l'aide de la primitive de communication proj. Estimez
le temps nécessaire à l'évaluation de fold (op) x.
</p>
    :: Description :: en => <p>Define <code>fold_proj</code> such that <code>fold_proj op e  &lt; x<sub>0</sub> ; x<sub>1</sub> ,..., x<sub>p−1</sub> &gt;</code>  is the (op)-réduction of all x<sub>i</sub> with neutral element e.
</p>
<p>Reminder : The neutral elemetL' élément neutren e of an operator op is such that  (op e a) = (op a e) = a.</p>

<p> Write a first version using <code>proj</code>. 
Estimate the evaluation time of fold (op) x.
</p>
    :: Hint        :: 
    :: testeur     ::  full_test_fold
 *)
let fold_proj op  e v = let projed = proj v in
		 List.fold_left (fun acc p -> op acc (projed p) ) e  all_procs

let ( **. ) a b = a ** b
let ( ** ) a b = int_of_float (float_of_int a ** (float_of_int b))

(**
    :: Description ::  fr =>
<p> Faites une seconde version à l'aide de la primitive de communication put. 
Estimez le temps nécessaire à l'évaluation de fold (op) x.
</p>
    :: Description ::  en =>
<p> Write a second version using <code>put</code>. 
Estimate the evaluation time of fold (op) x.
</p>
    :: Hint        :: fr => Commencez par ecrire les fonctions booléenne determinant si un processeur doit envoyer/recevoir à une étape donnée, et des fonction calculant la destination.
    :: Hint        :: en => Start by writing boolean functions that determine if a processor has to send/receive at a given superstep and functions to compute the destinators.
    :: testeur     ::  full_test_fold
 *)
let fold_put op  e v = 
  let sender shift to_p  = to_p + (shift/2) in 
  let is_receiver    i shift = (i mod shift = 0)  in
  let is_sender      i shift = (i mod (shift/2) = 0)&& not (is_receiver i shift) in
  let is_dest      src shift dest =  dest = src - (shift/2) in
  let one_step_comm shift v =
    let comm_fun  v src dest = 
      if is_sender src shift &&
	   is_dest src shift dest
      then Some v else None
    in
    put << comm_fun $v$ $this$ >>
  in
  let rec aux shift v =
    if shift >= 2*bsp_p then
      proj v 0
    else
      let received =
	let all_recv = (one_step_comm shift v) in
	<<
	let sender_proc = (sender shift $this$) in
	if sender_proc < bsp_p && is_receiver $this$ shift then
	 try
	  no_some ($all_recv$ sender_proc)
	 with exn -> failwith ("failure at "^(soi $this$)^" send proc "^(soi sender_proc))
	else
	  e
	  >>
      in
      let new_data =
	<<
	if is_receiver $this$ shift then	
	    op $v$  $received$
	else
	  e
	 >>
      in
      aux (shift * 2) new_data
  in aux 2 v

	 
	 
