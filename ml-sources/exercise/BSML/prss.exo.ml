open Embeded_bsml
open PRSS_utils			  

module BsmlEnv = PRSS_utils.BsmlEnv
module Bsml = BsmlEnv.Bsml
module Stdlib = BsmlEnv.Stdlib

open Stdlib
       
open Bsml

module Bsml_testers = Testers.Bsml_testers (Bsml)
open  Bsml_testers

	
let name = "PRSS"
let title = function "en" -> "Parallel Regular Sampling Sort "  | "fr" -> "Tri parallèle par echantillonage régulier" | lg -> failwith ("lang \""^lg^"\" not supported")


let description  = function "fr" -> " <p>
Le tri parallèle par échantillonnage régulier consiste à sélectionner des échantillons représentatifs des valeurs détenues par chaque processeur;
de les échanger afin que les processeurs se mettent d'accord sur la destination de leur données; puis d'échanger les fragment de listes triés et de les assembler localement(merge-sort).
<p>
Les échantillons sont sélectionnés sur chaque processeur dans la liste triée des valeurs détenues. 
Généralement, bsp_p échantillons sont séléctionnés par chaque processeur, à intervalles réguliers dans la liste de façon à représenter le mieux possible la diversité des valeurs présentes.
</p>
<p> Après un échange total des échantillons, la liste de tous les échantillons (bsp_p*bsp_p) est utilisée pour sélectionner bsp_p valeur pivots. 
Ces valeurs-pivot sont ensuite utilisées pour déterminer localement en chaque processeur le découpage de la liste en bsp_p fragment à distribuer sur les processeurs.
Le processeur <var>i</var> recevra les valeurs supérieures à <var> pivot<sub>i-1<sub></var>   est inférieures à <var>pivot<sub>i<sub></var>.
Pas de borne inférieure pour le processeur 0, ni de borne supérieure pour le processeur de plus haut rang.
</p>

<p> Le tri se fait donc en deux super-étapes + une phase de calcul asynchrone :</p>
   <ul>
       <li>Tri asynchrone; calcul des échantillons; échange des échantillons </li>
       <li>Échange des fragments de liste  </li>
       <li>Construction de la liste locale par assemblage des fragments de liste </li>
   </ul>
<p>
Pour implémenter les fonctions qui suivent, vous utiliserez le fichier <a target=\"_blank_\" href=\"filesys/exercise/BSML/PRSS_utils.ml\"> PRSS_utils.ml</a>
contenant les fonctions utilitaires décrite dans <a class=\"ml-source-ref\" target=\"_sources\" href=\"filesys/exercise/BSML/PRSS_utils.html\"> la documentation</a>.
</p>
                                    
                                    "
                          | "en" ->" <p>
The Parallel Regular Sampling Sort consists in gathering representative samples of values stored on each processor; 
                                   exchanging them to get a consensus on the final destination of data; 
                                   and then exchanging  fragments of sorted lists and locally merge them (merge-sort).
<p>
The samples are selected on each processors in the local list (sorted). 
Usually, bsp_p samples are selected on each processor, at regular intervals in the list so as to best represents the diversity of values.
</p>
<p> After a total exchange of samples, the list of all samples (bsp_p*bsp_p) is used to select bsp_p pivots. 
These pivots are then used to cut the local list at each processor into  bsp_p fragment to distribute among processors.
The processor <var>i</var> will receive the values greater than <var> pivot<sub>i-1<sub></var>  and lower than  <var>pivot<sub>i<sub></var>.
No lower bound for the processor 0, nor upper bound for the processor of higher rank.
</p>

<p> the sorting is thus done in two supersteps + an asynchronous computation phase :</p>
   <ul>
       <li> Asynchronous sort; samples computation; samples exchange</li>
       <li> Computation and exchange of list fragments</li>
       <li> Computation of local result by merging incoming list fragments</li>
   </ul>
<p>
To implement the functions of this exercise, tou can use  the file  <a target=\"_blank_\" href=\"filesys/exercise/BSML/PRSS_utils.ml\"> PRSS_utils.ml</a>
 which brings utilities functions described in  <a class=\"ml-source-ref\" target=\"_sources\" href=\"filesys/exercise/BSML/PRSS_utils.html\"> the documentation</a>.
</p>

                                    "
                          | lg -> failwith ("lang \""^lg^"\" not supported")
(**
    :: Description :: fr => <p>Cette fonction implémente la phase de tri asynchrone. Elle prend un vecteur de liste, et retourne un vecteur contenant les listes triées.  </p>
    :: Description :: en => <p>This function implements the asynchronous local sorting. It takes a vector of lists and return a vector of sorted lists.  </p>
    :: testeur     :: 
 *)
let tri_local lst = << sort $lst$ >>

(**
    :: Description :: fr => <p>Cette fonction implémente la construction et l'échange des  échantillons.</p> 
<p> L'entrée de cette fonction est un vecteur parallèle de liste, que l'on supposera triées. <br/>
 Cette fonction calcul une liste répliquée de (bsp_p - 1) échantillons . </p>
    :: Description :: en => <p>This function implements the computation and exchange of samples.</p> 
<p> The input is a parallel vector of lists that we suppose sorted. <br/>
 It computes a replicated list of (bsp_p - 1) samples. </p>
    :: Hint        :: 
    :: testeur     :: 
    ::   
 *)
let echantillons_par par_list   =
  let locals = << if List.length $par_list$ < bsp_p then 
		$par_list$ 
		else 
		echantillons (bsp_p-1)  $par_list$
		>>
  in
  let all_echantillons =
    sort  (list_of_par_list locals)
  in
  echantillons (bsp_p-1) all_echantillons


	       
			   
(**
    :: Description :: fr => <p>Cette fonction implémente la phase d'échange de fragments de liste.<p>
<p> Elle  prend en parametre un vecteur de listes localement triées, une liste de (bsp-p-1) échantillons, et retourne un vecteur de listes tel que:
 <ul><li> tous les éléments de liste du vecteur initial sont présents dans le vecteur final avec la même caridinalité (preservation des éléments)</li>
 <li> sur tous les processeurs, les listes sont triées</li>
 <li> pour tout processeur i, les éléments de la liste locale sont supérieurs à l'échantillon (i-1) et inférieurs à l'échantillon i.</li>
</ul>
</p>
 
<p>La fonction doit donc vérifier l'égalité suivante pour toute liste </code>echantillons_lst</code> d'échantillon valide :
<br/><code> List.sort compare (list_of_par_list plst) = list_of_par_list ( sort_par_aux plst echantillons_lst) </code>
</p>
    :: Description :: en => <p> This function  implements the list fragments exchange.<p>
<p> It takes as parameter the vector of locally sorted lists and the list of (bsp-p-1) samples, and returns a list vector such as :
 <ul><li> all elements in a list in the initial vector are present in the final vector, with the same cardinality (elements preservation)</li>
 <li> on all processors, lists are sorted</li>
 <li> for each processor i, the elements of the local list are greater than the sample (i-1) and lower than the sample i.</li>
</ul>
</p>
 
<p>The fucntion should satisfy the following equation for any input list and any valid sample list </code>sample_lst</code>:
<br/><code> List.sort compare (list_of_par_list plst) = list_of_par_list ( sort_par_aux plst sample_lst) </code>
</p>
    :: testeur     :: 
 *)
let sort_par_aux plst echantillons =
  let comms = put << fun dest -> 
		   if dest = 0 then 
		     elements_until (List.nth echantillons 0) $plst$
		   else if dest = bsp_p -1 then 
		     elements_starting_from (List.nth echantillons (bsp_p - 2)) $plst$
		   else 
		     elements_until
		     (List.nth echantillons dest)
		     (elements_starting_from (List.nth echantillons (dest-1)) $plst$)
		   >>
  in
  let  received = << (List.map $comms$ Stdlib.Base.procs)>>
  in
  << sort (List.flatten $received$) >>

    
(**
    :: Description :: fr => En combinant les fonctions précédentes, implémentez le tri par échantillonnage régulier.
    :: Description :: en => Using previously implemented functions, you now can write the parallel regular sampling sort.
    :: testeur     :: 
 *)
let sort_par plst =
  let locally_sorted =    tri_local plst
  in
  let echantillons =  echantillons_par locally_sorted
  in
  sort_par_aux locally_sorted echantillons 



(* let rd_plst i = << random_int_list_fixed_max i i >> *)
(* let plst = rd_plst 100 *)
(* let test = sort_par plst *)
		     
(** :: Description :: fr => Afin d'observer la répartition des données après le tri,  implémentez une fonction qui prend un vecteur parallèle de listes et retourne une liste d'entier. Le <var>i<small>ème</small></var> entier étant la taille de la liste au processeur <var>i</var>
    :: Description :: en => In order to watch the data distribution after the sorting, write a functions which takes a vector of lists and return a list of integer, the <var>i<small>ème</small></var> integer being the size of the list at processor <var>i</var>
    :: testeur     :: 
  *)
let histogramme plst = List.map (proj << List.length $plst$ >>) Stdlib.Base.procs

(* let h_plst = histogramme plst				 *)
(* let h_test = histogramme test *)

(* let l_plst = *)
(*   List.fold_left max 0 h_plst - *)
(*   List.fold_left min (List.hd h_plst) h_plst *)
(*   ,   *)
(*   List.fold_left (+) 0 h_plst *)

(* let l_test = *)
(*   List.fold_left max 0 h_test - *)
(*     List.fold_left min (List.hd h_test) h_test *)
(* 	     ,   *)
(*     List.fold_left (+) 0 h_test *)

(* let test_sort plst = sort (list_of_par_list plst) = list_of_par_list (sort_par plst) *)

(* let _ = test_sort (rd_plst 10000) *)
