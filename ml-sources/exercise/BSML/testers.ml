(**********************************************************)
(*  Online BSML  : Testers for building  exercises        *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(**********************************************************)

open Bsmlsig
open PRSS_utils
       
module Bsml_testers (Bsml : BSML) =
  struct 
    open Bsml
	   
    let forall_proc predicat =
      let rec aux i  = if i = bsp_p then true else predicat i && aux (i+1)
      in aux 0

    let failing_proc predicat =
      let rec aux i  =
	if i = bsp_p then [] else
	  if predicat i then  aux (i+1)
	  else i:: (aux(i+1))
      in aux 0
	     
    let exists_proc predicat =
      let rec aux i  = if i = bsp_p  then false else predicat i || aux (i+1)
      in aux 0

    let vect_eq ?(string_of=fun _ -> "_") v1 v2 =
      (* fun () -> *)
      let pv1 = proj v1 and pv2 = proj v2 in
      let predicat =  (fun p -> pv1 p = pv2 p) in
      if
	forall_proc predicat
      then
	(* ("égalité de vecteur", *)
	Loader.Success
	  (* ) *)
      else
	let fail = List.map
		     (fun i ->
		      "("^(string_of_int i)^", "^
			(string_of (pv1 i))^"=/="^
			  (string_of (pv2 i))^")"
		     )
		     (failing_proc predicat)
	in
	let expl = My_misc.string_of_list (fun i ->i ) fail
	in
	(* ("égalité de vecteur", *)
	Loader.Fail ("different values at "^expl)
    (* ) *)
		    

    let vect_eq_int =vect_eq ~string_of:string_of_int

    let vect_eq_int_list =vect_eq ~string_of:(My_misc.string_of_list ~limit:10 string_of_int)

    let vect_eq_str =vect_eq ~string_of:(fun i -> "\""^i^"\"")

    let vect_eq_float =vect_eq ~string_of:string_of_float

    let apply_vect_int f1 f2 =
      let data = << Random.int 100000 >> in
      let v1 = f1 data and v2 = f2 data in
      vect_eq_int v1 v2
		  
    let apply_vect_str f1 f2 =
      let data = << string_of_int ( Random.int 100000) >> in
      let v1 = f1 data and v2 = f2 data in
      vect_eq_str v1 v2

    let apply_vect_int_str f1 f2 =
      let data = << Random.int 100000 >> in
      let v1 = f1 data and v2 = f2 data in
      vect_eq_str v1 v2

    let random_int_list ()=
      let max_size = 10000 in
      let size = Random.int  max_size in
      let rec aux size res =
	if size <= 0 then res
	else aux (size - 1) (Random.int max_size :: res)
      in
      aux size []
	  
    let apply_vect_list f1 f2 =
      let data = << random_int_list () >> in
      let v1 = f1 data and v2 = f2 data in
      vect_eq v1 v2
	      
    let apply_vect_list_int f1 f2 =
      let data = << random_int_list () >> in
      let v1 = f1 data and v2 = f2 data in
      vect_eq_int v1 v2

    let apply_vect_int_list f1 f2 =
      let data = << random_int_list () >> in
      let v1 = f1 data and v2 = f2 data in
      vect_eq_int_list v1 v2

    let apply_list_vect_int_list f1 f2 =
      let data =  My_misc.from_0_to_n (3*bsp_p + (bsp_p/2)) in
      let v1 = f1 data and v2 = f2 data in
      vect_eq_int_list v1 v2
		       
    let fun_vect f1 f2 =
      let funct = (fun i -> i*2) in 
      let data = << Random.int 100000 >> in
      let v1 = f1 funct data and v2 = f2 funct data in
      vect_eq_int v1 v2

    let fun_vect2 f1 f2 =
      let funct = (fun i j-> j,i) in 
      let data1 = << Random.int 100000 >> in
      let data2 = << Random.int 100000 >> in
      let v1 = f1 funct data1 data2 and v2 = f2 funct data1 data2 in
      vect_eq v1 v2

    let is_random_list_gen _ f =
      let data1 = proj (f ()) in
      let data2 = proj (f ()) in
      let predicat =  (fun p ->
		       let d1 =(data1 p) and d2 =data2 p in
		       let l1 =List.length d1 and l2 = List.length d2 in
		       l1 != l2 || d1 != d2
		      )
      in
      if
	forall_proc predicat
      then
	Loader.Success
      else
	Loader.Fail ("deux générations produisent le même résultat")


    let vect_eq_f_f_par_to_f_par f1 f_user =
      let v1 = Random.float 1000. and v2 = << Random.float 1000. >> in
      vect_eq_float (f1 v1 v2)  (f_user v1 v2) 
		    

    let eq ?(string_of=fun _ -> "_") v1 v2  =
      if
	v1 = v2
      then
	Loader.Success
      else
	Loader.Fail (string_of v1 ^"not equal to "^(string_of v2))

		    
    let eq_f_int_par_to_int f1 f_user  =
      let v = << Random.int 1000 >> in
      eq ~string_of:string_of_int (f1 v) (f_user v)

    let eq_f_int_par_to_int_list f1 f_user  =
      let v = << Random.int 1000 >> in
      eq ~string_of:(string_of_list string_of_int) (f1 v) (f_user v)

    let eq_f_int_list_par_to_int_list f1 f_user  =
      let v = << random_int_list () >> in
      eq ~string_of:(string_of_list string_of_int) (f1 v) (f_user v)
	 
    let eq_f_int_list_par_to_int_list_small f1 f_user  =
      let v = << random_int_list_small () >> in
      eq ~string_of:(string_of_list string_of_int) (f1 v) (f_user v)

    let eq_int_list v1 v_user  =
      eq ~string_of:(string_of_list string_of_int) v1 v_user
	 
  end

    

    

    

    

    
