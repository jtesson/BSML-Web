module BsmlEnv = (val Embeded_bsml.get_current_bsml_env())
open BsmlEnv
      
open Bsml

let name = "building_parallel_vector"
let title = function "fr" -> "Construction de vecteurs parallèles" |
                     "en"|_ -> "Building parallel vectors"

let description  = function
    "fr" ->
    " Dans cet exercice, nous allons contruire des vecteurs parallèles simples.<br/>
 Nous commencerons par des vecteurs contenant la même constante partout, puis nous utiliserons le pid pour différencier les valeurs présentes sur chaque processeur.

<p>		    Un vecteur se contruit à l'aide de la syntaxe <code>&lt;&lt; exp &gt;&gt;</code>, où l'expression exp sera évauée sur chaque processeur. L'expression exp peut contenir n'importe quel expression Ocaml, elle peut aussi contenir une expression de type <code>$v$</code> où v est l'identifiant d'un vecteur parallèle défini au préalable. <code>$v$</code> permet d'accéder au contenu du vecteur v pour le processeur courant.
</p>
<p>
		    L'expression <code>$this$</code> est un mot-clef particulier permettant d'accéder au pid du processeur sur lequel l'expression est évaluée.
</p>
<p>

		    Tout ce qui est évalué en dehors des chevrons <code><< >></code> est du code répliqué (niveau global) qui sera diponible depuis tous les processeurs et aura la même valeur partout.
</p>
<p>
  <emph>Détail technique</emph> :
</p>
<p>

		    Bien que le système actuel ne l'interdise pas techniquement, il est interdit d'utiliser au niveau global des fonctions non deterministe (par ex. Random.int) car l'implantation distribuée ne peut pas garantir que la valeur de l'expression soit la même sur tous les processeurs. 
</p>
<p>
		    L'expression <code>let r = Random.int 20 in << r >> </code>n'est pas correcte bien que l'implémentation actuelle de BSML ne permette pas de la rejetter.
</p>
<p>

		    De même il ne faut pas modifier un mutable défini au niveau global depuis le niveau local.
</p>
<p>
		    L'expression <code>let r = ref 0 in << r:=r+1 >></code>  n'est pas correcte bien que l'implémentation actuelle de BSML ne permette pas de la rejetter.
</p>

		    "
   |"en"| _ ->
     " In this exercise, we will build simple parallel vectors.<br/>

      We will start with vectors storing the same constant everywhere, then we will use the pid to distinguish the values on each processor.
      <p> A vector is build with the syntax <code>&lt;&lt; exp &gt;&gt;</code>, where exp will be evaluated on each processor. the expression exp can be any Ocaml expression, it also can contain <code>$v$</code> where v is an identifier previously associated to a parallel vector. <code>$v$</code> gives access to the content of v on the processor where the expression is executed.
      </p>
      <p>
      The expression <code>$this$</code> is a keyword to get the pid of the  the processor where the expression is executed.
      </p>
      <p>
      
      Anything evaluated outside of the  <code><< >></code> notation is replicated code (global level) for which the computed value will be available, and will have the same value, on all processors.
                               </p>
                               <p>
                               <emph>Technical detail</emph> :
                               </p>
                               <p>

		               While the current type system cannot technically forbid it, it is not allowed to use non-deterministic fucntions (i.e. Random.int) at global level.
      The distributed implementations would not be able to guarantee that the expression would be evalutated to the same value everywhere (we say that it breaks the replicated coherency of the code. 
      </p>
      <p>
      The expression <code>let r = Random.int 20 in << r >> </code> is not correct, despite the inability of current BSML implementation to reject this code.
      </p>
      <p>
      It not allowed to modify a mutable defined at global level from the local level.
      </p>
      <p>
      The expression <code>let r = ref 0 in << r:=r+1 >></code>  is not correct, despite the inability of current BSML implementation to reject this code..
      </p>

      "
let forall_proc predicat =
  let rec aux i  = if i = bsp_p then true else predicat i && aux (i+1)
  in aux 0

let failing_proc predicat =
  let rec aux i  =
    if i = bsp_p then [] else
      if predicat i then  aux (i+1)
      else i:: (aux(i+1))
  in aux 0
  
let exists_proc predicat =
  let rec aux i  = if i = bsp_p  then false else predicat i || aux (i+1)
  in aux 0

let vect_eq ?(string_of=fun _ -> "_") v1 v2 =
	      (* fun () -> *)
	      let pv1 = proj v1 and pv2 = proj v2 in
	      let predicat =  (fun p -> pv1 p = pv2 p) in
	      if
		forall_proc predicat
	      then
		(* ("égalité de vecteur", *)
		 Loader.Success
		(* ) *)
	      else
		let fail = List.map
			     (fun i ->
			      "("^(string_of_int i)^", "^
				(string_of (pv1 i))^"=/="^
				  (string_of (pv2 i))^")"
			     )
			     (failing_proc predicat)
		in
		let expl = My_misc.string_of_list (fun i ->i ) fail
		in
		(* ("égalité de vecteur", *)
		 Loader.Fail ("different values at "^expl)
		(* ) *)
		  

let vect_eq_int =vect_eq ~string_of:string_of_int

let vect_eq_str =vect_eq ~string_of:(fun i -> i)


let ( => ) b1 b2 = not b1 || b2
			       
(**
    :: Description :: fr => Construire un vecteur parallèle contenant 3 en chaque processeur.
 Sur une machine à 4 processeur le résultat devrait être < 3 ; 3 ; 3 ; 3 >
    :: Description :: en => Build a parallel vector storing 3 at every  processor.
 On a  4 processors machine the result should be < 3 ; 3 ; 3 ; 3 >
    :: Generateurs :: []
    :: Hint        :: fr => La syntaxe <code><< exp >></code> permet de construire un vecteur parallèle contentant le resultat de l'evaluation de l'expression <code>exp</code> en chaque processeur.
    :: Hint        :: en => the <code><< exp >></code> build a parallel vector that store the result of the evaluation of the expression <code>exp</code> at each processor.
    :: testeur     :: vect_eq_int
    ::   
 *)
let trois = << 3 >>

(**
    :: Description :: fr => Construire le vecteur des identifiants de processeur.
 Sur une machine à 4 processeur le résultat devrait être < 0 ; 1 ; 2 ; 3 >
    :: Description :: en => Build a parallel vector of processor identifiers.
 On a  4 processors machine the result should be  < 0 ; 1 ; 2 ; 3 >
    :: Generateurs :: []
    :: Hint        :: fr => Dans un vecteur, on accède au pid courrant à l'aide de <code>$this$</code> .
    :: Hint        :: en => the pid can be accessed in a vector using <code>$this$</code> .
    :: testeur     :: vect_eq_int
 *)
let pids = << $this$ >>

	     
(**
    :: Description :: fr => Construire le vecteur  qui contient les identifiants de processeur mais en ordre inverse.
 Sur une machine à 4 processeur le résultat devrait être < 3 ; 2 ; 1 ; 0 >
    :: Description :: en => Build a parallel vector of processor identifiers in reverse order.
 On a  4 processors machine the result should be < 3 ; 2 ; 1 ; 0 >
    :: Generateurs :: []
    :: Hint        :: fr => Le nombre total de processeur est donné par <code>bsp_p</code>.
    :: Hint        :: en => The total number of processor is given by the primitive <code>bsp_p</code>.
    :: testeur     :: vect_eq_int
 *)
let inv_pids = << bsp_p - $this$ -1 >>


(**
    :: Description ::  Construire le vecteur  qui contient la chaîne de caractères correspondant au nombre de processeur.
 Sur une machine à 4 processeur le résultat devrait être < "4" ; "4" ; "4" ; "4" >
    :: Description :: en => Build a parallel vector which store the string corresponding to the number of processors.
 On a  4 processors machine the result should be < "4" ; "4" ; "4" ; "4" >
    :: Generateurs :: []
    :: Hint        :: fr => La fonction string_of_int peut être utile ici.
    :: Hint        :: en => The function <code>string_of_int</code> could help here.
    :: testeur     :: vect_eq_str
 *)
let p_string = << string_of_int bsp_p >>

(**
    :: Description ::  fr => Construire le vecteur qui contient partout une liste contenant 0.
 Sur une machine à 4 processeur le résultat devrait être < [0]; [0] ; [0] ;  [0] >
    :: Description :: en => Build a parallel vector of list containing 0.
 On a  4 processors machine the result should be  < [0]; [0] ; [0] ;  [0] >
    :: Hint        :: fr => la liste vide est construite avec l'expression [] .
    :: Hint        :: en => the empty list is written [] .
    :: testeur     :: vect_eq
 *)
let empty_lists  = << [0] >>

	 
(**
    :: Description ::  fr => Construire le vecteur qui contient pour chaque processeur pid la liste des entiers de 0 à pid.
 Sur une machine à 4 processeur le résultat devrait être < [0]; [0;1] ; [0;1;2] ;  [0;1;2;3] >
    :: Description :: en => Build a parallel vector the list of integers from 0 to pid at processor pid.
 On a  4 processors machine the result should be  < [0]; [0;1] ; [0;1;2] ;  [0;1;2;3] >
    :: Hint        :: fr => Ecrire une fonction from_0_to_n : int -> int list qui prend un paramètre n et retourne la liste des éléments de 0 à n. 
    :: Hint        :: en => First, you should write a function <code>from_0_to_n : int -> int list</code> which takes an integer <code>n</code> as parameter and builds the list of integer from 0 to n. 
    :: testeur     :: vect_eq 
 *)

let to_pids = << My_misc.from_0_to_n $this$ >>
