(*pp $PP *)

(**********************************************************)
(*  Online BSML  : Utilities to write exercises           *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(**********************************************************)
open Bsmlsig
module Exo_utils (Bsml : BSML) =
  struct
    module Observation =  (val Embeded_bsml.get_current_observer())
    open Bsml
    open My_misc
    open Observation
	   
    let all_procs = from_0_to_n (bsp_p-1)
				
    let pids = << $this$ >>
    let forall_proc predicat =
      let rec aux i  = if i = bsp_p then true else predicat i && aux (i+1)
      in aux 0

    let failing_proc predicat =
      let rec aux i  =
	if i = bsp_p then [] else
	  if predicat i then  aux (i+1)
	  else i:: (aux(i+1))
      in aux 0
	     
    let exists_proc predicat =
      let rec aux i  = if i = bsp_p  then false else predicat i || aux (i+1)
      in aux 0
    let sof = string_of_float
    let soi = string_of_int

    let vect_eq ?(string_of=fun _ -> "_") v1 v2 =
      (* fun () -> *)
      let pv1 = proj v1 and pv2 = proj v2 in
      let predicat =  (fun p -> try pv1 p = pv2 p
				with exn ->
				  print_endline ("fail to test predicat "^(soi p));
				  failwith ("fail to test predicat "^(soi p)))
      in
      try 
	if
	  forall_proc predicat
	then
	  (* ("égalité de vecteur", *)
	  Loader.Success
	    (* ) *)
	else
	  let fail = List.map
		       (fun i ->
			"("^(string_of_int i)^", "^
			  (string_of (pv1 i))^"=/="^
			    (string_of (pv2 i))^")"
		       )
		       (failing_proc predicat)
	  in
	  let expl = My_misc.string_of_list (fun i ->i ) fail
	  in
	  (* ("égalité de vecteur", *)
	  Loader.Fail ("different values at "^expl)
      with exn ->
	try
	  if v1 = v2 then
	    Loader.Success
	  else
	    Loader.Fail ("vector seems different but I can't say where")
	with exn -> 
	  failwith ("vect_eq "^(Printexc.to_string exn))
    (* ) *)

    let vect_eq_int =vect_eq ~string_of:string_of_int

    let vect_eq_float =vect_eq ~string_of:string_of_float

    let vect_eq_str =vect_eq ~string_of:(fun i -> i)

			     
    let vect_eq_int_list =vect_eq ~string_of:(fun il -> string_of_list string_of_int il)

    let foi = float_of_int
		
    let comp_perf ?(facteur= 0.8) ?(epsilon= 0. ) f1 f_user =
      let (v1,(w_f1,h_f1)): 'a*(float list *int list) =
	try perf f1 with exn -> failwith ("comp_perf_1 "^(Printexc.to_string exn))
      and (v2,(w_f_user,h_f_user)):'a*(float list*int list) =
	let _ = try f_user ()  with exn -> failwith ("comp_perf_2 "^(Printexc.to_string exn)) in
	try perf f_user  with exn -> failwith ("comp_perf_3 "^(Printexc.to_string exn))
      in
      let sstep1 =(List.length w_f1)
      and sstep2 =(List.length w_f_user)
      and w_1 = (List.fold_left ( +. ) 0. w_f1)
      and w_2 = (List.fold_left ( +. ) 0. w_f_user)
      and h_1 = (List.fold_left ( + ) 0 h_f1)
      and h_2 = (List.fold_left ( + ) 0 h_f_user)
      in
      let sstep_comp =  sstep2 - sstep1
      and w_comp =  int_of_float (((w_2 *. facteur -. (epsilon *. (foi sstep2))) -. w_1) /. w_1)
      and h_comp = h_2 - h_1
      in
      let () =
	print_endline ("sstep "^(soi sstep1)^" "^(soi sstep2)^" : "^soi sstep_comp);
	print_endline ("work "^(sof w_1)^" "^(sof w_2)^" : "^soi w_comp);
	print_endline ("H "^(soi h_1)^" "^(soi h_2)^" : "^soi h_comp)
      in
      sstep_comp,
      w_comp,
      h_comp,
      (* 0, 0, 0, *)
      (v1,v2)


    let no_comp v1 v2 = Loader.Success
			  
			  
    let compare_all ?(string_of=fun _ -> "")  ?(facteur= 0.8) ?(epsilon= 0. ) comp_res f1 f_user =
      let _ = try ignore (f_user () ) with exn -> failwith "fuser failed comp_all" in 
      let (sstep,w,h,(r1, r2)) =
	try
	  comp_perf ~facteur ~epsilon f1 f_user
	with exn ->
	  failwith ("comp_perf failure : "^(Printexc.to_string exn))
      in
      try
	Loader.Progress [ ("super-étape" , if sstep > 0 then
					     Loader.Fail  (soi sstep ^
							     " super-étapes de plus que la solution")
					   else Loader.Success);
			  ("travail asynchrone" ,
			   if w > 0 then
			     Loader.Fail "plus de travail asyncrone que la solution"
			   else Loader.Success);
			  ("échange de valeurs" ,
			   if h > 0 then
			     Loader.Fail  (soi h^" octets communiqués en plus  par rapport à la solution")
			   else Loader.Success);
			  ("validité fonctionnelle" ,
			   (* try *)
			   comp_res r1 r2
			  (* with exn -> *)
			  (*   try if r1 = r2 then Loader.Success else Loader.Fail " "  with exn ->  *)
			  (*   failwith ("vect_eq in compare "^(Printexc.to_string exn)) *)
			  )
			]
      with exn ->  failwith ("compare_all "^(Printexc.to_string exn))

    let eq_comm f1 f_user data =
      match compare_all no_comp (fun () -> f1 data)  (fun () -> f_user data) with
	Loader.Progress [ ss ; _ ; h ; _ ] -> Loader.Progress [ ss ; h]
      | pgrs  -> pgrs

    let eq_comm_pids f1 f_user = eq_comm f1 f_user pids 
    let eq_comm_list f1 f_user = eq_comm f1 f_user << My_misc.from_0_to_n $this$ >>
    let eq_comm_rev_list f1 f_user = eq_comm f1 f_user << My_misc.from_0_to_n (bsp_p-1- $this$) >>
					     
					     
    let eq ?(string_of=fun _ -> "_") v1 v2  =
      if
	v1 = v2
      then
	Loader.Success
      else
	Loader.Fail (string_of v1 ^"not equal to "^(string_of v2))

		    
    let eq_f_int_par_to_int f1 f_user  =
      let v = << Random.int 1000 >> in
      eq ~string_of:string_of_int (f1 v) (f_user v)

    let eq_f_int_par_to_int_list f1 f_user  =
      let v = << Random.int 1000 >> in
      eq ~string_of:(string_of_list string_of_int) (f1 v) (f_user v)

    let eq_f_int_list_par_to_int_list f1 f_user  =
      let v = << random_int_list () >> in
      eq ~string_of:(string_of_list string_of_int) (f1 v) (f_user v)
	 
    let eq_f_int_list_par_to_int_list_small f1 f_user  =
      let v = << random_int_list_small () >> in
      eq ~string_of:(string_of_list string_of_int) (f1 v) (f_user v)

    let eq_int_list v1 v_user  =
      eq ~string_of:(string_of_list string_of_int) v1 v_user
	 
    let ( => ) b1 b2 = not b1 || b2

    let full_test res_eq data f1 f_user   =
      compare_all res_eq  (fun () -> f1 data)  (fun () -> f_user data)

    let full_test_f_int_list_par_to_int_list f1 f_user =
      full_test eq_int_list  << My_misc.from_0_to_n $this$ >> f1 f_user 

    let full_test_f_int_rev_list_par_to_int_list f1 f_user =
      full_test  eq_int_list  << My_misc.from_0_to_n (bsp_p-1- $this$) >> f1 f_user 

    let full_test_f_int_rd_par_to_int_list f1 f_user =
      full_test  eq_int_list  << Random.int 1000 >> f1 f_user 

    let full_test_f_int_rd_list_par_to_int_list f1 f_user =
      full_test  eq_int_list  << random_int_list_small () >> f1 f_user 
  end
