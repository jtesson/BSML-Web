(**********************************************************)
(*  Online BSML  : Loading prepared exercise              *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(**********************************************************)
open Bsmlsig
open Listeners
(** Loading prepared exercise

an exercise is a cmo or cma file containing one or more module with signature EXERCISE.

Each of this module should be registered like this

let () = 
  register "myExercise 1" (module MyExerciseModule_1 : EXERCISE);
  register "myExercise 2" (module MyExerciseModule_2 : EXERCISE)

 *)

(** Function description gathered in a record.

For each function, you shoud give, as a function of the supported languages when it make sens :
  The name of the function that the student should produce (same name as the correction);
  The description of what the function should do;
  Optionally a hint to solve the problem ;
  Optionally a hint_code skeleton to solve the problem ;
  The name of the testers to be used to compare the function to the correction;

Pre-defined comparators and generators are provided by the Testers module.
 *)

let str_incompatible_version =
  function "fr" -> "Cet exercice n'est pas compatible avec la version actuelle" | "en"|_ -> "This exercise is not compatible with your current version"

let log = ref (fun s -> ())

type lang = string              
type fun_descr = { name : string  ;
		   typ : string;
		   descr : (lang*string) list;
		   hint: (lang*string) list;
		   hint_code: string;
		   testeurs : string list}

let escape_string str = "\""^(String.escaped str)^"\"" 
let string_of_fun_descr f_descr =
  "{ name =  "^(escape_string (f_descr.name))^
    "; typ = "^(escape_string f_descr.typ)^
    "; descr="^(My_misc.string_of_list
                  (fun (lg,descr) -> (escape_string lg)^","^(escape_string descr)) f_descr.descr)^
    "; hint ="^(My_misc.string_of_list
                  (fun (lg,descr) -> (escape_string lg)^","^(escape_string descr)) f_descr.hint)^
    "; hint_code ="^(escape_string f_descr.hint_code)^
    "; testeurs = "^(My_misc.string_of_list (fun i -> escape_string i) f_descr.testeurs)^
    "}"
			 
		   
type result = Success | Fail of string | Progress of ((string*result) list)
let rec string_of_result res =
  match res with
    Success -> "Success"
  | Fail str -> "Fail "^ str
  | Progress st_res_lst ->
     "Progress "^(My_misc.string_of_list (fun (st,res) -> "("^st^","^(string_of_result res)^")") st_res_lst)

let (+.+) r1 r2 =
  match r1,r2 with
    (str1,Progress r1),(str2, Progress r2) -> Progress
						( (List.map (fun (s,r) -> (str1^s),r) r1)
						  @
						    (List.map (fun (s,r) -> (str2^s),r) r2))
  | (str1,Progress r1),(str2, r2) -> Progress (List.map (fun (s,r) -> (str1^s),r) r1 @ [str2,r2] )
  | (str1, r1),(str2, Progress r2) -> Progress ((str1,r1):: (List.map (fun (s,r) -> (str2^s),r) r2))
  | (str1, r1),(str2, r2) -> Progress [(str1,r1);(str2,r2)] 

let (++) r1 r2 =
  match r1,r2 with
    (Progress r1),( Progress r2) -> Progress
				    (r1@r2)
  | (Progress r1), r2 -> Progress  (r1 @ ["",r2] )
  |  r1, Progress r2 -> Progress (("",r1):: r2)
  |  r1, r2 -> Progress [("",r1);("",r2)] 

let bool_test expl fb =
  try
    if fb () then
      Success
    else
      Fail expl
  with exn  -> Fail ("exception : "^(Printexc.to_string exn))

let catch_test catcher test =
  try
    test () 
  with
    exn  ->
    try
      catcher exn
    with exn ->
      Fail ("exception : "^(Printexc.to_string exn))

	 
module type EXERCISE = 
  sig
    (* the exercise title that will be displayed on the page *)
    val title : lang -> string
    (* the exercise description that will be displayed on the page
     Can contain HTML formatting
     *)	  
    val description : lang -> string

    (** The list describes the functions to be produced by the sutdent.
     *)
			
    val function_descriptors : fun_descr list
    (*  *)
    val get_fun_tester : string -> ('a -> result)
									    
  end

module Test_store () :
sig
  val add_tester :string -> ('a-> result) -> unit
  val get_fun_tester : string -> 'a -> result	
end
  = struct
  let table_tester : (string* ('a-> result)) list ref = ref []
							    
  let add_tester test_name (f_test:'a -> result) =
    table_tester:=(test_name,Obj.magic f_test)::!table_tester
					      
  let get_fun_tester_ (test_name :  string) : 'a  =
    Obj.magic (List.assoc test_name ! table_tester)
  let get_fun_tester :  string -> 'a -> result = get_fun_tester_
end
		      
let registered_exercises : (string * (module EXERCISE)) list ref
  = ref []

let newly_registered_exercises : (string * (module EXERCISE)) list ref
  = ref []

let started_ref :  (string * (module EXERCISE)) option ref=  ref None
	
module New_exo_listeners = Listeners (struct type t=(string * (module EXERCISE)) end)
					 
let register name (exo : ( module EXERCISE ))=
  !log ("registering exercise : "^name);
  New_exo_listeners.call(name,exo);
  newly_registered_exercises := (name, exo )::!newly_registered_exercises

					    
let registered_initializers : (unit->unit) list ref
  = ref []

let newly_registered_initializers : (unit -> unit) list ref
  = ref []

let register_initializer f=
  !log "registering an initializer";
  newly_registered_initializers := f::!newly_registered_initializers

									  

exception NoExerciseRegistered of string


let get_exercises_name () = List.map fst (!newly_registered_exercises @ !registered_exercises)
let get_exercise name = try
    List.assoc name (!newly_registered_exercises @ !registered_exercises)
  with Not_found ->
    raise ( NoExerciseRegistered name)

let get_started () = !started_ref

let start name exo =
  started_ref:=Some (name,exo)
		      
  (*   None -> raise (NoExerciseRegistered "no exercise has been started yet") *)
  (* | Some nam_exo -> nam_exo *)

let initialize initializer_list =
  List.iter (fun i -> i () ) initializer_list 
   
		      
let reinit () =
  let started  = get_started () in
  begin
    !log "reinit in loader";
    registered_exercises:=[];
    newly_registered_exercises:=[];
    List.iter (fun i ->i() )  !registered_initializers;
    registered_exercises := (!newly_registered_exercises @ !registered_exercises);
    newly_registered_exercises :=[] ;
    match started with
      None -> ()
     | Some (started_name, _) ->
	started_ref :=
	  Some (started_name ,List.assoc started_name !registered_exercises)
  end

let load_not_exercise  ?(lang="en") ~alert file_name  =   
  let file = Dynlink.adapt_filename file_name in
  (* let file = file_name in  *)
  if Sys.file_exists file then
    begin
      (
      try
	Dynlink.loadfile (file)
      with
      | Dynlink.Error (Dynlink.Inconsistent_import msg) -> alert (str_incompatible_version lang)
      | exn ->
	 raise (NoExerciseRegistered ("Problem while loading file "^file^" "^(Printexc.to_string exn)))
      )
    end
  else
    raise (NoExerciseRegistered ("trying to load non-existing file \""^file^"\""))
    
let load_exercise  ?(lang="en") ~alert exec file_name  =
  let file = Dynlink.adapt_filename file_name in
  (* let file = file_name in  *)
  if Sys.file_exists file then
    begin
      (
      try
	Dynlink.loadfile (file)
      with
      | Dynlink.Error (Dynlink.Inconsistent_import msg) -> alert (str_incompatible_version lang)
      | exn ->
	raise (NoExerciseRegistered ("Problem while loading file "^file^" "^(Printexc.to_string exn)))
      );
      let count = List.length !newly_registered_initializers  in
      (* let res = !newly_registered_initializers in *)
      if count > 0 then
	begin
	  List.iter (fun init -> init ()) !newly_registered_initializers ;
	  registered_initializers := (!newly_registered_initializers @ !registered_initializers);
	  newly_registered_initializers :=[] 
	end
      else
	raise (NoExerciseRegistered (file^" contains no call to register_initializer"))
      ;
      let count = List.length !newly_registered_exercises  in
      let res = !newly_registered_exercises in
      if count > 0 then
	begin
	  registered_exercises := (!newly_registered_exercises @ !registered_exercises);
	  newly_registered_exercises :=[] ;
	  res
	end
      else
	raise (NoExerciseRegistered (file^" contains call to register_initializer but no call to register"))
    end
  else
    raise (NoExerciseRegistered ("trying to load non-existing file "^file))
	     

							    
			       
(* TODO 
register an exercise :
 In the exercise hashtable, register 
- Exo_name_1
|    |
|    |- tester1_name , tester1 : 'a -> result, name_of_function_to_be_passed
|    |
|    |- tester1_name , tester2 : 'a -> result, name_of_function_to_be_passed
|    ...
|- Exo_name_2
    |
    |- tester1_name , tester1 : 'a -> result, name_of_function_to_be_passed
    |
    |- tester1_name , tester2 : 'a -> result, name_of_function_to_be_passed
    ...


Checking exo :
 Exec " view_result exo_name tester_name ((get_fun exo_name tester_name) name_of_fun_to_be_passed)"
*)
