(**********************************************************)
(*  Online BSML  : Program to build valid exercises       *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(**********************************************************)

(** make_exercise parses an exercise written as an ml files satisfying some constraints. 
TODO : Description of exercise format

 *)

(* #require "str" ;; *)

open My_misc
open Loader
(* parametres 
 1 : fichier ml
 *)
let _ = print_endline "getting arguments"
let nom_fichier = Sys.argv.(1)

let nom_fichier_mli = Sys.argv.(2)

let _ = print_endline ("checking arguments "^nom_fichier^" "^nom_fichier_mli)

let check_arg =
  if not (Filename.check_suffix nom_fichier ".exo.ml") then
    failwith ("the program argument #1 should be a valid file name, with suffix .exo.ml not "^nom_fichier)
  else
    if not (Sys.file_exists nom_fichier_mli ) then
      failwith ("the program argument #2  should designate a valid interface file, not "^nom_fichier)
    else
    ()

let nom_fichier_sans_ext = Filename.chop_suffix nom_fichier ".exo.ml"
						 
let _ = print_endline "opening input file"

let ic = open_in nom_fichier

let _ = print_endline "opening input signature file"
let ic_mli = open_in nom_fichier_mli

let warn str =  print_endline str
		 

		 

open Str
       
let space_exp =   "[ \t\n]*"    
let space_exp_plus =  "[ \t\n]+"    

let identifier = "\\([A-Za-z0-9_-]+\\)"
let regexp exp = global_replace (Str.regexp " ") space_exp_plus exp

let remove_space exp = global_replace (Str.regexp space_exp_plus) ""  exp		 
let remove_nl exp = global_replace (Str.regexp "\n") " "  exp		 

let group expr = "\\("^expr^"\\)"
let group_opt expr = "\\("^expr^"\\)?"
let group_or lst =   (string_of_list ~start:"" ~sep:"\\|" ~stop:"" (fun kw -> group (kw)) lst)


(* Keyword detection *)
let keywords = [" description "; " hint "; " testeur[s]? "; " hint code "]		 
let keywords_reg =
  Str.regexp_case_fold (regexp (string_of_list ~start:"" ~sep:"\\|" ~stop:"" (fun kw -> group (kw)) keywords))
let is_kw s  = Str.string_match  (keywords_reg ) s 0

				 

let block name = group_opt (name^" :: "^(group "[^|]*"))^(" | ")
let description_block = block "description"
let generateurs_block = block "generateurs?"
let hint_block = block "hint"
let hint_code_block = block "hint code"

(* matching a full comment block *)
let comment_def_block = "(\\*+"^"\\([^*]\\|\\(\\*[^)]\\)\\)+"^"\\*+)"
      

(** Type of field description  *)
type fields = Fun_name of string | Descr of ((lang*string) list) | Hint of  ((lang*string) list) | Hint_code of string | Testeurs of string list

let lang_splitter = "=>"
let split_lang txt = let splitted = Str.split (Str.regexp lang_splitter) txt in
                     let rec aux splitted = 
                       match splitted with
                         [txt] -> warn ("no multi-lang for \""^txt^"\""); [("_",String.trim txt)]
                       | lang::txt::t -> (String.trim lang,String.trim txt)::(aux t)
                       | _  -> []
                     in
                     aux splitted
                       
                                      
(** Building a  field description from a field name string and the following text.  *)
let normalize kw text=
  match String.lowercase (remove_space kw)  with
    "description" -> Descr (split_lang text)
  | "hint" -> Hint (split_lang text)
  | "hintcode" -> Hint_code text
  | "testeur" | "testeurs" -> Testeurs (Str.split (Str.regexp space_exp_plus) text)
  | s -> failwith ("Not a kw "^s)

		  
let string_of_field f =
  match f with
  | Fun_name fn -> "Fun_name "^fn
  | Descr ds -> "Descr "^(string_of_list (fun (i,j) ->i^"=>"^j) ds)
  | Hint _ -> "Hint"
  | Hint_code _ -> "Hint_code"
  | Testeurs ts -> "Testeurs "^(string_of_int (List.length ts))

(**
Reading input file
 *)

								
let file_content = ref ""
let exo_ic = ic
let () =
  try
    while true do
      let line = input_line exo_ic in
      file_content:= !file_content ^"\n"^ line
    done
  with _ -> close_in ic

(* let descr_regexp = "(\\\*+ "^ *)
(* 		     description_block^" "^ *)
(* 		     generateurs_block^" "^ *)
(* 		     (\* hint_block^" "^ *\) *)
(* 		     (\* hint_code_block^ *\) *)
(* 		     " \\*+)" *)
(* let descr_regexp' = regexp descr_regexp *)
(* (\* let descr_regexp' = "(.*\\)" *\) *)

(* let descr_regexp_ = "\\(description[ \t\n]*::[ \t\n]*\\([^|]*\\\)\\)" *)

(* 
 Splitter cut the file at each :: (* or *)
 *)

let splitter = Str.regexp "\\(::\\)\\|\\((\\*\\)\\|\\(\\*)\\)"

(* Splitting input file *)
let splitted = Str.split splitter !file_content

(** Filtering splitted file and detecting field types  *)

(* regexp to catch a function def   *)
let match_fun_def = 
  Str.regexp (space_exp^
		(* let or and *)
		(group (group_or ["let";"and"]))^
		  (* then we coud have a rec *)
		  space_exp_plus^
		    (group_opt "rec ")^
		      space_exp^
			(* finally the name of the function *)
			identifier)

(** getting the function name  *)
let get_fun_name str =
  Fun_name
    (ignore(Str.search_forward match_fun_def str 0) ; Str.matched_group 5 str)

let log s =
  (* () *)
  output_string stderr ("\027[32m"^s^"\n\027[37m=========\n")

let alert s =
  (* () *)
  output_string stderr ("\027[31m"^s^"\n\027[37m=========\n")

(** Filtering splitted file:
 none keyword-preceded section are discarded unless we are waiting for a function definition 
 The field will appear in reverse order with respect to the file order, making fun name appear first in the result list.
*)
let filter_kw lst =
  let rec aux res remains last_was_descr  fait = 
    match remains with
      [] -> res
    | [d] -> if  last_was_descr then
	     try
	       let fun_name =(get_fun_name d) in
	       (* log ("not kw : "^d); *)
	       fun_name::res
	     with Not_found -> failwith "waiting for a final function"
	     else
	       res 
    | kw::descr::t ->
       (* let sub_descr = "\n"^(String.sub descr 0 (min (String.length descr) 15 )) in *)
       if is_kw kw then
	 begin
	   (* log ("kw : "^kw^sub_descr); *)
	   aux ((normalize kw descr)::res) t true (kw::descr::fait )
	 end
       else
	 begin
	   if last_was_descr then
	     try
	       let fun_name =(get_fun_name kw) in
	       (* log ("not kw : "^kw^sub_descr) ; *)
	       aux (fun_name::res) (descr::t) false (kw::fait )
	     with Not_found ->
	       (* log ("not kw - not fun : "^kw^sub_descr) ; *)
	       aux res (descr::t) true  (kw::fait )
	   else
	     (* (log ("not kw - not waiting fun : "^kw^sub_descr) ; *)
	     aux res (descr::t) false  (kw::fait )
	      (* ) *)
	 end
  in
  (* log  (string_of_list ~sep:"\n" (fun i -> i) lst) ; *)
  let res = aux [] lst false  [] in
  log  (string_of_list ~sep:"\n" string_of_field res) ;
  res

(** **************************  *)
(** * Splitting interface file  *)
(** **************************  *)
let interface_content = ref ""
let () =
  log ("reading "^nom_fichier_mli);
  try
    while true do
      let line = input_line ic_mli in
      interface_content:= !interface_content ^"\n"^ line
    done
  with _ -> close_in ic_mli

let type_intro = "val"		     
let splitter = Str.regexp (space_exp_plus^type_intro^space_exp_plus)

let fun_name_reg = Str.regexp (space_exp^identifier)

let extract_type decl = 
  let ident = (ignore(Str.search_forward fun_name_reg decl 0) ; Str.matched_group 1 decl) in
  let type_start= (ignore(Str.search_forward (Str.regexp (":")) decl 0); Str.match_end ())
  in
  (ident, remove_nl(String.sub decl type_start (String.length decl - type_start)))

let splitted_interface =
  List.map extract_type 
	   (Str.split splitter !interface_content)

(* let _ = log (string_of_list (fun (i,j) -> i^" <:> "^j) splitted_interface)				 *)   
let get_type f_name = try List.assoc f_name splitted_interface with Not_found -> ""
			
(** Building Exercise description
The description list will be collected function by function over the filtered list commming from the file splitting.

The list should contain fun name followed by fields descriptions.
 *)


		   
(** Creating initial record with empty string for every field but the name
 Record type is given in Loader module
  *)
let init_fun_descr name = {
  name = name ;
  typ = get_type name ;
  descr = [] ;
  hint = [] ;
  hint_code = "" ;
  testeurs = [] }

let funify l = (fun i -> try List.assoc i l with _ -> "")
			    
(** Build list of function description from filtered splitted file *)
let rec build_funs lst =
  (* Gathering function description until next function *)
  let rec next res lst =
    match lst with
      [] -> res,lst
    | Fun_name str ::t -> res,lst
    | Descr lst ::t  -> next {res with descr =  lst@ res.descr} t
    | Hint lst ::t   -> next {res with hint =  lst@ res.hint} t
    | Hint_code str ::t   -> next {res with hint_code = (String.trim str)} t
    | Testeurs str_list ::t   -> next {res with testeurs  = List.map String.trim str_list} t
  in
  match lst with
    [] -> []
  | Fun_name name :: t ->
     let fd,remain = next (init_fun_descr name) t in
    fd::(build_funs remain)
  | _ -> failwith "should start with fun name"
	      
(**  list of all function gathered from the input file  *)
let fun_list_from_file =
  let filtered =
      try 
	filter_kw splitted
      with ex ->
	print_endline (string_of_list
			 ~sep:"\n==========================================\n"
			 (fun i -> i)
			 splitted
		      );
	raise ex
	
  in
  try 
    List.rev (build_funs filtered)
  with
    ex ->
    print_endline (string_of_list ~sep:"\n==========================================\n"
				  string_of_field
				  filtered
		  );
    raise ex

(** *****************************************  *)
(** ********** Module construction **********  *)
(** *****************************************  *)

let module_type = ref "module type Exo = sig \n"
let module_tester = ref ("module Exo  = struct
"^ !file_content^"
module TS = Loader.Test_store () 
open Loader
\n")
		       
let () =
  List.iter (fun fd ->
	     	     match fd.testeurs with
	       [] -> ()
	     | _ -> 
		(let test ="test_"^fd.name  in
		 let param_f = fd.name^"_user" in
		 module_type := !module_type^ 
				  "val "^test^" : 'a -> result \n";
		 let tester ="   let "^test^" ("^param_f^": 'a) = \n     "^
			       (string_of_list
				  ~start:""
				  ~sep:"++"
				  ~stop:""
				  (fun tester_name ->
				   "( Loader.catch_test (fun exn -> raise exn ) (fun () -> "^
					tester_name^" "^fd.name^" "^param_f
					^"))") 
				  fd.testeurs)
	     in
	     module_tester := !module_tester^tester^"\n"^
				"  let () = TS.add_tester \""^test^"\" "^test^"\n"
										
										
		)
	    )
	    fun_list_from_file ;
  module_tester := !module_tester^"
  let  get_fun_tester fname = TS.get_fun_tester (\"test_\"^fname) \n

  let  function_descriptors ="^( string_of_list string_of_fun_descr fun_list_from_file)^"
end

let init () = 
    ignore (Loader.register Exo.name (module Exo))

let _ =
  Loader.register_initializer init
\n"
  ;
    module_type := !module_type^"\nend\n"
  ;
    print_endline "writting generated file";
  flush stdout;
  let oc = open_out (nom_fichier_sans_ext^".ml") in
  (output_string oc !module_tester ;
   flush oc;
   close_out oc
  )
    
