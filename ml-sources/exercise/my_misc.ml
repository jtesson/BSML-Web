(**********************************************************)
(*  Online BSML  : helper functions                       *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(**********************************************************)

(**  n premiers éléments d'une liste  *)
let take n lst =
  let rec aux lst i res =
  if n = i then List.rev res else
    match lst with
      [] ->  List.rev res
    | h :: t -> aux t (i+1) (h::res)
  in
  aux lst 0 []

(**  la liste sans les n premiers éléments  *)
let rec drop n lst =
  if n = 0 then lst
  else 
    match lst with
      [] ->  []
    | h :: t -> drop (n-1) t

(**  Coupe une liste en deux, retourne les deux morceaux sous la forme d'une paire de liste  *)
let cut_at n l =
  let rec aux n l1 l2 =
    if n <= 0 then (List.rev l1, l2)
    else
      match l2 with
	[] -> failwith "cut_at after the end"
      | h::t -> aux (n-1) (h::l1) t
  in
  aux n [] l		     
      
(** chaine de caractère construite à partir d'une liste
 si limit est >=0, seulement limit éléments seront affichés
limit start sep et stop sont optionnels.
string_of_data doit être une fonction convertissant une donnée en chaine de caractère.

exemple : 
   string_of_list ~start:"[. " ~sep:"," string_of_int [0;1;3]
donne la chaîne [. 0,1,3]
  *)
let string_of_list ?( limit= -1 ) ?(start="[") ?(sep=";") ?(stop="]") string_of_data lst =
  let rec str_of_list_aux str lst =
    match lst with
    | [] -> failwith "unreachable state"
    | [d] -> str ^(string_of_data d)
    | h::t -> str_of_list_aux (str ^ (string_of_data h)^sep) t
       
  in
  let stop = if limit >= 0 then "..."^stop else stop in
  let lst = take limit lst in 
  match  lst with
    [] -> start^stop
  | h::t -> (str_of_list_aux start lst)^stop

(** Suppression de l'encapsulation de type option  *)
let no_some : 'a option -> 'a = function None -> failwith "no_some on None" | Some v -> v


    
(** Liste des éléments de 0 à n inclus  *)
let from_0_to_n n =
  let rec aux i list =
    if i = 0  then 0::list
    else aux (i-1) (i::list)
  in aux n []

(** Liste d'entier aléatoire de taille bornée aléatoire  *)
let random_int_list_fixed_max max_size=
  let size = Random.int  max_size in
  let rec aux size res =
    if size <= 0 then res
    else aux (size - 1) (Random.int max_size :: res)
  in
  aux size []
	 
let random_int_list ()= random_int_list_fixed_max 10000 
      
let random_int_list_small ()= random_int_list_fixed_max 100



(*  Function to merge duplicates in a list.
   merge : merge together the found duplicates into an ad-hoc structure
   singleton : produce the inital ad-hoc structure from the first element
   is_dup a b : tell if b is a duplicate of a
   lst : list of elements

 example : duplicate (fun (e,i) _ -> (e,i+1)) (fun e -> (e,1)) (=) ["a";"b";"a";"b";"c"] 
returns [("a",2);("b",2)] 

 *)
let rec duplicate merge singleton is_dup lst =

  match lst with
    h::t -> let dups,fresh = List.partition (is_dup h) t
            in
            if dups = [] then  duplicate merge singleton is_dup fresh
            else
              let merged_dups = List.fold_left merge (singleton h) dups
              in merged_dups:: duplicate merge singleton is_dup fresh
   | [] -> []
                                                        
