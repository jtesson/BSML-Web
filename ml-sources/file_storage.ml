(**********************************************************)
(*  Online BSML  : BSML Toplevel initialisation           *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(* This library handles two kinds of files :              *)
(* The files remotely accessible at a given URL and the   *)
(* files locally saved in the browser local storage.      *)
(*                                                        *)
(* TODO : Read/write file system through wedbdav/git/...  *)
(**********************************************************)

open Shared

(** operator to append directories using the system directory separator  *)
let  ( // ) = Filename.concat

let is_relative path = (Url.url_of_string path = None)

(** Do the same operation for each line of a file  *)
let foreach_line file f=
  let ic = open_in file in
  try
    while true do
      let line = input_line ic in
      f line
    done
  with End_of_file -> close_in ic

(** list files in the given directory  *)
let ls s = Sys.readdir s

  
(** output the file content *)
let cat file =       		
      foreach_line file print_endline

open XmlHttpRequest
open Lwt

(** get a blob ressource from an URL *)
let blob_ressource url =
  let open XmlHttpRequest in
  (
    (XmlHttpRequest.perform_raw Blob url) 
    >>= 
      (fun response -> 
       if response.code = 200 then
         File.readAsBinaryString (Js.Opt.get response.content (fun () -> failwith "no blob"))
       else
	 (
	   log ("Fail to rertrieve file fom url "^url);
           failwith ("Fail to rertrieve file fom url "^url)
	 )
      )
  )

(** Download the file from the given URL at the given local destination *)
let register_remote_file url dest=
  blob_ressource url 
  >|=
    (fun js_str -> 
     let txt =(Js.to_string js_str) 
     in
     Sys_js.register_file dest txt
    )
      
      
	  
(** Using the local storage of the browser as a file system  *)
module FileStorage (Name : sig
			     val name : string
			   end )
  =
  struct
    exception File_exists of string
			       
    exception File_do_not_exists of string
    exception Not_a_directory of string
				      
    (* exception Not_a_directory of string *)
				      
    let get_storage () =
      match Js.Optdef.to_option (Dom_html.window##localStorage) with
      | None -> raise Not_found
      | Some t -> t


    let file_name fname =  (Name.name // fname)		     
    let file_name_js fname  = Js.string (file_name fname)
    let js_file_name fname  = (Js.string Name.name)##concat(fname)
					
    let file_exists fname =
      let s = get_storage () in
      match Js.Opt.to_option(s##getItem(file_name_js fname)) with
    	None -> false
      | Some _ -> true

    let get_file_opt fname =
      let s = get_storage () in
      match  Js.Opt.to_option(s##getItem(file_name_js fname))with
	None -> None
      | Some str ->
    	 let a : string = Json.unsafe_input str
	 in
	 Some a

    (* TODO could probably be more efficient (useless conversion to ocaml string/js string) *)
    let get_file_opt_js fname =
      let s = get_storage () in
      match  Js.Opt.to_option(s##getItem(js_file_name fname))with
	None -> None
      | Some str ->
    	 let a : string = Json.unsafe_input str
	 in
	 Some (Js.string a)

	      		    
    let get_file fname =
      match get_file_opt fname with
	None -> raise (File_do_not_exists fname)
      | Some str -> str

    let save_file  ~overwrite fname data =
    	let s = get_storage () in
    	let fname_js = file_name_js fname in
    	match Js.Opt.to_option (s##getItem(fname_js)) with
    	| None
	  ->
    	   s##setItem(fname_js, Json.output data)
    	| Some _ ->
    	   if not overwrite then
    	     raise (File_exists fname)
	   else
    	     s##setItem(file_name_js fname, Json.output data) 
	     
					  
    let ls dname =
      if file_exists dname then
    	raise  (Not_a_directory dname)
      else ();
      let s = get_storage () in
      
      let len = s##length in
      let lst = ref [] in
      let dlength = String.length (file_name dname) in
      for i = 0 to len -1 do
    	let fname = Js.to_string (Shared.noopt (s##key(i)))  in
    	let fname_length = String.length fname in
    	if fname_length <= dlength then
    	  ()
    	else
    	  if String.sub fname 0 dlength = dname then
    	    lst:=  String.sub fname dlength (fname_length-dlength) :: !lst
    	  else
    	    ()
      done;
      List.filter
    	(fun s -> String.length s >0)
    	(List.map
    	   (fun s -> try let idx = String.index s '/'  in
    			 String.sub s 0 (idx+1)
    		     with Not_found  -> "")
    	   !lst
    	)
	

    let load_ressource s = get_file_opt s
	     
  end
	

    
