(* 
This instrumentation of BSML draw in Canvas a representation of each super-step


For each super-step, a div will be created with id "single-bsp-canvas-container-X" where X is the super-step number
this div will contain a canvas with id "bsp-canvas-X"
|--bsp-canvas-container------------------------------------------------------------------------------------------|
|                                                                                                                |                                                      |
|  |-single-bsp-canvas-container-0--|   |-single-bsp-canvas-container-1--|   |-single-bsp-canvas-container-2--|  |
|  |                                |   |                                |   |                                |  |
|  | |-bsp-canvas-0--------------|  |   | |-bsp-canvas-1--------------|  |   | |-bsp-canvas-2--------------|  |  |
|  | |                           |  |   | |                           |  |   | |                           |  |  |
|  | |---------------------------|  |   | |---------------------------|  |   | |---------------------------|  |  |
|  |                                |   |                                |   |                                |  |
|  |--------------------------------|   |--------------------------------|   |--------------------------------|  |
|----------------------------------------------------------------------------------------------------------------|
computation time and communication will be represented graphically in the canvas and numerically in a table attached to the div.
The table will have css class "table-bsp-step". 
The canvas will have css class "canvas-bsp-step". 
The canvas/table container will have css class "container-bsp-step". 

// At the begining, the container "single-bsp-canvas-container-0" and the canvas "bsp-canvas-0" should exist.
// The size of the initial canvas is used to build the next ones.

This instrumentation adds TimeSizeInstrumentation.Time_Histogrammes_size and TimeInstrumentation.Replicated_time instrumentation.

Improvements TODO : 
   - canvas proportionnal to computation time 
   - with adjustable scale
   - in a separate window
   - ability for the user to put markers next to the canvas/on the canvas
   - don't merge all async timing/replicated timing but show their interleaving (needs new instrumentation)
 *)


open Bsmlsig
open BsmlInstrumentation
open Shared

(** The panel size will be one of the following:
    - Fixed i j : all panel have the same size [i]x[j]
    - Scaledy i w c l : steps are drawn along the y axis, the height of the step is  [w]*superstep work (comuptation timing) +[c]*max comm + [L]. the width is [i]
    Not yet implemented :
    - Scaledx f i : steps are drawn along the x axis, the width of the step is  [f]*superstep timing. the height is [i]
  *)
type panel_size = Fixed of int*int |  Scaledy of int * float * float * int (* | Scaledx of float *int *)

module type DRAWING_CONTAINER = sig
  val container : Dom_html.element Js.t
  val size : panel_size 
end

module Legacy_Container :  functor  (U : UNIT)  -> DRAWING_CONTAINER =
  functor  (U : UNIT)  ->
  struct
    let container =  try find_div "bsp-canvas-container" with Not_found ->
                       Shared.log ("bsp-canvas-container not found");
                       failwith  ("bsp-canvas-container not found")
    let () = Graphics_js.open_canvas (by_id_coerce "bsp-canvas-0" Dom_html.CoerceTo.canvas)
    let size = Fixed ((Graphics_js.size_x ()), (Graphics_js.size_y ()))
  end

module Scaled_Container :  functor  (U : UNIT)  -> DRAWING_CONTAINER =
  functor  (U : UNIT)  ->
  struct
    let container =  try find_div "bsp-canvas-container" with Not_found ->
                       Shared.log ("bsp-canvas-container not found");
                       failwith  ("bsp-canvas-container not found")
    let () = try Graphics_js.open_canvas (by_id_coerce "bsp-canvas-0" Dom_html.CoerceTo.canvas) with _  -> ()
    let size = try
        Scaledy (  (Graphics_js.size_x ()), 100.,0.001,(Graphics_js.size_y()/5))
      with
        _ ->
        Scaledy (  400, 100.,0.001,60)

  end

  
module JsDrawing (Drawing_Container : DRAWING_CONTAINER)  (Bsml:BSML)   =
  struct

    open Bsml
    (* Module containing a function computing the size of a given value  *)
    module Sz = struct
      let size_of_data (d:'a) : int =
	try
          Tools.size_of_data d
	with exn ->
          Printexc.print_backtrace stderr; raise exn
    end
    (* Building base instrumentations to get asychronous computation timing histogramms and replicated computation timings  *)
    module All_Instr = Merge (TimeSizeInstrumentation.Time_Histogrammes_size (Sz) )
			     (TimeInstrumentation.Replicated_time)
    module TSInstr =  All_Instr(Bsml)
    module Log_the_log =  LogInstrumentation.Log (struct let logger = Shared.log ~level:Warn ~use_debug_div:false ~use_debug_firebug:true end) (Bsml) 

    let names = TSInstr.names
    (** ***************************  *)
    (** TODO : move in common utils  *)
    (** ***************************  *)
   let fold_procs f start =
     let rec aux f start p =
       if p = 0 then
	 f start p
       else
	 f (aux f start (p-1)) p
     in
     aux f start (bsp_p -1)

   let scientific_notation_float f =
     let pow = float_of_int (int_of_float (log10 f)) in
     (f/. (10. ** pow) , pow)

   let string_of_float_sc f =
     let (x,y) = scientific_notation_float f in
     let x=float_of_int (int_of_float (x*.100.))/.100. in
     (string_of_float x)^"e"^(string_of_float y)

   (** ********  *)
   (** END TODO  *)
   (** ********  *)

   (** logs_local and logs_global are not modified. 
       we keep the data as they are and just add side effects (drawing in the webpage)
    *)
   type logs_local = TSInstr.logs_local
   type logs_global = TSInstr.logs_global
   (* The instrumentation  *)

   (* open Graphics_js *)

   (* the [container] contains all bsp-steps drawing  *)
   let container = Drawing_Container.container

   (* creating "current container" and "current canvas" *)
   let canvas_counter = ref 0
                      
   let create_canvas_container () =
          let canvas_container = Dom_html.createDiv Dom_html.document in
          let () =
            canvas_container##id <-
	      (Js.string ("single-bsp-canvas-container-"^(string_of_int !canvas_counter)));
            canvas_container##className <- (Js.string "container-bsp-step")
          in
          let canvas_container_node :> Dom.node Js.t = canvas_container in
          ignore (container##appendChild(canvas_container_node)) ;
          canvas_container

   let foi = float_of_int
   let iof = int_of_float
   (* Computing the canvas size given a total superstep time   *)
   let canvas_size  max_time max_comm =
     match Drawing_Container.size with
        Fixed (i,j) -> i, j
     (* |  Scaledx (scale,height) -> int_of_float (max_time *. scale)+1, height *)
     |  Scaledy (width,wscale,cscale,l)  -> width, iof (max_time *. wscale +. (foi max_comm *. cscale)) + l

   (* If we want to build a canvas and we don't know the timing and the max comm, we use this defaults.  *)
   let default_time = 1.
   let default_comm = 0
                    
                    
   let create_canvas canvas_container max_time max_comm =
     let canvas = Dom_html.createCanvas Dom_html.document in
     let (size_x,size_y) = canvas_size max_time  max_comm in
     let () = canvas##id <- (Js.string ("bsp-canvas-"^(string_of_int !canvas_counter)));
   	      canvas##className <- (Js.string "canvas-bsp-step");
   	      canvas##width <- size_x ;
   	      canvas##height <- size_y;
   	      canvas##id <- (Js.string ("bsp-canvas-"^(string_of_int !canvas_counter)))
     in
     let canvas_node :> Dom.node Js.t = canvas in
     incr canvas_counter;
     ignore (canvas_container##appendChild(canvas_node));
     canvas

(* the [current_canvas_container] is a div containing the next canvas to be used  *)
   let current_canvas_container = ref  (try
                                          Some
                                            (noopt (Dom_html.CoerceTo.div  (noopt (container##querySelector(Js.string "#single-bsp-canvas-container-0")))))
                                        with exn ->
                                          try
                                            Shared.log ~level:Warn ("unable to get div #single-bsp-canvas-container-0; building it");
                                            None
                                              (* create_canvas_container () *)
                                          with exn ->
                                            Shared.log ~level:Warn ("unable to build div #single-bsp-canvas-container-0");
                                            raise exn)
                                
   (* the [current_canvas] is the next canvas to be used  *)
   let current_canvas = ref  (try
                                Some
                                  (noopt (Dom_html.CoerceTo.canvas (noopt (container##querySelector(Js.string "canvas#bsp-canvas-0")))))
                              with exn ->
                                try
                                  Shared.log ("unable to get canvas canvas#bsp-canvas-0; building it");
                                  None
                                    (* create_canvas !current_canvas_container default_time default_comm *)
                                with exn ->
                                  raise exn)
	                    

   (* For each superstep we will create a new canvas with id  "bsp-canvas-[canvas-counter]"
    contained in its own div "single-bsp-canvas-container-[canvas-counter]" 
    *)
   let create_container_and_canvas timing comms=
     let canvas_container = create_canvas_container () in (canvas_container,create_canvas canvas_container timing comms)
			  
   (* Go to the next fresh canvas *)
   let switch_canvas timing comms =
     let (canvas_container, canvas) = create_container_and_canvas timing comms in
     current_canvas := Some canvas;
     current_canvas_container := Some canvas_container;
     Graphics_js.open_canvas canvas
		      
   (* Open the first canvas, clear it, initialize the underlying instrumentation  *)
   let init () =
     begin
       match !current_canvas with
         None -> ()
       | Some canvas -> Graphics_js.open_canvas canvas ; Graphics_js.clear_graph()
     end;
     Log_the_log.init() ; TSInstr.init()

		   
   let drawing = ref true


   (* Reinitialisation :  
     Removing all existing canvas, start a fresh one, and reinit the underlying instrumentation  *)
   let reinit logs = 
     container##innerHTML <- Js.string "" ;
     canvas_counter :=0;
     (* let (canvas_container, canvas) = create_container_and_canvas default_time default_comm in *)
     current_canvas_container := None ;
     current_canvas := None ;
     (* Graphics_js.open_canvas !current_canvas ; *)
     (* Graphics_js.clear_graph();  *)
     drawing := true;
     Log_the_log.reinit logs ; TSInstr.reinit logs

   (* *************************************** *)
   (* *************************************** *)
   (* **  Actual drawing of bsp supersteps ** *)
   (* *************************************** *)
   (* *************************************** *)

   (* Checking canvas size, resizing if needed  *)
   let check_size timing comms=
     let (sx, sy) = canvas_size timing comms in
     begin
       if (sx != Graphics_js.size_x()) || (sy != Graphics_js.size_y()) then
         Graphics_js.resize_window sx sy
     end
   (* Horizontal spacing between timing bars *)
   let margin () : int =
     match Drawing_Container.size with
     | _ -> (Graphics_js.size_x () / bsp_p )/2
          
     (* | _ -> (Graphics_js.size_y () / bsp_p )/2 *)
          
   (* vertical between timing bars and communication endpoint *)
   let margin_comm () =
     match Drawing_Container.size with
     (* | Scaledx _ ->    assert false  *)
     | Scaledy _ ->    assert false 
     | Fixed _   ->  (Graphics_js.size_y() /5)
                   
   (* Abscisse of the timing bar of proc [i] *)
   let x_of_proc i =
     (Graphics_js.size_x() / bsp_p) * i + (margin ())
                                        
   (* Accumulators to detect the longest timingbar and the total size of communicated data *)
   let max_time = ref 0.
   let max_time_proc = ref 0
   let max_size =ref 1
   let cumulated_size = ref 0
   let init_accumulators () =
     max_time := 0. ; max_time_proc := 0; max_size := 1; cumulated_size :=0
   			   
   (* Length of a timing bar according to a given timing.
      If Drawing_Container.size is Fixed, the length is constant for the max timing and other bar are proportionnal to this bar.
      Else the size is proportionnal to the timing.
    *)
   let y_of_time t =
     match Drawing_Container.size with
       Fixed _ -> int_of_float (t /. !max_time *. (float_of_int(Graphics_js.size_y() - margin_comm()) ))
     | Scaledy (_,wscale,cscale,l) -> int_of_float (t  *. wscale)+1
		       
   let y_end_seq_time = Array.make bsp_p 0

   open Graphics_js
   (* Color of the segment of the bar that correspond to the asynchronous work of processor [i]  *) 
   let seq_color i = if i = !max_time_proc then red else green
                                                           
   (* Color of the segment of the bar that correspond to the replicated work of processor [i]  *) 
   let replicated_color i = yellow

   (* Color of the segment of the small segment that materialise the axis of the bar of processor [i] after the timing segment  *) 
   let join_end_color i = rgb 125 125 125
			    
   (* Width the timing segment  *) 
   let seq_width = 6
   (* Width the axis segment  *) 
   let join_end_width = 1
			  
   (* drawing the segment from top  (replicated comp + async comp + axis until the end of the box) of processor [i] using [timing] *)
   let draw_seq i timing =
     let (seq_time,replicated_time)= timing in
     let timing = seq_time +. replicated_time in
     (* starting point to top, and x position of the processor i *)
     let start_x = (x_of_proc i) and start_y = size_y() in
     begin
       moveto start_x start_y;
       let
         (*  seq timing will start after replicated, thus it ends after the total timing *)
	 y_end_seq_point = start_y - (y_of_time timing)
       and
         y_end_replicated_point = start_y - (y_of_time replicated_time)
       in
       (
         (* memorizing end point (to be used as starting point of communications) *)
	 y_end_seq_time.(i) <- y_end_seq_point;
         (* drawing replicated computation time *)
	 set_color  (replicated_color i);
	 set_line_width seq_width;
	 lineto  start_x y_end_replicated_point;
         (* drawing async computation time *)
	 set_color  (seq_color i);
	 set_line_width seq_width;
	 lineto  start_x y_end_seq_point;
         (* drawing continuing axis *)
	 set_color  (join_end_color i);
	 set_line_width join_end_width;
	 lineto start_x 0
       )
     end
	   
   (* let arrowto  ?(size=10.) ?(angle=0.7) to_x to_y = *)
   (*   let pi = acos (-.1.) in *)
   (*   let (from_x,from_y) = current_point() in *)
   (*   let x= float_of_int(to_x - from_x) and y= float_of_int(to_y - from_y ) in *)
   (*   let alpha = atan (y/.x) in *)
   (*   let c =  pi +. alpha -. angle in *)
   (*   let y' = to_y - int_of_float(( size *. (sin  c) ) ) *)
   (*   and x' = to_x - int_of_float( ( size *. (cos  c) ) ) *)
   (*   in *)
   (*   let c' = alpha -. pi -. angle in *)
   (*   let y''  =  int_of_float(size *.(cos  c') )  *)
   (*   and x'' =    int_of_float(size *.(sin  c') ) *)
   (*   in let x = int_of_float(x) and y = int_of_float y *)
   (* 	in lineto to_x to_y ; lineto x' y'; moveto (to_x) (to_y)  *)

   (** **************  **)
   (**                 **)
   (**  Comm drawing   **)
   (**                 **)
   (** **************  **)

   (* Using a color scale to represent the size of communicated values *)
   let color_of_size s =
     let r = 255 * s / !max_size in
     let g = 255 -r in
     let correction = (min (255-r)(255 - g))
     in
     let r = r +correction
     and g = g +correction
     and b = 0 in
     rgb r g b

   (* Endpoint of communication.
      Arrows are too complicated and actualy not very nice, so we just put a bullet *)
   let arrowto   ?(size=5) ?(angle=0.7) to_x to_y =
     lineto to_x to_y ; fill_circle to_x to_y size

   (* Draw communications of size [size] from [from] to [dest] *)
   let draw_comm from dest size =
     let y_dest = 0 in (* (start_y - (y_of_time !max_time) -  max_comm * cscale +l ) *)
   	 moveto (x_of_proc from) y_end_seq_time.(from);
   	 if size > 0 then
	   begin
	     set_color (color_of_size size);
   	     arrowto (x_of_proc dest) y_dest;
	   end
   	 else
   	   ()


   (* *************************************** *)
   (* *************************************** *)
   (* **  Tables of Timings / Comm         ** *)
   (* *************************************** *)
   (* *************************************** *)

   (** ***************************  *)
   (** TODO : move in common utils  *)
   (** ***************************  *)
   let new_cell_s tr s =
     let td :> Dom.node Js.t = td_of (Js.string s) in
   	       tr##appendChild(td);
	       tr
   let tr_of_fun table line_descr str_of_data data_fun =
     let tr = Dom_html.createTr Dom_html.document in
     (* first cell is description *)
     let td :> Dom.node Js.t = td_of (Js.string line_descr)
     in
     ignore(tr##appendChild (td)) ;
     (* then we put cells with the processor numbers *)
     let tr  :> Dom.node Js.t= tr
     in
     (ignore(fold_procs (fun tr i -> new_cell_s tr (str_of_data (data_fun i))) tr);
      ignore(table##appendChild (tr))
     )
   (** ********  *)
   (** END TODO  *)
   (** ********  *)


   (* Activation/deactivation of drawings *)
   let start_drawing () = drawing := true (*  *)
   let stop_drawing () = drawing := false


   (* ******************************************* *)
   (* ******************************************* *)
   (* **  Actual instrumentation of primitives ** *)
   (* ******************************************* *)
   (* ******************************************* *)
      
   let pause logs =
     stop_drawing ();
     Log_the_log.pause logs ; TSInstr.pause logs
		   
   let resume logs =
     start_drawing ();
     Log_the_log.resume logs ; TSInstr.resume logs

   let selective_pause log_name logs =
     if [log_name] = names then pause logs else logs

   let selective_resume log_name logs =
     if [log_name] = names then resume logs else logs

   let logging_global_start logs = 
     Log_the_log.logging_global_start logs ; TSInstr.logging_global_start logs
   let logging_global_stop  logs = 		    
     Log_the_log.logging_global_stop  logs ; TSInstr.logging_global_stop logs
                                   
   let logging_local_start i logs =
     Log_the_log.logging_local_start i logs ; TSInstr.logging_local_start i logs
			       
   let logging_local_stop i s logs=
     Log_the_log.logging_local_stop i s logs ; TSInstr.logging_local_stop i s logs
			      
   let logging_comm_out from dest v logs  =
     Log_the_log.logging_comm_out from dest v logs  ; TSInstr.logging_comm_out from dest v logs 
			      
   let logging_comm_in  = Log_the_log.logging_comm_in ; TSInstr.logging_comm_in
			    
   let logging_before_sync_local   = Log_the_log.logging_before_sync_local  ; TSInstr.logging_before_sync_local 
   let logging_after_sync_local   = Log_the_log.logging_after_sync_local  ; TSInstr.logging_after_sync_local 
				       
    let logging_sync_global all_logs :
	  ((((float list * float option* float) *( int array ))*(float list * float option* float)) Bsml.par *
             ((unit* (int array array list))*unit))
      =
      let (logs_local, ((seq_logs_global , size_logs_global),replicated_logs_gloal)) =
	Log_the_log.logging_sync_global all_logs ; TSInstr.logging_sync_global all_logs
      in
      let timings = 
	apply
	  (mkpar (fun i v ->  let
		    (( seq_time::times , _ , _),
		     _ ),
		    (replicated_time::_ , _ , _) = v in (seq_time, replicated_time)))
	  logs_local
      in
      let (comms::_) = size_logs_global
      and  lengths = proj timings
      in
      let (max_len,max_proc) =
	fold_procs (fun (v,mp) p -> let mv = max v (fst (lengths p)+. (snd (lengths p))) in if mv=v then (v,mp) else (mv,p)) (0.,0)
      in
      begin
   	max_time := max_len;
	max_time_proc := max_proc;
	Array.iteri
	  (fun from comm_array ->
	   Array.iteri  (fun dest size ->  max_size:= max size !max_size ;
					   cumulated_size := !cumulated_size + size)
			comm_array
	  )
	  comms;
        (* Ensure that the canvas exists  *)
        switch_canvas !max_time !cumulated_size;
   	ignore
   	  (apply (mkpar (fun this timing -> draw_seq this timing)) timings);
	Array.iteri
	  (fun from comm_array ->
	   Array.iteri  (fun dest size -> draw_comm from dest size)
			comm_array
	  )
	  comms ;
   	(* Building HTML table showing the matrix of communication sizes  *)
   	let table = Dom_html.createTable Dom_html.document in
	begin
   	  table##className <- (Js.string "table-bsp-step");	
   	  (* Building list of processors *)
	  ignore( tr_of_fun table "" string_of_int (fun i ->i) );
   	  (* Printing seq timings *)
   	  tr_of_fun table "local time" string_of_float_sc (fun i -> fst (lengths i));
   	  (* Printing replicated timings *)
   	  tr_of_fun table "replicated time" string_of_float_sc (fun i -> snd (lengths i));
   	  (* For each processor, we will build a row containing
   	     a cell with the proc number, then cells containig the size of the communications
   	   *)
	  Array.iteri (fun p row ->
		       (* putting processor number of this row  *)
   		       ignore(tr_of_fun table (string_of_int p ^"->") string_of_int (fun i -> row.(i)))
   		      )
		      comms;
	  let table  :> Dom.node Js.t = table in
   	     (no_some !current_canvas_container)##appendChild(table);
	end;
	init_accumulators ();
	(logs_local, ((seq_logs_global , size_logs_global),replicated_logs_gloal))
      end
  end

open BsmlWithLocality
   
(** Building a BSML module instrumented with the above instrumentation *)
module Make  (Drawing_Container : DRAWING_CONTAINER) (Bsml:BSMLSEQ)  =
  struct
    include  BsmlInstrumentation.Make (Bsml)
    (BsmlInstrumentation.Merge
       (JsDrawing (Drawing_Container))
       ( NestedVectorDetection.NestingDetection ))

    let is_global ()  : bool = snd (snd (get_logs ()))
    let current_level () : level = if is_global() then Global else Local
  end                                                          


module Make_COERCED   (Drawing_Container : DRAWING_CONTAINER)  (Bsml:BSMLSEQ) : INSTRUMENTED_WITH_LEVEL
  =
  Make (Drawing_Container) (Bsml)
                                   

