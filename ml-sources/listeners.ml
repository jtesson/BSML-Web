(**********************************************************)
(*  Online BSML  : Listeners module                       *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(**********************************************************)

(* Modular Implementation of listners, Typ.t is the type of values passed to the awaked listeners  *)
(*  TODO : use Lwt to get asynchronism ? *)

module Listeners (Typ :sig type t end) = struct
  let listeners = ref []

  let add lstn = 
    listeners:= lstn::!listeners

  (* let addBefore lstn lstn2 =  *)
  (*   listeners:= lstn::!listeners *)

  let remove lstn =
    listeners:= List.filter (fun lstn0 -> lstn0  != lstn)  !listeners

  let call (data : Typ.t) =
    List.iter (fun lstn -> lstn data) !listeners
end
