(**********************************************************)
(*  Online BSML  : Courses loading                        *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(**********************************************************)

(** Loading prepared courses (ie package of exercises)  *)
(** A package of exercises is a directory containing files of exercises + there  dependencie's  
    and a file listing those files.
    Non-exercise modules (like test helpers) are listed as dependecies (on a line prefixed by "DEPS:".)
    
    The directory should be listed in exercises_dir together with very short description  
*)

(** We use a MVC pattern to handle courses.

* Model M  store the list of courses. 
New courses repository can be added, the ultimate goal would be to be abl load external repositories, from otherweb site. Allowing to build independent exercise repositories.

It receives Control commands and notify Viewers.

It registers Viewers.

* View V : Displays the menu
* Controler C : React to actions on the menu

*)
open Shared
open File_storage
open Listeners
  
module M =
  struct 	

    (* Implementation details *)
    (* list exercise files available in a given directory *)

    let remove_prefix pref line =
      let pref_len = String.length pref
      and line_len = String.length line
      in      
      if line_len <  pref_len then
        (false, line)
      else
        let prefix = String.sub line 0 pref_len  in
        if prefix = pref
        then (true , String.sub line pref_len (line_len - pref_len) )
        else (false, line)

    let depends_prefix = "DEPS:"	   

    let exercises_list dir =
      let list_cmos = ref [] and pre_cmos = ref [] in
      foreach_line (dir^"/exercises_list")
	           ( fun line ->
		     let line =String.trim line in
		     (* log  ("exercise list line \""^line^"\""); *)
		     match remove_prefix depends_prefix line with
		       true , str ->  pre_cmos :=
				        (List.map (fun cmo ->
					     (* log ("appending extension to cmo file\""^cmo^"\""); *)
					     cmo^".cmo")
					          (List.filter
						     (fun str -> not (String.trim str =""))
						     (My_html.split " " str)
					          )
				        )
				        @ !pre_cmos
		     | false, cmo -> 
		        ( (* log ("exercise list line :"^cmo^":");  *)
		          if cmo <> "" then
			    list_cmos := (cmo^".cmo") :: !list_cmos
		        )
	           );
      !pre_cmos,List.rev !list_cmos

    (* List courses available according to the description given in a repository *)
    let courses_list repo_name  =
      let list_course = ref [] in
      foreach_line (repo_name // "exercises_dir")
		   (fun line ->
		     (* log ("course list line :"^line);  *)
		     let dname, descr = My_html.split_at_first " " line in
		     let dname = String.trim dname in
		     try
		       let preds,exos = exercises_list (repo_name // dname) in
		       list_course := (repo_name,dname,descr, preds, exos) :: !list_course
		     with
		       exn -> log ~level:Error ("fail to retrieve exercises list for "^dname^" : "^(Printexc.to_string exn))
		   );
      !list_course


    (*  (Repository, Directory name, description, pred_cmo list, exo_cmo list) list ref  *)
    let lst_courses : (string * string * string * string list * string list) list ref = ref []

    let repositories = ref ["exercise"]
			   
    module New_courses_listeners = Listeners (struct type t=(string * string) end) 
    module Courses_status_listeners = Listeners  (struct type t=(string*status) end) 
    module Remove_courses_listeners = Listeners  (struct type t=(string) end) 
    module New_exercise_file_listeners =
      Listeners (struct type t=(int*string * string) end) (* course * fname *)
    module New_cmo_file_listeners =
      Listeners (struct type t=(string * string) end) (* course * fname *)

    open Lwt

    let real_url repo_path course_path exo_path =
      if is_relative repo_path then 
	"filesys" // repo_path // course_path // exo_path
      else
	repo_path // course_path // exo_path
				      
    let localize_path repo_path course_path exo_path =
      "exos" // course_path // exo_path

    let handle_exercise pos repo_path course_path exo_path =
      let fname = localize_path repo_path course_path exo_path
      and url = real_url repo_path course_path exo_path
      in
      log ("handle_exercise : getting remote file "^url^" "^fname) ;
      catch
	(fun _ ->
	  register_remote_file url fname
	  >|=
	    (fun _ -> New_exercise_file_listeners.call (pos,course_path,fname))
	)
	( fun exn -> log ("fail while handling exercise file "^fname^
			    " url : "^url^" exn :"^(Printexc.to_string exn));
		     return_unit)

    let pipe f data =
      let rec aux task (f : 'a -> unit Lwt.t)  data =
        match data with
	  [] -> task
        | h::t -> aux (task >>= (fun _ -> f h)) f t
      in
      match data with
	[] -> return ()
      | h::t -> aux (f h) f t
	            
    let handle_not_exercise repo_path course_path pred_path =
      let fname = localize_path repo_path course_path pred_path
      and url = real_url repo_path course_path pred_path
      in
      (* log ("handle_not_exercise : getting remote file "^url^" "^fname) ; *)
      catch
	(fun _ ->
	  register_remote_file url fname 
          >|=
	    (fun _ -> New_cmo_file_listeners.call (course_path,fname))
	)
	( fun exn -> log ("fail while handling file "^fname^
			    " url : "^url^" exn :"^(Printexc.to_string exn)); return_unit)


    (* Public methods *)
    let start_course name =
      Courses_status_listeners.call (name,Started)
				    
    let stop_course name =
      Courses_status_listeners.call (name, Stopped)
				   
    let add_repository dir =
      repositories:= dir::!repositories;
      let courses = courses_list dir in
      List.iter 
	(fun (repo,course,descr, preds, exos) ->
	 New_courses_listeners.call (course,descr);
	 ignore (
	     (* pipe *)
	     (*   (fun pred -> log ("pipe pred "^pred); return_unit ) *)
	     (*   preds *)
	     (* >|= *)
	     (* (fun _ -> *)
	      pipe (fun pred -> handle_not_exercise repo course pred )  preds
	      >|=
		(fun _ ->
		 List.iteri
		   (fun pos exo -> Lwt.async (fun  _ -> handle_exercise pos repo course exo ))
		   exos 
		)
	     (* ) *)
	   )
	)
	courses;
      lst_courses := courses @ !lst_courses
				
				
				   
    let remove_course name =
      lst_courses := List.filter (fun (_, nm , _ , _,_) -> nm != name) !lst_courses;
      Remove_courses_listeners.call name
				    
    let get_list_of_exercises () =
      List.flatten
	(List.map
	   (fun (_,course,_,cmos,exos) -> List.map (fun exo -> (course,cmos,exo) ) exos)
	   !lst_courses
	)
	

    let get_list_of_courses () =
      List.map (fun (_, nm , descr , _,_) -> nm,descr) !lst_courses

    let set_loggers () =
      New_courses_listeners.add (fun (dname, descr) -> log ("new course "^dname^" : "^descr));
      Courses_status_listeners.add (fun (dname,status) -> log ("change course "^dname^" to "^(string_of_status status)));
      Remove_courses_listeners.add (fun (dname) -> log ("remove course "^dname));
      New_exercise_file_listeners.add (fun (pos,course,fname) ->
				       log ("new exercise "^fname^" course "^course^" pos "^(string_of_int pos) ));
      New_cmo_file_listeners.add (fun (course,fname) ->
				       log ("new cmo "^fname^" course "^course))
	       
    let init () =
      set_loggers ();
      List.iter add_repository !repositories
      

    let reinit () = init()
		     
  end	       
       

(** *********************** **)
(** View of list of courses  *)
(** *********************** **)
module V = struct
  open My_html

  let lang = ref "en"
  let get_lang () = !lang
  let set_lang lg = lang:=(String.trim lg)

  (* ids *)
  let courses_list = id  "courses_list"

  (* classes *)
  let class_remove = cls "remove-button"
  let class_light_button = cls "light-button"
  let class_crs_entry = cls "course_list_class list-group-item"
  let class_crs_entry_done = cls "course_done"
  let class_crs_entry_started = cls "course_started"
  let class_crs_entry_stopped = cls "course_stopped"
  let class_crs_entry_new = cls "course_new"

  let class_loaded = cls "ready"
  let class_crs_title = cls "course_title_class"
  let class_course_descr = cls "course_descr_class"

  module Courses_list_listeners = Listeners (struct type t=(string) end) 
			       
  (** getting the list container  *)
  let get_list_of_courses_container () =
    by_id courses_list

  (** computing the id of a course entry from it's name  *)
  let course_entry_id name = "course-entry-"^name
  let course_entry_title_id name = "course-entry-title-"^name
  let course_entry_remove name = "course-remove-"^name
						    
  (** getting the course entry  *)
  let get_course_entry ?(do_log=true) name =
    by_id ~do_log:do_log (course_entry_id name)
	  
  let get_course_entry_title ?(do_log=true) name =
    by_id ~do_log:do_log (course_entry_title_id name)

  (** check whether a course has en entry  *)
  let in_list name =
    try ignore(get_course_entry ~do_log:false name); true with _ -> false


  let class_exercise_list = My_html.cls "exercise-list-container list-group"
  let course_exercise_list_id name = id (name^"-"^"exercise_list")

  (** adding a course to the list
    name identify the course and should be usable as html id
    Description will be displayed as title of the course

   *)
  let add_course_to_list ?(pos=None) name (description : string)  =
    let list_crs = get_list_of_courses_container () 
    in
    if in_list name then
      failwith ("course "^name^" already exists")
    else
      let node =
	Tyxml_js.To_dom.of_node
	  (Tyxml_js.Html5.(
			  div ~a:[a_id (course_entry_id name);
				  a_class [class_crs_entry;
					   class_crs_entry_new]
				 ]
			      [ h5 ~a:[a_id (course_entry_title_id name) ;
					       a_class [class_crs_title]
				      ]
				   [pcdata description];
				       div ~a:[a_id (course_exercise_list_id name);
					       a_class [class_exercise_list]
					      ]
					   []
			      ]
			)
	  )

      in
      (* begin *)
      (* 	match pos with *)
      (* 	  Some i -> (match Js.Opt.to_option (list_crs##childNodes##item(i))with *)
      (* 		       Some place -> list_crs##replaceChild(node,place) *)
      (* 		     | None -> list_crs##appendChild(node) *)
      (* 		    ) *)
      (* 	 |None -> *)
      ignore(list_crs##appendChild(node))
      (* end *)
       ;
      Courses_list_listeners.call (name)

  let set_course_handler name handler =
    (get_course_entry_title name)##onclick <- (
			     Dom.handler (fun _ ->
					  Js.bool (handler ())
					 )
			   )
  (** removing an course from the list *)
  let remove_course_from_list name  =
    remove_node (get_course_entry name)

  let cls_name ls =
    Js.string (string_of_list  ~start:"" ~sep:" " ~stop:"" (fun i -> i) ls)

  (** updating status of a course  *)
  let update_status_in_list name status =
    let node = get_course_entry name in
    match status with
      Done -> node##className <- (cls_name [class_crs_entry; class_crs_entry_done])
    | Started -> node##className <-  (cls_name [class_crs_entry; class_crs_entry_started])
    | Stopped -> node##className <-  (cls_name [class_crs_entry; class_crs_entry_stopped])
    | New -> node##className <-  (cls_name [class_crs_entry; class_crs_entry_new])


				   
  (** Starting a course :
 updating status in list
   *)	      
  let start name =
    update_status_in_list name Started
    		
  let ask_replace fname =
    Js.to_bool (Dom_html.window##confirm(Js.string( fname)))

  let initialized = ref false
  let is_initialized () = !initialized
		 
  let init lang =
    initialized := true;
    set_lang lang;
    check_ids ();
    List.iter (fun (name,descr) -> add_course_to_list name descr) (M.get_list_of_courses ());
    M.New_courses_listeners.add (fun (name, descr) -> add_course_to_list name descr);
    M.Remove_courses_listeners.add (fun name -> ignore(remove_course_from_list name));
    M.Courses_status_listeners.add (fun (name,status) -> update_status_in_list name status)
       
				   

end

    

(** *************************** **)
(** controller  list of courses  *)
(** *************************** **)
      
module C  =
  struct
		  
    let rec start_course name : unit -> bool =
      (fun _ ->
       M.start_course name ;
       V.set_course_handler name (stop_course name);
       false
      )

    and stop_course name : unit -> bool =
      (fun _ ->
       M.stop_course name ;
       V.set_course_handler name (start_course name);
       false
      )
	
	
    let handle_new_crs (crs_list : (string * string) list) =
      List.iter
	(fun (name, dscr) -> ignore(V.set_course_handler name (start_course name)))
	crs_list
		 
			       
    let init () =
      if V.is_initialized () then
	handle_new_crs (M.get_list_of_courses())
      else
	()
      ;
	V.Courses_list_listeners.add (fun name -> ignore(V.set_course_handler name (start_course name)))	
				    
  end
