(**********************************************************)
(*  Online BSML  : Exercise loading                       *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(**********************************************************)
(** Loading prepared exercise. 
An exercise is an ocaml object file (cmo) that should have been prepared throught make_exercise.
See exercise/make_exercise.ml for more informations.

*)

(** We use a (badly implemented) MVC pattern to handle exercises.

* The model is managed in Loader module. 


* View module  Exercise_View : Displays the exercise panel. multi-lang.  
* Controler module exercise_controler : 
         React to actions on the view :
           - generates glue code to launch function testing
           - Add exercises

*)

open Loader
open Shared
open My_html

       
(* *********************** *)
(* *********************** *)
(* ****     View     ***** *)
(* *********************** *)
(* *********************** *)
module Exercise_View = struct
  let lang = ref ""
  let get_lang () = !lang
  let set_lang lg = lang:=(String.trim lg)

  let translate ?(ex_name="") ?(ex_field="") field =
    try
      List.assoc !lang field
    with
      Not_found  ->
      match field with
        [] -> ""
      | (_,descr)::_ -> Shared.log ~level:Warn ("no lang \""^(!lang)^"\" using first available for exo "^ex_name^" : "^ex_field); descr
                                           
                          
                     
    (* ids *)
  let exercise_loading_container = id  "exercises-files"
  let exercise_input = id  "exercise_loader"
  (* let exercise_button = id  "btn-exercise" *)
  let exercise_list = id  "exercise_list"

  let exercise_container = id  "exercises"
  let exercise_title = id  "exercise_title"
  let exercise_description = id  "exercise_description"
  let exercise_functions = id  "exercise_functions"
  let exercise_result_jauge = id  "exercise_result_jauge"
  let exercise_check_button = id  "exercise_check_button"

  (* classes *)
  let class_remove = cls "remove-button"
  let class_light_button = cls "light-button"
  let class_ex_entry_title = cls "exercise_list_title_class list-group-item"
  let class_ex_entry_descr = cls "exercise_list_descr_class"
  let class_ex_entry_done = cls "exercise_done"
  let class_ex_entry_started = cls "exercise_started"
  let class_ex_entry_new = cls "exercise_new"

  let class_loaded = cls "ready"
  let class_exercise_title = cls "exercise_title_class"
  let class_exercise_descr = cls "exercise_descr_class"

  let class_fun_name = cls "fun_name_class"
  let class_fun_descr = cls "fun_descr_class"
  let class_fun_container = cls "function_entry"
  let class_fun_entry_name = cls "function_entry_name"
  let class_fun_entry_descr = cls "function_entry_descr"
  let class_fun_entry_hint_button = cls "function_entry_hint_button fun_check small-button"
  let class_fun_entry_hint_code = cls "function_entry_hint_code"
  let class_fun_entry_hint = cls "function_entry_hint"
  let class_hint_revealed = cls "hint_revealed"
  let class_fun_entry_check_button = cls "fun_check small-button"
  let class_progress_bar_container = cls "progress_bars"
  let class_result = cls "function_result"
  let class_result_descr = cls "function_result_descr"
  let class_success = cls "function_success"
  let class_failure_explanation = cls "function_failure"

  let str_hint  = function "fr" -> "astuce" | "en" -> "hint" | _ -> failwith "no translation for str_hint"
  let str_check = function "fr" -> "tester" | "en" -> "test"  | _ -> failwith "no translation for str_check"
  let str_check_title f_name = function "fr" -> ("teste de votre fonction "^f_name) |  "en" -> ("testing your function "^f_name) | _ -> failwith "no translation for str_check_title"
  let str_show_hint_title f_name =
    function
    "fr" -> "Afficher une astuce pour "^f_name^" (attention, pour certains exercices le nombre d'astuces affichable est limité)"
  | "en" -> "Show a hint for "^f_name^"(warning, for some exercises the number of hints usable is limited) "
  | _ -> failwith "no translation for str_show_hint_title"
  let str_replace_file f_name =
    function
      "fr" -> "Voulez-vous remplacer le fichier "^f_name^" ?"
     | "en" -> "Do you want to replace the file "^f_name^" ?"
     | _ -> failwith "no translation for str_replace_file"
                     
  let str_wrong_type f_name =
    function
      "fr" -> "Votre fonction "^f_name^" n'a pas le bon type"
    | "en" -> "Your function "^f_name^" has not the good type"
    | _ -> failwith "no translation for str_wrong_type"


  let check () =
    let not_found = ref "" in
    let b = ref false in
    (* let handler id exn = match exn with Failure txt -> b:=true; not_found:=(!not_found^"\n"^txt) | _ -> raise exn *)
    (* in *)
    check_ids (* ~handler:handler *) ();
    if !b then failwith ("id checking fail : "^ !not_found)
	      
  (* ignore( by_id exercise_input ); *)
  (* ignore( by_id exercise_button ); *)
  (* ignore( by_id exercise_list ); *)
  (* ignore( by_id exercise_title ); *)
  (* ignore( by_id exercise_description ); *)
  (* ignore( by_id exercise_result_jauge ); *)
  (* ignore( by_id exercise_check_button ) *)

  (* Exercise loading from cmo file : input  *)
			
  let get_exo_input () = by_id_coerce exercise_input Dom_html.CoerceTo.input
  (* let get_exo_button () = by_id exercise_button *)

  let set_exo_input_handler handler =
    (get_exo_input ())##onchange <-
      Dom.handler (fun e ->
		   trace ~fname:"loading exercise from file"
			 (fun () -> handle_file_list exercise_input handler);
		   Js._false)
		  
  let set_exercise_check_handler check_handler =
    (by_id  exercise_check_button)##onclick <- ( Dom_html.handler (fun _ ->
								   check_handler ();
								   Js._false))

  (** List of exercises  *)

  (** getting the list container  *)
  let get_list_of_exercise course =
    let course = if course = "" then "" else course^"-"
    in
    by_id (id (course^exercise_list))

  (** computing the id of an exercise entry from it's name  *)
  let exercise_entry_id name = "exercise-entry-"^name
  let exercise_entry_remove name = "exercise-remove-"^name
							
  (** getting the exercise entry  *)
  let get_exercise_entry ?(do_log=false) name =
    by_id ~do_log:do_log (exercise_entry_id name)

  (** check whether an exercise has en entry  *)
  let in_list name =
    try ignore(get_exercise_entry ~do_log:false name); true with _ -> false

  (** removing an exercise from the list *)
  let remove_exercise_from_list name  =
    remove_node (get_exercise_entry name)

  let rec add_div div n =
    if n >0 then 
      (div##appendChild(
	    Tyxml_js.To_dom.of_node
	      (Tyxml_js.Html5.div []));
       add_div div (n-1))
    else
      ()
	
  let fill_with_empty_div size div =
    let div :> Dom.node Js.t  = div in
    let len = div##childNodes##length in
    if len > size then ()
    else
      add_div div (size - len)
	      
  (** adding an exercise to the list
    name identify the exercise and should be usable as html id
    exo is the module containing the exercise
    handler is the handler to be called when the entry is activated (clicked)
   *)
  let add_exercise_to_list  ?(pos=None) ?(course="") name (exo : (module EXERCISE)) handler =
    let module Exo = (val exo) in
    let description = Exo.description
    and exo_title = Exo.title
    and list_exos = get_list_of_exercise course 
    in
    if in_list name then ignore(remove_node (by_id (exercise_entry_id name))) else ();
    let node =
      Tyxml_js.To_dom.of_node
	  (Tyxml_js.Html5.(
			  div ~a:[a_id (exercise_entry_id name);
				  a_class [class_ex_entry_title; class_ex_entry_new];
				  a_onclick (handler)
				 ]
			      (* TODO *)
			      [
				button ~a:[ a_id (exercise_entry_remove name);
					    a_class [class_remove ; class_light_button];
					    a_title "remove exercise";
					    a_onclick (fun _ ->
						       ignore(remove_exercise_from_list name);
						       false
						      )
					  ][];
				pcdata (exo_title !lang);
				div ~a:[a_class [class_ex_entry_descr]]
				    [pcdata (description !lang)];
			      ]
			) 
	  )

      in
      begin
      	match pos with
      	  Some i ->
      	  fill_with_empty_div (i+1) list_exos;
      	  (match Js.Opt.to_option (list_exos##childNodes##item(i))with
      	     Some place -> list_exos##replaceChild(node,place)
      	   | None -> failwith ("fill_with_empty_div didn't worked as expected, load_exercise.")
      	  )
      	 | None ->
      list_exos##appendChild(node)
      end
	       

  let cls_name ls =
    Js.string (string_of_list  ~start:"" ~sep:" " ~stop:"" (fun i -> i) ls)

  (** updating status of an exercise  *)
  let update_status_in_list name status =
    let node = get_exercise_entry name in
    match status with
      Done -> node##className <- (cls_name [class_ex_entry_title; class_ex_entry_done])
    | Started -> node##className <-  (cls_name [class_ex_entry_title; class_ex_entry_started])
    | New -> node##className <-  (cls_name [class_ex_entry_title; class_ex_entry_new])
    | Stopped -> node##className <-  (cls_name [class_ex_entry_title])


  let fun_id ex_name f_name = ("function-"^ex_name^"-"^f_name)
  let fun_hint_id ex_name f_name = "hint-"^(fun_id ex_name f_name)
  let fun_hint_code_id ex_name f_name = "hint_code-"^(fun_id ex_name f_name)
  let fun_show_hint_id ex_name f_name = "show_hint-"^(fun_id ex_name f_name)

						       
  (** Show hint button  *)
  let show_hint_button ex_name (f_descr : fun_descr) =
    Tyxml_js.Html5.(
		   if translate ~ex_field:("hint"^f_descr.name)  f_descr.hint="" then
		     button ~a:[a_id (fun_show_hint_id ex_name (f_descr.name));
				a_class [class_fun_entry_hint_button ];
				a_style "display:none";
				a_title (str_show_hint_title (f_descr.name)  !lang) ]
			    [pcdata (str_hint !lang) ]
		   else
		     button ~a:[a_id (fun_show_hint_id ex_name (f_descr.name));
				a_class [class_fun_entry_hint_button ];
				a_title (str_show_hint_title (f_descr.name)  !lang) ]
			    [pcdata (str_hint !lang)]
		 )
  let progress_bar_id  ex_name f_name =
    let id =  fun_id ex_name f_name in
    "progress_"^id

		  
  let progress_bar ex_name f_name =
    let id =  progress_bar_id ex_name f_name in
    Tyxml_js.Html5.(
		   div ~a:[a_id id;
			   a_class [class_progress_bar_container ]]
		       []
		 )
  (** fun check button  *)
  let fun_check_id   ex_name f_name =
    let id =  fun_id ex_name f_name in
    "fun_check_"^id
		   
  let fun_check_button name f_name =
    let id =  fun_check_id name f_name in
    Tyxml_js.Html5.(
		   button ~a:[a_id id;
			      a_class [class_fun_entry_check_button ];
			      a_title (str_check_title f_name !lang)
			     ]
			  [pcdata (str_check  !lang)]
		 )

		 
  (** Starting an exercise :
 updating status in list
 loading exercise description in ad-hoc panel
   *)	      
  let start name (exo : (module EXERCISE)) =
    let module Exo= (val exo) in
    update_status_in_list name Started;
    let description = Exo.description
    and exo_title = Exo.title
    in
    begin
      (by_id exercise_container)##className <- (Js.string class_loaded);
      (* attach title *)
      (by_id exercise_title)##innerHTML <- (Js.string (exo_title !lang));
      (* attach description *)
      (by_id exercise_description)##innerHTML <- (Js.string (description !lang));
      (* for each function, make an entry *)
      let funs = (by_id exercise_functions) in
      remove_children funs;
      funs##innerHTML <- (Js.string "");
      List.iter
	(fun f_descr ->
	 let fun_entry =  Tyxml_js.To_dom.of_node Tyxml_js.Html5.(
						    div ~a:[a_id (fun_id name (f_descr.name));
							    a_class [class_fun_container]
							   ]
							[ div ~a:[a_class [class_fun_entry_name]]
							      [pcdata ((f_descr.name)^" : "^f_descr.typ) ] ;
							  div ~a:[a_id (fun_id name (f_descr.name) ^"-descr");
								  a_class [class_fun_entry_descr]]
							      [ ];
							  div ~a:[a_id (fun_hint_id name (f_descr.name));
								  a_class [class_fun_entry_hint]]
							      [ ];
							  div ~a:[a_id (fun_hint_code_id name (f_descr.name));
								  a_class [class_fun_entry_hint_code]]
							      [ ];
							  progress_bar name (f_descr.name) ;
							  fun_check_button name (f_descr.name);
							  show_hint_button name f_descr
							]
						  )
	 in
	 ignore(funs##appendChild(fun_entry))
	)
	Exo.function_descriptors;
      List.iter
	(fun f_descr ->
	 let fun_entry = by_id ((fun_id name (f_descr.name) ^"-descr"))in
	 fun_entry##innerHTML <-(Js.string (translate  ~ex_name:(exo_title !lang)  ~ex_field:("descr "^f_descr.name) f_descr.descr))
	)
	Exo.function_descriptors
    end

  let stop_exercise () =
    log "stop exercise";
    (* attach title *)
    let clear node = remove_children node in
    clear (by_id exercise_title);
    (* attach description *)
    clear (by_id exercise_description);
    (* for each function, make an entry *)
    clear (by_id exercise_functions) 
			 
			 
  let reinit lang =
    log "reinit view";
    set_lang lang;
    match Loader.get_started () with
      None -> ()
    | Some ( name , exo) -> (stop_exercise();start name exo)
						  
  let set_function_handlers check_handler hint_view_handler ex_name f_descr =
    let id =  fun_id ex_name (f_descr.name)
    and hint_id = fun_hint_id ex_name (f_descr.name)
    and hint_code_id = fun_hint_code_id ex_name (f_descr.name)
    and show_hint_id = fun_show_hint_id ex_name (f_descr.name)
    and check_id = fun_check_id ex_name (f_descr.name)
    in		    
    (by_id  check_id)##onclick <- ( Dom_html.handler (fun _ ->
						      log ("check "^ex_name^" "^(f_descr.name));
						      check_handler ();Js._false));
    (by_id  show_hint_id)##onclick <- (
      Dom_html.handler
	(fun btn ->
	 log (hint_id^" set hint "^ex_name^" "^(f_descr.name)^" "^(translate f_descr.hint));
	 (by_id (hint_id))##innerHTML <- (Js.string (translate f_descr.hint)) ;
	 log (hint_code_id^" set hint_code "^
		ex_name^" "^(f_descr.name)^" "^f_descr.hint_code);
	 (by_id (hint_code_id))##innerHTML <- (Js.string  f_descr.hint_code);
	 log (hint_id^" reveal "^ex_name^" "^(f_descr.name));
	 (by_id (id))##className <-(cls_name [class_fun_container;class_hint_revealed]);
	 hint_view_handler ();
	 Js._false
	))
					
					
  let rec view_of_result res =
    match res with
      Success -> Tyxml_js.Html5.(div ~a:[a_class[class_result; class_success]][])
    | Fail reason ->  
       Tyxml_js.Html5.(
		      div ~a:[a_class [ class_result; class_failure_explanation ] ]
			  [ Unsafe.data reason]
		    )
    | Progress res_list -> build_progress_bar res_list
  and build_progress_bar rl =
    let rec aux  (rl: (string*result) list) =
      match rl with
	[] -> []
      | (descr, res)::t ->
	 Tyxml_js.Html5.(
			div ~a:[a_class [ class_result_descr]]
			    [pcdata descr ;
			     view_of_result res]
		      )::(aux t)
    in
    Tyxml_js.Html5.(
		   div  ~a:[a_class [class_result ; class_progress_bar_container]] (aux rl)
		 )
		 
  let remove_all dv =
    let chld_lst = dv##childNodes  in
    let len = chld_lst##length in
    for i = 0 to len -1 do
      Js.Opt.case (chld_lst##item(i))
		  (fun () -> failwith "Load_exercise.remove_all: not reachable")
		  (fun v -> dv##removeChild(v))
    done
      
  let update_view_of_result res exo_name f_name =
    let dv = (by_id (progress_bar_id exo_name f_name) ) in
    remove_all dv;
    dv##appendChild(Tyxml_js.To_dom.of_node(view_of_result res))


      
  let ask_replace fname =
    Js.to_bool (Dom_html.window##confirm(Js.string(str_replace_file fname  !lang)))

  let alert msg =
    Dom_html.window##alert(Js.string(msg))

  let init lang =
    set_lang lang;
    check ()


end
(* *********************** *)
(* *********************** *)
(* ****  Fin View    ***** *)
(* *********************** *)
(* *********************** *)
			 
let handle_result ex_name f_name (test_result : unit -> Loader.result) =
  let test_result = Loader.catch_test
		      (fun exn -> match exn with
			    (Js.Error err) ->  if err##name = (Js.string "TypeError")
					       then Fail (Exercise_View.str_wrong_type f_name  (Exercise_View.get_lang ()))
					       else raise exn
			  | _ -> raise exn
	      )
	      test_result
  in
  begin    
    log (Loader.string_of_result test_result);
    print_endline (Loader.string_of_result test_result);
    ignore (Exercise_View.update_view_of_result test_result ex_name f_name) ;
    test_result
  end;

    
(* *********************** *)
(* *********************** *)
(* ****   Controller  **** *)
(* *********************** *)
(* *********************** *)
			 
module Exercise_Controler  (Exec : sig val exec : string -> bool end) =
  struct
    module EV = Exercise_View
		    
		  
    let function_check_handler ex_name f_descr = 
      let lang = Exercise_View.get_lang () in
      fun _ ->(
      log ("checking "^ex_name^" "^(f_descr.name));
      let type_check =
	"module M : (sig val "^(f_descr.name)^" : "^f_descr.typ^" end) = struct let "^(f_descr.name)^" = "^(f_descr.name)^" end\n"
	^ "module type T = module type of (struct let "^(f_descr.name)^" = "^(f_descr.name)^"  end)\n"^
	  "module M' : T = M "
      and command = 
	"let module Exo = (val (Loader.get_exercise \""^ex_name^"\")) in
	 let log_state = Bsml.logging_state () in
	     (if log_state then Bsml.stop_logging() );
	     let res = Load_exercise.handle_result \""^ ex_name^"\" \""^
	       (f_descr.name)^"\" 		     
			     (fun () ->
			     Exo.get_fun_tester \""^
		 (f_descr.name)^"\"  "^(f_descr.name)^")
	     in
              (if log_state then Bsml.start_logging());
	      res"
      in
      let open Lwt in
      Lwt.async ( fun () ->
		  Lwt.pause  () >>= fun () ->
		  log ("checking "^ex_name^" "^(f_descr.name)^" : "^command);
		  ( try
		      if Exec.exec type_check then 
			ignore(Exec.exec command)
		      else
			ignore(handle_result ex_name (f_descr.name)
					     (fun () -> Fail ("Erreur de Type")))
		    with
		      exn ->
		      ignore(handle_result ex_name (f_descr.name)
					   (fun () -> Fail ("error while executing test"^(Printexc.to_string exn))))
		  )
		  ;
		    Lwt.return_unit
		);
      false)
		      
			     
    let function_show_hint_handler ex_name  f_descr = fun _ -> ()

    let reinit () =
      match Loader.get_started () with
	None -> ()
      | Some ( name , exo) ->
	 let module Exo = (val exo) in
	 begin
	   List.iter
	     (fun  f_descr ->
	      EV.set_function_handlers (function_check_handler name f_descr)
				       (function_show_hint_handler  name f_descr)
				       name
				       f_descr
	     )
	     Exo.function_descriptors;
	   let all_check = List.map
			     (fun  f_descr -> function_check_handler name f_descr)
			     Exo.function_descriptors
	   in
	   EV.set_exercise_check_handler
	     (fun _ -> List.iter (fun f -> ignore(f ())) all_check ; false)
	 end


    let start_exercise name exo =
      (fun _ ->
       if Loader.get_started() = Some (name, exo) then
	 (* log "start exercise" *)
	  ()
       else
	 begin
	   Loader.start name exo;
	   EV.start name exo;
	   let module Exo = (val exo) in
	   List.iter
	     (fun  f_descr ->
	      EV.set_function_handlers (function_check_handler name f_descr)
				       (function_show_hint_handler  name f_descr)
				       name
				       f_descr
	     )
	     Exo.function_descriptors;
	   let all_check = List.map
			     (fun  f_descr -> function_check_handler name f_descr)
			     Exo.function_descriptors
	   in
	   EV.set_exercise_check_handler
	     (fun _ -> List.iter (fun f ->  ignore (f ()) ) all_check ; false)
	   
	 end;
       false)

    let handle_new_exos ?(course="") ?(pos=None) (exo_list : (string * (module Loader.EXERCISE)) list) =
      List.iter
	(fun (name, exo) -> ignore(EV.add_exercise_to_list ~pos ~course name exo (start_exercise name exo)))
	exo_list

    let normalize_file_name fname =
      String.map (function ' '|'-'|'\t'|'\n'|'\''|'"' -> '_' | c -> c) fname

		 
    let new_exercise_handler ?(pos=None) course fname =
      trace ~fname:("new_exercise_file_distant_handler "^course^" "^fname)
    	    (fun () ->
	     handle_new_exos ~pos ~course
			     (Loader.load_exercise EV.alert Exec.exec fname)
	    )

    let new_exercise_file_handler file=
      let reader = jsnew File.fileReader() in
      begin
	reader##readAsBinaryString(file);
	reader##onload <-
	  Dom.handler (fun _ ->
		       let code =
			 Js.to_string (Js.Opt.get (File.CoerceTo.string reader##result)
						   (fun ()-> failwith "no result"))
		       in
		       let fname = "/exos/"^(Js.to_string file##name) in
		       let fname = normalize_file_name fname in
		       if Sys.file_exists fname then
			 if EV.ask_replace fname then
			   begin
			     Sys.remove fname;
			     Sys_js.register_file fname code;
			     trace ~fname:"new_exercise_file_handler"
				   (fun () ->
				    handle_new_exos
				      (Loader.load_exercise EV.alert Exec.exec fname)
				   )
			   end
		       	 else
			   ()
		       else
			 begin
			   Sys_js.register_file fname code;
			   trace ~fname:"new_exercise_file_handler"
				 (fun () ->
				  handle_new_exos
				    (Loader.load_exercise EV.alert Exec.exec fname)
				 )
			 end
		    ;
		   Js._false
		  )
      end
	
    let set_new_exercise_file_handler () =
      EV.set_exo_input_handler new_exercise_file_handler


			       
    let init () =
      set_new_exercise_file_handler ()
  end

