(**********************************************************)
(*  Online BSML  : HTML helpers                           *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(**********************************************************)
open My_misc
open Shared

(* TODO mv splitters in misc *)
let split_at_first splitter str  =
  let splt_start = String.get splitter 0 
  and splt_len = String.length splitter
  in 
  let rec next init_pos start_pos :(string*int)=
    let i_start = String.index_from str start_pos splt_start
    in
    (* print_string "found start at "; *)
    (* print_int i_start; *)
    if (String.sub str i_start splt_len  = splitter ) then
      (
	(* print_endline " and it match.\n"; *)
	(String.sub str init_pos (i_start - init_pos),
         i_start + splt_len)
      )
    else
      next init_pos (i_start+1)
  in
  let (st , nxt_pos) =  next 0 0 in
  (* print_endline ("\nmatch from"^(string_of_int start)^" to "^(string_of_int nxt_pos)^"\n"); *)
   (st,String.sub str nxt_pos (String.length str - nxt_pos))

let split splitter str  =
  let splt_start = String.get splitter 0 
  and splt_len = String.length splitter
  in 
  let rec next init_pos start_pos :(string*int)=
    let i_start = String.index_from str start_pos splt_start
    in
    (* print_string "found start at "; *)
    (* print_int i_start; *)
    if (String.sub str i_start splt_len  = splitter ) then
      (
	(* print_endline " and it match.\n"; *)
	(String.sub str init_pos (i_start - init_pos),
         i_start + splt_len)
      )
    else
      next init_pos (i_start+1)
  in
  let rec aux (start:int) (acc:string list) =
      try
	let (st , nxt_pos) =  next start start in
	(* print_endline ("\nmatch from"^(string_of_int start)^" to "^(string_of_int nxt_pos)^"\n"); *)
	aux nxt_pos (st::acc)
      with  Not_found  ->
	List.rev
	  ((String.sub str start (String.length str - start)) :: acc)
  in
  aux 0 []
      
(* let test = split " : " "qdslkjf  :dqs f  ! :df :qdf  d: dsqf" *)

(** Memorising all id used in order to check them all at once  *)
let all_ids : string list ref = ref []
let add_id norm_id = all_ids:= norm_id :: !all_ids

(** Memorising all class name used in order to check them all at once 
not sure it's possible to check existance of class in css
 *)
let all_cls : string list ref = ref []
let add_cls norm_cls = all_cls:= norm_cls :: !all_cls

(* class and id name should follow a given syntaxe *)
let normalise_attribute s =  s

(** normalising and registering an id  *)
let id s =
  let nid = normalise_attribute s
  in add_id nid ;
     nid

(** normalising and registering a class  *)
let cls cls =
  let splt = split " " cls in
  let ncls = List.map normalise_attribute splt
  in List.iter add_cls ncls ;
     string_of_list ~start:""  ~stop:""  ~sep:" " (fun i ->i) ncls

let check_ids ?(handler=(fun id exn -> Shared.log ("Check failed for id "^id);raise exn)) () =
  (match duplicate (fun (e,i) _ -> (e,i+1)) (fun e -> (e,1)) (=) !all_ids with
    [] -> ()
  | l -> failwith ("duplicated ids: "^(string_of_list (fun (id,n) -> " "^(string_of_int n) ^" x "^id^" ") l))
  )
  ; 
  List.iter (fun id ->  try ignore(by_id id) with exn -> handler id exn ) !all_ids 

(** remove first child  *)
let remove_first_child elt =
  Js.Opt.case
    (elt##firstChild)
    (fun () -> false)
    (fun e -> ignore(elt##removeChild(e));true)

(** Make a popup and fill it with an element of the current page.
   Needs ssw.html like page to build the popup.
  *)

let move elt container =
  let elt :> Dom.node Js.t = elt
  and container :> Dom.node Js.t = container
  in
  container##appendChild(elt)

let push_out id_push ?(options="menubar=no,scrollbars=yes") ?(url="ssw.html?") target =
  let out_w = Dom_html.window##open_(Js.string (url^id_push),Js.string target,Js.some (Js.string options))
  in
  out_w##focus(); out_w

(* if container in popup is empty, close it *)
let push_back_in out_w elt dest =
  let container = noopt(elt##parentNode)
  in
  ignore(move elt dest);
  print_int (container##childNodes##length);
  if container##childNodes##length <= 1 then
    out_w##close()

let class_detach_button = cls "detach_button button"
let txt_raw_detach_button =  "detach as a popup"
let txt_detach_button =  Js.string txt_raw_detach_button
let txt_reattach_button =  Js.string "reattach to main"

let class_detached = cls "detached"
                                     
let rec switch_reattach_button (elt_button : Dom_html.element Js.t) out_w (elt : Dom_html.element Js.t) elt_id old_parent =
  begin 
  elt##classList##add(Js.string class_detached);
  elt_button##innerHTML <- txt_reattach_button;
  elt_button##onclick <- Dom_html.handler (
                               fun _ ->
                                push_back_in out_w elt old_parent;
                                switch_detach_button elt_button elt elt_id old_parent ;
                                Js._false                                          
                 ) 
  end
                           
and switch_detach_button elt_button elt elt_id parent =
  elt##classList##remove(Js.string class_detached);
  elt_button##innerHTML <- txt_detach_button;
  elt_button##onclick <- Dom_html.handler (
                             fun _ ->
                             let out_w = push_out elt_id  elt_id in
                             switch_reattach_button elt_button out_w elt elt_id  parent;
                             Js._true
                           )
  
let make_detachable id =
  let elt = by_id id in
  let node :> Dom.node Js.t = by_id id in
  let elt_button : Dom_html.element Js.t =
    Tyxml_js.To_dom.of_element(Tyxml_js.Html5.(span ~a:[a_class [class_detach_button]] [pcdata txt_raw_detach_button]))
  in let  node_button  :> Dom.node Js.t = elt_button 
  in
  ignore(node##insertBefore(node_button,node##firstChild));
  switch_detach_button elt_button elt id (noopt(elt##parentNode))
                       

let iter_node_list nl f = 
  let length = nl##length in
  for i=0 to length-1 do
    Js.Opt.case (nl##item(i)) (fun () -> failwith "no accessible item") (f) 
  done
