(******************************************************************)
(*  Online BSML : Shared functions btw. REPL and compiled program *)
(*  February 2016	          	                          *)
(*  Author: Julien Tesson     				          *)
(*  Laboratoire LACL - Université Paris-est Créteil               *)
(******************************************************************)

let debug = try Some (Dom_html.getElementById "debug") with  _ -> None

type log_level = Info | Warn | Error
                                
let log ?(level=Info) ?(use_debug_div=false) ?(use_debug_firebug=true) s =
  (if use_debug_firebug  then
     match level with
       Info -> 
       Firebug.console##log(Js.string s)
     | Warn -> 
        Firebug.console##warn(Js.string s)
     | Error -> 
        Firebug.console##error(Js.string s)
  )
  ;
    (if use_debug_div  then
       match debug with 
         Some debug -> debug##innerHTML <- Js.string ((Js.to_string debug##innerHTML)^"<p>"^s^"</p>") 
       | None -> () 
    )
	  
let trace ?(fname="") f  = 
  try
    log fname;
    Printexc.record_backtrace true ;
    f ();
    Printexc.record_backtrace false
  with ex ->
    log (fname^" "^(Printexc.to_string ex));
    raise ex

	  	  
let trace_ignore ?(fname="") f  = 
  try
    log fname;
    Printexc.record_backtrace true ;
    ignore(f ());
    Printexc.record_backtrace false
  with ex ->
       log (fname^" "^(Printexc.to_string ex));
       ()


let by_id ?(do_log=true) id =
  try
    Dom_html.getElementById id
  with  _ -> let msg = ("element "^id^" not found") in
	     if do_log then log msg;
	     failwith msg

let by_class ?(do_log=true) classe =
  try
    Dom_html.document##getElementsByClassName (Js.string classe)
  with  _ -> let msg = ("element "^classe^" not found") in
	     if do_log then log msg;
	     failwith msg
                      
let first_by_tag ?(do_log=true) tag =
  Js.Opt.get (Dom_html.document##getElementsByTagName(Js.string tag)##item(0))
             (fun () ->
               let msg = ("no element tagged "^tag^" found") in
	       if do_log then log msg;
	       failwith msg)

let getAttribute elt attr=
  Js.to_string (Js.Opt.get (elt##getAttribute(Js.string attr)) (fun () -> raise Not_found))

let by_id_coerce s f  = Js.Opt.get (f (Dom_html.getElementById s)) (fun () -> raise Not_found)
let do_by_id s f = try f (Dom_html.getElementById s) with Not_found -> ()

let find_div id  =try by_id_coerce id  Dom_html.CoerceTo.div with _ -> let msg = ("div "^id^" not found") in log msg; failwith msg

let find_tr id  =try by_id_coerce id  Dom_html.CoerceTo.tr
		 with _ -> let msg = ("tr "^id^" not found") in log msg; failwith msg

let find_canvas id  =try by_id_coerce id  Dom_html.CoerceTo.canvas with _ -> let msg = ("canvas "^id^" not found")in log msg; failwith msg

let find_input id  =
  try
    by_id_coerce id  Dom_html.CoerceTo.input
  with exn -> let msg = ("input "^id^" not found:"^(Printexc.to_string exn))in log msg; failwith msg
									 

let td_of s =
  let td = Dom_html.createTd Dom_html.document in
  begin
    td##innerHTML <- s;
    td
  end


(* handling of the list of files *)
let handle_file_list input_id f  =
  let file_input = find_input input_id  in
  let file_list_input =
    Js.Optdef.case (file_input##files) (fun () -> failwith "no files") (fun a -> a) in
  let length = file_list_input##length in
  let file_list =
    let rec aux i l =
      if i=0 then l
      else
	aux (i-1)
	    (* removing null files *)
	    (Js.Opt.case (file_list_input##item(i-1)) (fun () -> l) (fun a -> a::l)) 
    in
    aux length []
  in
  List.iter f file_list


let no_some opt = match opt with
    None ->  raise Not_found
  | Some t -> t
  
let noopt t = no_some (Js.Opt.to_option t)
let nooptdef t = no_some (Js.Optdef.to_option t)
    

	    

(* file_system *)
let ls s = Sys.readdir s

let foreach_line file f=
  let ic = open_in file in
  try
    while true do
      let line = input_line ic in
      f line
    done
  with End_of_file -> close_in ic
  
let cat file =       
      foreach_line file print_endline
		       
(* / file_system *)

let string_of_list ?(start="[") ?(sep=";") ?(stop="]") string_of_data lst =
  let rec str_of_list_aux str lst =
    match lst with
    | [] -> failwith "unreachable state"
    | [d] -> str ^(string_of_data d)
    | h::t -> str_of_list_aux (str ^ (string_of_data h)^sep) t
       
  in
  match lst with
    [] -> start^stop
  | h::t -> (str_of_list_aux start lst)^stop

let remove_node node =
    let entry:> Dom.node Js.t =  node in
  let parent =
    Js.Opt.get (entry##parentNode)
	       (fun () -> failwith "removing entry wihtout parent")
  in
  parent##removeChild(entry)

let remove_children node =
  let entry:> Dom.node Js.t =  node
  in
  let children = entry##childNodes in
  for i=(children##length - 1) downto 0  do
    let child =  Js.Opt.get (children##item(i))
			    (fun () -> failwith "removing non-existing node")
    in
    ignore(entry##removeChild(child))
  done


let remove_by_id id =
  let node = by_id id in
  remove_node node


type status = Done | Started  | Stopped | New
let string_of_status s=
  match s with
    Done-> "Done"
  | Started -> "Started"
  | Stopped -> "Stopped"
  | New -> "New"

let alert msg =
  Dom_html.window##alert(Js.string(msg))

				 
(* let ss_div 'a Js.t = ref try by_ *)

(* let made_detachable = ref bool *)
                          
                 
let (+=) a b = a:=!a+b

module type UNIT = sig end
module Unit : UNIT = struct end
