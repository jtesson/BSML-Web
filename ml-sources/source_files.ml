(**********************************************************)
(*  Online BSML  : loading source files                   *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(**********************************************************)
open Shared
module type ENV =
  sig
    val container : string
    val file_container : string
    val exec : string -> bool
    val edit : string -> bool
  end
       
module SourceFiles (Env :ENV) =
  struct

    let handle_source file handler (f: bool -> unit)  =
      let reader = jsnew File.fileReader() in
      begin
	reader##readAsText(file);
	reader##onload <-
	  Dom.handler (fun _ ->
		       let code =
			 Js.to_string (Js.Opt.case (File.CoerceTo.string reader##result) (fun ()-> failwith "No result for source file loading") (fun a -> a))
		       in
		       f (handler code); Js._false
		      )
      end

    let exec_source file f = handle_source file Env.exec f
    let edit_source file f = handle_source file Env.edit f

    
    type file_state = New | Loaded | Failed

    let str_of_state s = match s with New -> "New" | Loaded -> "Loaded" | Failed -> "Failed"
    let state_of_str s = match s with "New" -> New | "Loaded" -> Loaded | "Failed" -> Failed | _ -> failwith (s^" not a file state")

    let class_source_ref = My_html.cls "ml-source-ref"
    let class_source_file = My_html.cls "class_source_file list-group-item"
    let class_source_name = My_html.cls "source-name"	
    let class_source_edit = My_html.cls "edit-source"	
    let class_source_load =My_html.cls "load-source"
    let class_source_remove =My_html.cls "remove-button"
    let class_light_button =My_html.cls "light-button"
    let class_light_text_button =My_html.cls "light-text-button"
    let class_closed= My_html.cls "closed"
				  
    module T = Tyxml_js.Html5

    type model = { name : string ; file : File.file Js.t ; state : file_state ref}
     
    let file_list :model ref list ref = ref []

    let fresh_name name =
      let name_length = String.length name in
      let homonymes =
	List.filter
	  (* keeps only name with file name as prefix *)
	  (fun fm ->
	   let fm_name = !fm.name in
	   let fm_length = String.length  fm_name
	   in
	   if fm_length < name_length then
	     false
	   else
	     let prefix = String.sub fm_name 0 name_length
	     in  prefix = name 		  

	  )
	  !file_list
      in
      match homonymes with
	[] -> name
      | _ -> name^(
	  string_of_int
	    (1 +
	       (List.fold_left
		  max
		  (-1)
		  ( List.map (fun fm -> let name' =  !fm.name in
					let len_name' = String.length name' in
					let num = String.sub
						    name'
						    name_length
						    (len_name' - name_length)
					in 
					match num with
					  "" -> -1
					| s -> try int_of_string s with _ -> -1
			     )
			     homonymes
		  )
	       )
	    )
	)

    let file_list_view = find_div Env.file_container
    let container_view = find_div Env.container

    let get_state fm = !(!fm.state)

			
    let rec view_of_file file_model =
      let name = (!file_model).name
      and state = !((!file_model).state) in
      let raw =T.(a ~a:[a_id name ; a_class [class_source_file; str_of_state state]]
		     [
		       button ~a:[ a_class [class_source_name ; class_light_text_button]] [ pcdata name ];
		       button ~a:[ a_class [class_source_load ; class_light_button] ;
				   a_title  "Load source in the toplevel";
			       a_onclick (fun _ ->
					  exec_source
					    !file_model.file
					    (fun b -> if b then
							update_file file_model Loaded
						      else
							update_file file_model Failed
					    ); false
					 )]
			  [];
		       button ~a:[ a_class [class_source_edit ; class_light_button] ;
				   a_title "Edit source in the editor panel";
				   a_onclick (fun _ ->
					  edit_source
					    !file_model.file
					    (fun b -> ()
					    ); false
					 )]
			      [];
		       button ~a:[ a_class [class_source_remove ; class_light_button];
				   a_title "remove source from list";
				   a_onclick (fun _ ->
					   remove_file file_model; false
					  )
			      ][]
		     ]
		 )
      in
      Tyxml_js.To_dom.of_node raw

    and add_file_view (file_model : model ref) =
      container_view##classList##remove(Js.string class_closed);
      file_list_view##appendChild(view_of_file file_model)

    and remove_file_view (file_model : model ref) =
      let tr:> Dom.node Js.t = by_id !file_model.name in
      let parent =
	Js.Opt.get (tr##parentNode) (fun () -> failwith "removing tr wihtout parent")
      in
      parent##removeChild(tr);
      if file_list_view##childNodes##length = 0 then
	container_view##classList##add(Js.string class_closed)
      else
	()
	    
    and update_file_view (file_model : model ref) =
      try
	let file_container = by_id !file_model.name in
	file_container##className <- (
	  Js.string (class_source_file^" "^(str_of_state (get_state file_model))))
      with _ ->
	ignore (add_file_view file_model)

		      
    and add_file_model (file_model : model ref) =
      (* checking that no file with same name appears *)
      file_list:=  file_model :: !file_list

    and remove_file_model (file_model : model ref) =
      file_list:=  List.filter (fun fm -> not(fm == file_model)) !file_list
			       
    and update_file_model (file_model : model ref) state =
      !file_model.state:= state
			  


    and add_file (f:File.file Js.t) =
      let name = fresh_name (Js.to_string (f##name))
      in
      let (file_model : model ref) = ref {name=name ; file=f ; state=ref New } in
      add_file_model file_model;
      add_file_view file_model;
      ()

    and remove_file (file_model : model ref) =
      remove_file_model file_model;
      remove_file_view file_model

    and update_file file_model state =
      update_file_model file_model state;
      update_file_view file_model

    let file_input = My_html.id "input-file"
		       
    let setup_load_file_button ?(input=file_input) () =
      (find_input input)##onchange <- Dom_html.handler (fun _ ->
						      trace (fun _ -> ignore (handle_file_list "input-file" add_file))
						      ; Js._false
						     )
  end
