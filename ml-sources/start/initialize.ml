(**********************************************************)
(*  Online BSML  : BSML Toplevel initialisation           *)
(*  February 2016	          	                  *)
(*  Author: Julien Tesson     				  *)
(*  Laboratoire LACL - Université Paris-est Créteil       *)
(**********************************************************)

(**  File to be read at the initialisation of the toplevel.
 
The file first grab the bsp parameters in the web page, then build a module describing the BSP machine and make a Bsml REPL based on this machine.
BSML is instrumented with JsInstrumentation, which produces graphical representation of executed supersteps.

Observation model is for measurement for exercises corrections (If I remember right ... don't comment your code one year later)
 *)

let _ = Shared.log "Loading initialize.ml"
open Shared


(* Opening BSML env *)
let () = Embeded_bsml.init_bsml ()
module BsmlEnv =  (val Embeded_bsml.get_current_bsml_env ())
let () =  BsmlEnv.init_msg (); 
          BsmlEnv.init_output ()
open BsmlEnv

(* Opening tools *)
       
open My_html
                                          
       (*  Print Version *)
#use "start/version.ml";;
#use "start/reference_protection.ml"
