
module ProtectedRef =struct
exception Reference_misusage of string 
  open BsmlWithLocality
  let check_write_level l = match l, Bsml.current_level() with
      Global,Local ->
        raise (Reference_misusage "writing global reference at local level") 
    | _ -> ()
         
  type 'a ref = {mutable content : 'a;  level : level}
              
  let ref content = {content; level= Bsml.current_level ()}
                
  let (!) r = r.content
  let (:=) r v = check_write_level r.level;  r.content<-v

  let incr (r:int ref) = r:=succ !r
  let decr (r:int ref) = r:=pred !r
end
                       
open ProtectedRef
