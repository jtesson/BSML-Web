(* js_of_Bsml toplevel 
 * 
 * 2016 Julien Tesson
 * Laboratoire LACL - Université Paris-est Créteil
 *
 *  Based upon
 * Js_of_ocaml toplevel 
 * http://www.ocsigen.org/js_of_ocaml/
 * Copyright (C) 2011 Jérôme Vouillon
 * Laboratoire PPS - CNRS Université Paris Diderot
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, with linking exception;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *)

(* Gathering compiler/code version info *)
#ifndef OCAML
let compiler_name = "OCaml"
#else
let compiler_name = OCAML
#endif
                  
#ifndef VERSION
#warning "VERSION is undefined"
#define VERSION "n/a"
#endif
                  
let version = VERSION
            
open Lwt

open Shared
open My_html
let loading_panel =id "loading-panel"
let loading_descr =id "loading-descr"
let loading_text =id "loading-text"
let loading_counter =id "loading-counter"
let loading_progress = id "loading-progress"
let toplevel_container = id  "toplevel-container"
let output = id "output"
let editor_menu = id "editor_menu"
(* let main_menu = id "main-menu" *)
(* let main_menu_button = id "main-menu-button" *)
let reinit_instrumentation= id "reinit-instrumentation"
let editor_load_button =id "editor_load_button"
and editor_eval_button=id "editor_eval_button"
and editor_eval_selection_button =id "editor_eval_selection_button"
and editor_save_button = id "editor_save_button"		    
and editor_share_btn=id "btn-share-editor"
and editor_load_selection_button = id "editor_load_selection_button"
let btn_share=id "btn-share"
let btn_reset=id "btn-reset"

let cls_open = cls "open"
let cls_closed = cls "closed"
let class_loading_end = cls "loading-end"

let loading_percent = ref 0
let loading_steps =  16
                    
let progress_next descr =
  try
    loading_percent += (100/loading_steps);
      Shared.log ("Loading : "^descr);
      let progress_counter =  (by_id loading_counter) in
      let progress_bar =  (by_id loading_progress) in
      let descr_node =  (by_id loading_text) in
      let new_count = Js.string (string_of_int !loading_percent) in
      begin
        progress_bar##setAttribute(Js.string "value",new_count);
        
        remove_first_child descr_node;
        let text = Dom_html.document##createTextNode(Js.string ("Loading : "^descr)) in
        let txt_node :> Dom.node Js.t =  text in
            ignore(descr_node##appendChild(txt_node));
        remove_first_child progress_counter;
        let text = Dom_html.document##createTextNode(new_count) in
        let txt_node :> Dom.node Js.t =  text in
            ignore(progress_bar##appendChild(txt_node));
                        
      end
                    
  with
    _ -> Shared.log ~level:Error ("fail show Loading : "^descr)

                    
let progress_finished ()= 
  try
    (by_id loading_panel)##classList##add(Js.string class_loading_end);
  with
    _ -> Shared.log ("Loading : end")

                     
		     
let str_clear_editor= function "fr" -> "Êtes-vous sûr de vouloir remplacer le contenu actuel de l'éditeur ?"
                             | "en" -> "Warning : This will replace the current content of this editor, continue ?"

(*Creating Pseudo filesystem that either load file using a synchronous XMLHttpRequest or get them from browser local storage*)
let load_resource_aux' url =
  try
    let xml = XmlHttpRequest.create () in
    xml##_open(Js.string "GET", url, Js._false);
    xml##send(Js.null);
    if xml##status = 200 then Some (xml##responseText) else None
  with _ -> None

let load_resource_aux url =
  try
    let xml = XmlHttpRequest.create () in
    xml##_open(Js.string "GET", url, Js._false);
    xml##send(Js.null);
    if xml##status = 200 then Some (Js.to_string (xml##responseText)) else None
  with _ -> None

let load_resource scheme (_,suffix) =
  let url = (Js.string scheme)##concat(suffix) in
  load_resource_aux' url

let setup_pseudo_fs ?(storage="Ocaml_fs") () =
  progress_next "pseudo_fs";
  Sys_js.register_autoload' "/dev/" (fun s -> Some (Js.string ""));
  Sys_js.register_autoload' "/http/"  (fun s -> load_resource "http://" s);
  Sys_js.register_autoload' "/https/" (fun s -> load_resource "https://" s);
  Sys_js.register_autoload' "/ftp/"   (fun s -> load_resource "ftp://" s);
  let module Storage = File_storage.FileStorage (struct let name  = storage end) in
  Sys_js.register_autoload' "/local_storage/" (fun (_,s) ->Storage.get_file_opt_js s);  
  Sys_js.register_autoload "/" (fun (_,s) -> load_resource_aux ((Js.string ("filesys/"^s^"?"^(version)))))

(* *********************************** *)  
(** Executing code in the the Toplevel *)
(* *********************************** *)  

(* without return value *)
let exec' s =
  let res : bool = JsooTop.use Format.std_formatter s in
  if not res then Format.eprintf "error while evaluating %s@." s

(* with return value : bool if execution succeed *)
let exec s =
  let res : bool = JsooTop.use Format.std_formatter s in
  (if not res then Format.eprintf "error while evaluating %s@." s );
  res

    
(** launching toplevel  *)
let setup_toplevel () =
  progress_next "toplevel";
  JsooTop.initialize ();
  Sys.interactive := false;
  (* progress_next "toplevel - Lwt_main"; *)
  (* exec' ("module Lwt_main = struct *)
  (*            let run t = match Lwt.state t with *)
  (*              | Lwt.Return x -> x *)
  (*              | Lwt.Fail e -> raise e *)
  (*              | Lwt.Sleep -> failwith \"Lwt_main.run: thread didn't return\" *)
  (*           end"); *)
  progress_next "toplevel - headers";
  let header1 =
      Printf.sprintf "        %s version %%s" compiler_name in
  let header2 = Printf.sprintf
      "     Compiled with Js_of_ocaml version %s" Sys_js.js_of_ocaml_version in
  exec' (Printf.sprintf "Format.printf \"%s@.\" Sys.ocaml_version;;" header1);
  exec' (Printf.sprintf "Format.printf \"%s@.\";;" header2);
  exec' ("#use \"/start/initialize.ml\" ");
  exec' ("#enable \"pretty\";;");
  exec' ("#enable \"shortvar\";;");
  Sys.interactive := true;
  ()

(** building main menu  *)
let setup_main_menu execute =
  progress_next "main_menu";
(*   let rec close ()  = *)
(*     let menu =  (by_id main_menu) and menu_button = by_id main_menu_button in *)
(*     (menu##classList##remove(Js.string cls_open); *)
(*       menu##classList##add(Js.string cls_closed); *)
(*       menu_button##onclick <- *)
(* 	Dom_html.handler (fun _ -> opening ()) *)
(*       ; *)
(*     Js._false) *)
(*   and  opening () = *)
(*     let menu =  (by_id main_menu) and menu_button = by_id main_menu_button  in *)
(*     (menu##classList##remove(Js.string cls_closed); *)
(*      menu##classList##add(Js.string cls_open); *)
(*      menu_button##onclick <- *)
(*        Dom_html.handler (fun _ -> close()) *)
(*       ; *)
(* 	Js._false *)
(*     ) *)
(*   in *)
  (by_id reinit_instrumentation)##onclick <-
    Dom_html.handler (fun _ -> execute "Bsml.reinit_instrumentation(); Bsml.stop_logging()"; Js._false)
  (* ; *)
(*   close () *)

(* resizing textbox and container to fit the text size *)
let resize ~container ~textbox ()  =
  Lwt.pause () >>= fun () ->
  textbox##style##height <- Js.string "auto";
  textbox##style##height <- Js.string (Printf.sprintf "%dpx" (max 18 textbox##scrollHeight));
  container##scrollTop <- container##scrollHeight;
  Lwt.return ()

(* Not sure how it works  *)
let setup_printers () =
  progress_next "printers";
  exec'("let _print_error fmt e = Format.pp_print_string fmt (Js.string_of_error e)");
  Topdirs.dir_install_printer Format.std_formatter (Longident.(Lident "_print_error"));
  exec'("let _print_unit fmt (_ : 'a) : 'a = Format.pp_print_string fmt \"()\"");
  Topdirs.dir_install_printer Format.std_formatter (Longident.(Lident "_print_unit"))


(** Code sharing :
   encode the code returned by get_code into an URL
   The URL is built from the URL of the page.
    
   If possible the URL is shortened using is.gd web service
 *)
                              

(* we need to compute the hash form href to avoid different encoding behavior
     across browser. see Url.get_fragment *)
let parse_hash () =
  let frag = Url.Current.get_fragment () in
  Url.decode_arguments frag

let rec iter_on_sharp ~f x =
  Js.Opt.iter (Dom_html.CoerceTo.element x)
	      (fun e -> if Js.to_bool (e##classList##contains(Js.string "sharp")) then f e);
  match Js.Opt.to_option x##nextSibling with
  | None -> ()
  | Some n -> iter_on_sharp ~f n

let share_code get_code position output =
  Dom_html.handler
    (fun _ ->
     let code_encoded = B64.encode (get_code()) in
     let url,is_file = match Url.Current.get () with
       | Some (Url.Http url) -> Url.Http ({url with Url.hu_fragment = "" }),false
       | Some (Url.Https url) -> Url.Https ({url with Url.hu_fragment= "" }), false
       | Some (Url.File url) -> Url.File ({url with Url.fu_fragment= "" }), true
       | _ -> assert false in
     let frag =
       let frags = parse_hash () in
       let frags = List.remove_assoc position frags @ [position,code_encoded] in
       Url.encode_arguments frags in
     let uri = Url.string_of_url url ^ "#" ^ frag in
     let append_url str =
       let dom = Tyxml_js.Html5.(
				p [ pcdata "Share this url : "; a ~a:[a_href str] [ pcdata str ]]) in
       Dom.appendChild output (Tyxml_js.To_dom.of_element dom)
     in
     Lwt.async (fun () ->
		Lwt.catch (fun () ->
			   if is_file
			   then failwith "Cannot shorten url with file scheme"
			   else
			     let uri = Printf.sprintf "http://is.gd/create.php?format=json&url=%s" (Url.urlencode uri) in
			     Lwt.bind (Jsonp.call uri) (fun o ->
							let str = Js.to_string o##shorturl in
							append_url str;
							Lwt.return_unit)
			  )
			  (fun exn ->
			   Format.eprintf "Could not generate short url. reason: %s@." (Printexc.to_string exn);
			   append_url uri;
			   Lwt.return_unit));
     Js._false)

(** Setting up a button event to share the content of the editor  *)
let setup_share_editor_button ~output =
  do_by_id editor_share_btn (fun e ->
			       e##classList##add(Js.string "ready");
			       e##onclick <- (share_code Editor.get_text "editor_code" output)
			      )

			    
(** Setting up a button event to share the code entered in the toplevel  *)
let setup_share_button ~output =
  do_by_id btn_share (fun e ->
    e##classList##add(Js.string "ready");
    e##onclick <- (share_code
		   (fun  () ->
		    let code = ref [] in
		    Js.Opt.iter
		      (output##firstChild)
		      (iter_on_sharp ~f:(fun e ->
					 code := Js.Opt.case (e##textContent)
							     (fun () -> "")
							     (Js.to_string) :: !code));
		    (String.concat "" (List.rev !code))
		   )
		   "code"
		   output
		  )
	   )
(** Get the lang attribute of the page  *)
let get_language () = getAttribute (first_by_tag "html") "lang"

open Editor    

(** Installing the editor menu and filling the editor panel with previously saved code  *)
let setup_menu ~container ~textbox ~output execute  = 
  progress_next "editor_menu";
  (* let menu = by_id editor_menu in *)
  let load_btn =(by_id editor_load_button)##onclick <- (
      Dom_html.handler (fun _ ->
			let code = (editor##getValue())
			in
			textbox##value <- (code)##trim();
			Lwt.async(fun () ->
				  resize ~container ~textbox ()  >>= fun () ->
				  textbox##focus();
				  Lwt.return_unit);
			Js._false)
    )
  and load_selection =
    (by_id editor_load_selection_button)##onclick <- (
      Dom_html.handler (fun _ ->
			let code = Js.string (get_selected_text())
			in
			textbox##value <- (code)##trim();
			Lwt.async(fun () ->
				  resize ~container ~textbox ()  >>= fun () ->
				  textbox##focus();
				  Lwt.return_unit);
			Js._false)
    )

  and eval_btn =(by_id editor_eval_button)##onclick <- (
      Dom_html.handler (fun _ ->
			let code =
			  Js.to_string(editor##getValue())
			in
			execute code;
			Js._false
		       )
    )

  and eval_selection_btn = (by_id editor_eval_selection_button)##onclick <- (
      Dom_html.handler (fun _ ->
			execute (get_selected_text());
			Js._true
		       )
    )
  and eval_save_btn = (by_id editor_save_button)##onclick <- (
      Dom_html.handler (fun _ ->
			Editor.save();
			Js._false
		       )
    )
  in
  setup_share_editor_button ~output;
  Editor.init()
let setup_canvas () = try make_detachable "bsp-canvas-container-container" with _ -> ()
let _ = init ();;

(** Reset button to reinit the toplevel and exercises  *)
let setup_reset_button exec_and_print reinit_exercises reinit_toplevel_view =
  progress_next "reset_button";
  do_by_id btn_reset (fun e ->
			e##onclick <- Dom_html.handler
					(fun _ ->
					 ignore (exec_and_print   "Bsml.reinit_instrumentation(); Bsml.stop_logging()") ;
					 reinit_toplevel_view ();
					 setup_toplevel ();
					 reinit_exercises ();
					 (* Shared_value.increment(); *)
					 Js._false
					)
		       )
			    
	   
let current_position = ref 0
let highlight_location loc =
  let x = ref 0 in
  let output = by_id output in
  let first = Js.Opt.get (output##childNodes##item(!current_position)) (fun _ -> failwith ("fail to highlight item"^(string_of_int !current_position ))) in
  iter_on_sharp first
    ~f:(fun e ->
     incr x;
     let _file1,line1,col1 = Location.get_pos_info (loc.Location.loc_start) in
     let _file2,line2,col2 = Location.get_pos_info (loc.Location.loc_end) in
     if !x >= line1 && !x <= line2
     then
       let from_ = if !x = line1 then `Pos col1 else `Pos 0 in
       let to_   = if !x = line2 then `Pos col2 else `Last in
       Colorize.highlight from_ to_ e)


let append colorize output cl s =
  Dom.appendChild output (Tyxml_js.To_dom.of_element (colorize ~a_class:cl s))
		   
		  
			  
				  
module History =  struct
  let data = ref [|""|]
		 
  let idx = ref 0
  let get_storage () =
    match Js.Optdef.to_option Dom_html.window##localStorage with
    | None -> raise Not_found
    | Some t -> t

  let setup () =
    try
      let s = get_storage () in
      match Js.Opt.to_option (s##getItem(Js.string "history")) with
      | None -> raise Not_found
      | Some s -> let a = Json.unsafe_input s in
		  data:=a; idx:=Array.length a - 1
    with _ -> ()


  let push text =
    let l = Array.length !data in
    let n = Array.make (l + 1) "" in
    Array.set  !data (l - 1) text;
    Array.blit !data 0 n 0 l;
    data := n; idx := l;
    try
      let s = get_storage () in
      let str = Json.output !data in
      s##setItem(Js.string "history", str)
    with Not_found -> ()
  let current text = !data.(!idx) <- text
  let previous textbox =
    if !idx > 0
    then begin decr idx; textbox##value <- Js.string (!data.(!idx)) end
  let next textbox =
    if !idx < Array.length !data - 1
    then begin incr idx; textbox##value <- Js.string (!data.(!idx)) end
end
		
(* let rec minimize e = *)
(*   Dom_html.handler (fun _ -> *)
(*   e##setAttribute(Js.string "height",Js.string  "20px"); *)
(*   e##setAttribute(Js.string "width",Js.string "20px"); *)
(*   e##onclick <-(maximize e); *)
(*   Js._false) *)

(* and maximize e = *)
(*   Dom_html.handler (fun _ -> *)
(*   e##setAttribute(Js.string "height",Js.string ""); *)
(*   e##setAttribute(Js.string "width",Js.string ""); *)
(*   e##onclick <-(minimize e); *)
(*   Js._false) *)



(* let setup_help () = *)
		    (*   (by_id "help")##onclick <-(minimize (by_id "help")) *)
		    
let confirm s = Js.to_bool(Dom_html.window##confirm(Js.string s))
		    
open Load_exercise		   
let run _ =
  let lang = get_language () in
  begin
    progress_next "setting language";
    Shared.log ("set language to "^lang);
               
    (* setting log in Loader *)
    Loader.log := Shared.log;
    (* code executed when document is loaded  *)
    let container = by_id toplevel_container in
    let output = by_id output in
    progress_next "connecting pipes";
    let textbox : 'a Js.t = by_id_coerce "userinput" Dom_html.CoerceTo.textarea in
    
    let sharp_chan = open_out "/dev/null0" in
    let sharp_ppf = Format.formatter_of_out_channel sharp_chan in
    
    let caml_chan = open_out "/dev/null1" in
    let caml_ppf = Format.formatter_of_out_channel caml_chan in

    let exec_and_print  ?(print_result=true) ?(print_code=true) ?(enable_logging=true) content =
      let content' =
	let len = String.length content in
	if try content = "" || (content.[len-1] = ';' && content.[len-2] = ';') with _ -> false
	then content
	else content ^ ";;" in
      let res = ref true in
      let highlight_location = (fun loc -> highlight_location loc; res := false ) in
      (if enable_logging then
         (
           JsooTop.execute false ~highlight_location:(fun _ ->()) caml_ppf "Bsml.start_logging ();;"
         )
      );
      ( if print_code then 
	  JsooTop.execute print_result ~pp_code:sharp_ppf ~highlight_location caml_ppf content'
        else
	  JsooTop.execute print_result ~highlight_location caml_ppf content'
      );
      (if enable_logging then
         JsooTop.execute false ~highlight_location:(fun _ ->())  caml_ppf "Bsml.stop_logging ();;"
      );
      !res
    in
    let execute () =
      let content = Js.to_string (textbox##value##trim()) in
      current_position := output##childNodes##length;
      textbox##value <- Js.string "";
      History.push content;
      ignore(exec_and_print content);
      resize ~container ~textbox () >>= fun () ->
      container##scrollTop <- container##scrollHeight;
      textbox##focus();
      Lwt.return_unit in
    let history_down e =
      let txt = Js.to_string textbox##value in
      let pos = (Obj.magic textbox)##selectionStart in
      try
	(if String.length txt = pos  then raise Not_found);
	let _ = String.index_from txt pos '\n' in
	Js._true
      with Not_found ->
	History.current txt;
	History.next textbox;
	Js._false
    in
    let history_up   e =
      let txt = Js.to_string textbox##value in
      let pos = (Obj.magic textbox)##selectionStart - 1  in
      try
	(if pos < 0  then raise Not_found);
	let _ = String.rindex_from txt pos '\n' in
	Js._true
      with Not_found ->
	History.current txt;
	History.previous textbox;
	Js._false
    in

    let meta e =
      let b = Js.to_bool in
      b e##ctrlKey || b e##shiftKey || b e##altKey || b e##metaKey in
    let ctrl e =
      let b = Js.to_bool in
      b e##ctrlKey in
    begin (* setup handlers *)
      textbox##onkeyup <-   Dom_html.handler (fun _ -> Lwt.async (resize ~container ~textbox); Js._true);
      textbox##onchange <-  Dom_html.handler (fun _ -> Lwt.async (resize ~container ~textbox); Js._true);
      textbox##onkeydown <- Dom_html.handler (fun e ->
					      match e##keyCode with
					      | 13 when not (meta e) -> Lwt.async execute; Js._false
					      | 13 -> Lwt.async (resize ~container ~textbox); Js._true
					      | 09 -> Indent.textarea textbox; Js._false
					      | 76 when ctrl e -> output##innerHTML <- Js.string ""; Js._true
					      | 75 when ctrl e -> setup_toplevel (); Js._false
					      | 38 -> history_up e
					      | 40 -> history_down e
					      | _ -> Js._true
					     );
    end;
    progress_next "installing async exception printer";

    Lwt.async_exception_hook:=(fun exc ->
			       Format.eprintf "exc during Lwt.async: %s@." (Printexc.to_string exc);
			       match exc with
			       | Js.Error e -> Firebug.console##log(e##stack)
			       | _ -> ());

    
    Lwt.async (fun () ->
	       resize ~container ~textbox () >>= fun () ->
	       textbox##focus ();
	       Lwt.return_unit);
    progress_next "installing initial drawing canvas";    
#ifdef graphics
       (try Graphics_js.open_canvas (by_id_coerce "bsp-canvas-0" Dom_html.CoerceTo.canvas) with _ -> ());
#endif

      progress_next "channel_flusher";
    Sys_js.set_channel_flusher caml_chan  (append Colorize.ocaml output "caml");
    Sys_js.set_channel_flusher sharp_chan (append Colorize.ocaml output "sharp");
    Sys_js.set_channel_flusher stdout     (append Colorize.text  output "stdout");
    Sys_js.set_channel_flusher stderr     (append Colorize.text  output "stderr");

    progress_next "setting interface";
    setup_share_button ~output;
    (* Embeded_bsml.init_bsml(); *)
    (* trace ~fname:"examples" (fun () -> setup_examples ~container ~textbox); *)
    trace_ignore ~fname:"menu" (fun ()->setup_menu ~container ~textbox ~output (exec_and_print ~print_code:false ~enable_logging:true));
    trace_ignore ~fname:"main menu"  (fun ()->setup_main_menu (exec_and_print ~print_code:false  ~enable_logging:false)) ;
    (* setup_help (); *)
    trace ~fname:"pseudo fs" (fun () -> setup_pseudo_fs ~storage:"Editor_store" ());
    progress_next "Setting bsml environment";
    trace ~fname:"init_bsml" Embeded_bsml.init_bsml;
    progress_next " toplevel";
    trace ~fname:"toplevel" (fun () -> setup_toplevel ());
    progress_next " ss canvas";
    trace ~fname:"ss canvas" setup_canvas;
    (* setup_js_preview (); *)
    (* trace ~fname:"printers" (fun () -> setup_printers ()); *)
    progress_next " history";
    trace ~fname:"history" (fun () -> History.setup ());
    progress_next "install introduction";
    trace ~fname:"inroduction" (fun () -> Intro.install_intro ());
    (** Loading user provided files in the toplevel *)
    progress_next " source loader";
    let module Env = (struct
		       let container = "sources"
		       let file_container = "source-file-container"
		       let exec = (fun code ->
				   try
				     exec_and_print ~print_code:false code
				   with _ -> false)
		       let edit = (fun code ->
				   try  
				     if
				       confirm (str_clear_editor lang)
				     then
				       ignore(Editor.replace code)
				     else
				       ignore(Editor.append code)
				   ; true
				   with _ -> false)
		     end)
    in
    begin
      (* Source loading *)
      let module Sources =
	Source_files.SourceFiles (Env)
      in
      Sources.setup_load_file_button ();
      (*Exercises  *)
      let module View =Load_exercise.Exercise_View in
      View.init lang;
      let module Controler =Load_exercise.Exercise_Controler(Env) in
      Controler.init ();
      setup_reset_button  (exec_and_print   ~print_result:false   ~print_code:false  ~enable_logging:false )
                          (fun () -> Loader.reinit ();View.reinit lang; Controler.reinit ())
			  (fun _ -> output##innerHTML <- Js.string ""; ());
      (* Modules *)
      trace_ignore (
	  fun () ->
	  Load_course.V.init lang;
	  Load_course.C.init();
	  Load_course.M.New_cmo_file_listeners.add
	    (fun (crs,file_name) -> Loader.load_not_exercise  Shared.alert file_name);
	  Load_course.M.New_exercise_file_listeners.add
	    (fun (pos,crs,exo_file) -> Controler.new_exercise_handler ~pos:(Some pos) crs exo_file);
	  Load_course.M.init();
	  ()
	)
      
    end;
    ();  
    textbox##value <- Js.string "";

    progress_next " editor saved code";
    
    (* Load code in editor any *) 
    try
      let code = List.assoc "editor_code" (parse_hash ()) in
      ignore(Editor.editor##setValue(Js.string (B64.decode code),0))
    with
    | Not_found -> ()
    | exc -> Firebug.console##log_3(Js.string "exception", Js.string (Printexc.to_string exc), exc) ;
	     (* Run initial code if any *)
	     try
	       let code = List.assoc "code" (parse_hash ()) in
	       textbox##value <- Js.string (B64.decode code);
	       Lwt.async execute
	     with
	     | Not_found -> ()
	     | exc -> Firebug.console##log_3(Js.string "exception", Js.string (Printexc.to_string exc), exc)
                                            
  end
let _ = Dom_html.window##onload <- Dom_html.handler (fun _ -> run (); progress_finished(); Js._false)
